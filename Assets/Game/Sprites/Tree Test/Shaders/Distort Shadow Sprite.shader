Shader "Sprites/Distort Shadow Sprite"
{
    Properties
    {
        _ShadowTex ("Shadow Texture", 2D) = "white" {}
        _DistortTex ("Distort Texture", 2D) = "white" {}
        _Tiling ("MainTex Tiling", Vector) = (0,0,0,0)
        _DistortAmount ("Distortion Amount", float) = 0 
        
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        

        Pass
        {
        CGPROGRAM
            #pragma vertex SpriteVert2
            #pragma fragment SpriteFrag2
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc"      
            
            sampler2D _ShadowTex;
            sampler2D _DistortTex;
            float4 _Tiling;
            float _DistortAmount;
            
            struct v2f2
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                float4 worldPos : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            v2f2 SpriteVert2(appdata_t IN)
            {
                v2f2 OUT;
            
                UNITY_SETUP_INSTANCE_ID (IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
            
                OUT.vertex = UnityFlipSprite(IN.vertex, _Flip);
                OUT.vertex = UnityObjectToClipPos(OUT.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _RendererColor;
            
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif
                
                OUT.worldPos = mul(unity_ObjectToWorld, IN.vertex);
                return OUT;
            }
                        
            fixed4 SpriteFrag2(v2f2 IN) : SV_Target
            {
                fixed4 c = SampleSpriteTexture (IN.worldPos.xy * _Tiling.xy) * IN.color;
                fixed4 d = tex2D(_DistortTex, IN.worldPos.xy * _Tiling.zw);
                               
                float dGray = (d.r + d.g + d.b) / 3;
                float cGray = (c.r + c.g + c.b) / 3;
                float gray = (dGray + cGray) / 2;
                            
                gray -= 0.5;
                gray *= 2;                
                fixed s = 1 - tex2D(_ShadowTex, IN.texcoord + float2(gray * _DistortAmount,0)).a;                
                c.rgb *= s;
                return c;
            }
        ENDCG
        }
    }
}
