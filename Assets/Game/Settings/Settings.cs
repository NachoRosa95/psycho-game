﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Object/Singleton/Settings")]
public class Settings : SingletonScriptableObject<Settings>
{
	public float TextSpeed = 0.2f;
}
