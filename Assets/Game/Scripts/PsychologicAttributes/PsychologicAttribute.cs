﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttributeID
{
    None = 0,
    Empathy = 100,
    Aggressiveness = 101,

    ProblemSolvingSum = 200,
    ReflectivitySum = 201,
    ImpulsivitySum = 202
}

[CreateAssetMenu(menuName = "PsychoEngine/Attribute")]
public class PsychologicAttribute : ScriptableObject
{
    public List<PsychologicRelation> Relations = new List<PsychologicRelation>();
    public string AttributeName;
    public float Weight
    {
        get { return weight; }
        set
        {
            if (value < 0) value = 0;
            if (weight == value) return;
            weight = value;
        }
    }
    public AttributeID ID;

    [SerializeField] float weight;
    private float lastWeight = -1;
    protected static List<PsychologicAttribute> visiteds = new List<PsychologicAttribute>();

    public void Inject(float weightToAdd)
    {
        InjectRecursive(weightToAdd);
        visiteds.Clear();
    }

    public virtual void InjectRecursive(float weightToAdd, PsychologicAttribute summand = null)
    {
        visiteds.Add(this);

        //Debug.Log(this.name + ": " + weight + " += " + weightToAdd);
        Weight += weightToAdd;

        foreach (PsychologicRelation relation in Relations)
        {
            var attribute = relation.Target;
            if (!attribute || visiteds.Contains(attribute)) continue;

            if (!relation.Conditional)
                attribute.InjectRecursive(weightToAdd * relation.WeightMultiplier, this);
            else if (relation.Execute(weightToAdd))
                attribute.InjectRecursive(relation.Output * relation.WeightMultiplier, this);
            else
            {
                // Condition not fulfilled
            }
        }
    }

    public void SetWeight(float value)
    {
        weight = value;
    }

    public void Log()
    {
        // if (!System.Enum.IsDefined(typeof(AttributeID), ID))
        // {
        //     Debug.LogError(string.Format("Current ID of {0} ({1}) is not a valid ID defined in enum AttributeIDs and thus may not be logged. You may need to add it to the list", AttributeName, ID), this);
        //     return;
        // }
        if (weight == lastWeight || ID == AttributeID.None) return;

        Metricas.DoAction(ActionType.AttributeChange, ID.ToString(), weight);
        lastWeight = weight;
    }
}