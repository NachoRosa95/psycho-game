﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ConditionalRelation : PsychologicRelation
{
    // Obsolete, saved for prosperity

    // public float Output;
    // public float Upper;
    // public float Lower;

    // public bool Execute(float value)
    // {
    //     return Lower > value ? false : value > Upper ? false : true;
    // }
}
