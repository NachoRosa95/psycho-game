﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugSEM : MonoBehaviour
{
    [SerializeField] PsychologicAttribute empathy;
    [SerializeField] PsychologicAttribute aggresivness;


    void Update()
    {
        Debug.Log("Empathy has a weight of: " + empathy.Weight);
        Debug.Log("Aggresivness has a weight of: " + aggresivness.Weight);
    }

}
