﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSolveAttributeChange : AttributeChangeEvent
{
    public void OnSolved()
    {
        // Debug.LogWarning("OnSolved");
        Activate();
    }
}
