﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributeChangeEvent : MonoBehaviour
{
    [SerializeField] protected PsychologicAttribute attribute;
    [SerializeField] protected AnimationCurve curve;
    [SerializeField] protected float maxWeightFromThisSource;
    [SerializeField] protected float weightToAdd;

    // Para que sea visible en modo Debug
    private float weight;
    private float accumWeight;

    public float Weight { get { return GetWeight(); } }

    protected float GetWeight()
    {
        // Divido a accumWeight por maxWeightFromThisSource para que al pasar un numero de 0 a maxWeightFromThisSource devuelva un numero de 0 a 1
        var weightMultiplier = curve.Evaluate(accumWeight / maxWeightFromThisSource);
        weight = weightToAdd * weightMultiplier;
        if (weight < 0.0001) weight = 0;
        return weight;
    }

    public void Activate()
    {
        accumWeight += weightToAdd;
        var weight = Weight;
        Debug.LogWarning("Injecting " + weight + " to " + attribute.AttributeName);
        attribute.Inject(weight);

        //Debug.Log("Attribute "+ attribute.name +"'s value is: "+attribute.Weight);
    }
}
