﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class SumAttribute : PsychologicAttribute
{
    public List<PsychologicAttribute> Summands = new List<PsychologicAttribute>();

    public override void InjectRecursive(float weightToAdd, PsychologicAttribute summand = null)
    {
        if (!Summands.Contains(summand)) Summands.Add(summand);
        Weight = Summands.Sum(item => item.Weight);

        // visiteds.Add(this);

        // foreach (PsychologicRelation relation in Relations)
        // {
        //     var attribute = relation.Target;
        //     if (!attribute || visiteds.Contains(attribute)) continue;

        //     var condition = relation as ConditionalRelation;
        //     if (condition == null)
        //         attribute.InjectRecursive(weightToAdd * relation.WeightMultiplier);
        //     else if (condition.Execute(weightToAdd))
        //         attribute.InjectRecursive(condition.Output * relation.WeightMultiplier);
        //     else
        //     {
        //         // Condition not fulfilled
        //     }
        // }
    }
}
