﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDamageAttributeChange : AttributeChangeEvent
{
    public LayerMask validLayers;

    public void OnDamage(GameObject dealer = null)
    {
        if(dealer == null) return;
        // If validLayers contains source.layer
        if (validLayers == (validLayers | (1 << dealer.layer)))
        {
            Activate();
        }
    }
}
