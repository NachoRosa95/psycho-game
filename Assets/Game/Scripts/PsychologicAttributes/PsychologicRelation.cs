﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PsychologicRelation
{
    // public PsychologicAttribute Origin;
    public PsychologicAttribute Target;
    public float WeightMultiplier = 1;

    [Space]
    public bool Conditional = false;
    public float Output = 1;
    public float Upper = 1;
    public float Lower = 0;

    public bool Execute(float value)
    {
        return Lower > value ? false : value > Upper ? false : true;
    }
}
