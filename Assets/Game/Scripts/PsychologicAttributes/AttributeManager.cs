﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AttributeManager : MonoBehaviour
{
    [SerializeField] List<PsychologicAttribute> attributes;
    [SerializeField] float initialValue = 0;
    [SerializeField] float logRate = 5f;

    void Awake()
    {
        InitAttributes();
        if (!Application.isEditor)
            StartCoroutine("doLogs");
        // SceneManager.sceneLoaded += InitAttributes;
    }

    void InitAttributes()
    {
        foreach (PsychologicAttribute attribute in attributes)
        {
            attribute.SetWeight(initialValue);
        }
    }

    IEnumerator doLogs()
    {
        while (Application.isPlaying)
        {
            yield return new WaitForSeconds(logRate);
            Log();
        }
    }

    void Log()
    {
        foreach (PsychologicAttribute attribute in attributes) attribute.Log();
    }
}
