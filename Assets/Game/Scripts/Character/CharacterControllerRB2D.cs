﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.Serialization;


public class CharacterControllerRB2D : MonoBehaviour
{
    public enum Walls
    {
        Any,
        Left,
        Right
    }

    public enum SurfaceAngle
    {
        Any,
        FloorSlip,
        Floor,
        Wall,
        Ramp,
        Ceiling
    }

    [SerializeField, FormerlySerializedAs("JumpKey")]
    private string jumpKey = "Jump";
    
    [SerializeField, FormerlySerializedAs("AttackKey")]
    private string attackKey = "Attack";
    
    [SerializeField]
    private Weapon weapon;
    
    [SerializeField]
    private Transform body;

    [SerializeField]
    private LayerMask groundedMask;

    [SerializeField]
    private AudioClip midAirJumpClip;

    [SerializeField, FormerlySerializedAs("Speed")]
    private float speed = 1;

    [SerializeField, FormerlySerializedAs("Acceleration")]
    private float acceleration = 1;

    [SerializeField, FormerlySerializedAs("Drag")]
    private float drag = 1;

    [SerializeField, FormerlySerializedAs("JumpSpeed")]
    private float jumpSpeed = 1;

    [SerializeField, FormerlySerializedAs("WallJumpSpeed")]
    private float wallJumpSpeed = 50;

    [SerializeField, FormerlySerializedAs("MaxJumps")]
    private int maxJumps = 1;

    [SerializeField, FormerlySerializedAs("WallJumpEnabled")]
    private bool wallJumpEnabled = true;

    [SerializeField]
    private float weaponInterpolationSpeed = 50;

    [SerializeField, FormerlySerializedAs("FallMultiplier")]
    private float fallMultiplier = 2.5f;

    [SerializeField, FormerlySerializedAs("LowJumpMultiplier")]
    private float lowJumpMultiplier = 2f;
    
    [SerializeField]
    private bool isGrounded;

    [SerializeField, Range(0,0.2f)]
    private float skinWidth = 0.02f;

    [SerializeField]
    private KeyCode cheatKey;
    
    private Rigidbody2D rb;
    private Vector2 velMultiplier;
    private Collider2D col;
    private AudioSource feetAudioSource;
    private float curJumps = 0;
    private Vector2 lastNormal;
    private Vector2 lastContact;
    private Vector2 mousePosRelative;
    private List<Vector2> offsets;
    private bool cheating;
    private bool blockMovement;

    private float horizontalAxisValue;
    private float previousHorizontalAxisValue;

    private Walls allowedWall = Walls.Any;
    private SurfaceAngle lastSurfaceAngle = SurfaceAngle.Any;
    private Collider lastCollider;
    private List<Surface> lastSurfaces = new List<Surface>();
    
    //Sound component
    private Temp_SoundComponent soundComp;
    
    // Range of y component for a normal to be considered a wall
    // Anything higher is a ceiling
    // Anything lower is determined by its x component
    private Vector2 wallNormalYRange = new Vector2(-0.1f, 0.1f);

    // Range of x component for a normal to be considered a floor
    // Anything higher or lower is considered a steep ramp
    private Vector2 floorNormalXRange = new Vector2(-0.8f, 0.8f);

    public UnityEvent_Float OnHorizontalValueChange = new UnityEvent_Float();
    public UnityEvent OnJumpStarted = new UnityEvent();
    public UnityEvent OnAttackStarted = new UnityEvent();
    public UnityEvent_Bool OnGroundedChanged = new UnityEvent_Bool();

    public float HorizontalAxisValue
    {
        get => horizontalAxisValue;
        set
        {
            horizontalAxisValue = value;
            if (horizontalAxisValue != previousHorizontalAxisValue) OnHorizontalValueChange?.Invoke(value);

            previousHorizontalAxisValue = horizontalAxisValue;
        }
    }

    private Vector3 GroundedOriginPosition
    {
        get
        {
            return transform.position + Vector3.up * skinWidth;
        }
    }

    public bool BlockMovement { get => blockMovement; set => blockMovement = value; }
    public Rigidbody2D RB { get => rb; set => rb = value; }

    private void Awake()
    {
        if (body == null) body = transform.Find("Body");
        if (weapon == null) weapon = GetComponentInChildren<Weapon>();
        rb = GetComponent<Rigidbody2D>();
        // sr = body.GetComponent<SpriteRenderer>();
        col = body.GetComponent<Collider2D>();
        feetAudioSource = GetComponent<AudioSource>();
        soundComp = GetComponent<Temp_SoundComponent>();

        offsets = new List<Vector2>();
        offsets.Add(Vector3.right);
        offsets.Add(-Vector3.up);
        offsets.Add(-Vector3.right);
        offsets.Add(Vector3.up);
    }

    private void Update()
    {
        // wasGrounded = isGrounded;
        var wasGrounded = isGrounded;
        RaycastGrounded();
        // if (wasGrounded != isGrounded) OnGroundedChanged?.Invoke(isGrounded);
        UpdateMousePos();

        if (blockMovement) return;

        ActivateCheatMode();
        UpdateInput();
        UpdateAnimationInput();
        UpdateVelocity();
    }

    private void ActivateCheatMode()
    {
        if (Input.GetKeyDown(cheatKey)) cheating = !cheating;

        if (cheating) rb.bodyType = RigidbodyType2D.Kinematic;
        else rb.bodyType = RigidbodyType2D.Dynamic;

    }

    private void UpdateMousePos()
    {
        var mousePosScreen = Input.mousePosition;
        var mousePosWorld = Camera.main.ScreenToWorldPoint(mousePosScreen);
        mousePosWorld.z = 0;
        var vectorTemp = mousePosWorld - transform.position;
        vectorTemp.z = 0;

        mousePosRelative = vectorTemp;

        // weapon.transform.right = Vector3.Lerp(weapon.transform.right, vectorTemp, weaponInterpolationSpeed * Time.deltaTime);


    }

    private void UpdateInput()
    {
        velMultiplier.x = Input.GetAxisRaw("Horizontal");

        //CHEAT MODE
        if (cheating) velMultiplier.y = Input.GetAxisRaw("Vertical");
        else velMultiplier.y = 0;
        //CHEAT MODE

        if (Input.GetButtonDown(jumpKey) && !isJumping)
        {
            StartCoroutine(_JumpPeriod());
        }
        if (Input.GetKeyDown(KeyCode.K)) { GetComponent<HealthComponent>().Damage(1, gameObject); }

        if (Time.deltaTime > 0)
        {
            var joysticks = Input.GetJoystickNames();
            var useJoystick = false;

            Vector2 input;

            foreach (string name in joysticks)
            {
                if (!string.IsNullOrEmpty(name)) useJoystick = true;
            }
            if (!useJoystick && Input.mousePresent)
            {
                input = mousePosRelative.normalized;
            }
            else
            {
                input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            }

            // float z;
            // dir = new Vector2(Mathf.Round(input.x), Mathf.Round(input.y));

            var mousePosition = Input.mousePosition;
            mousePosition.z = weapon.transform.position.z;
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // var desiredForward = mousePosition - weapon.transform.position;

            var minDist = float.MaxValue;
            Vector3 minOffset = default;
            foreach (Vector3 offset in offsets)
            {
                var dist = Vector3.Distance(mousePosition, transform.position + offset);
                if (dist < minDist)
                {
                    minDist = dist;
                    minOffset = offset;
                }
            }

            var lookRotation = Quaternion.LookRotation(Vector3.forward, minOffset);
            weapon.transform.rotation = lookRotation;

            // weapon.transform.LookAt(weapon.transform.position + minOffset, Vector3.right);

            // if (Mathf.Abs(input.x) > Mathf.Abs(input.y))
            // {
            //     z = Mathf.Sign(input.x) == 1 ? 0 : 180;
            // }
            // else if (Mathf.Abs(input.y) > Mathf.Abs(input.x))
            // {
            //     z = Mathf.Sign(input.y) == 1 ? 90 : 270;
            // }
            // else z = -1;

            // if (z != -1) weapon.transform.localRotation = Quaternion.Euler(0, 0, z);

            if (input.x < 0) { FaceLeft(); }
            else if (input.x > 0) { FaceRight(); }

            if (Input.GetButtonDown(attackKey)/* && !EventSystem.current.IsPointerOverGameObject()*/)
            {
                //Debug.Log("Attacked");
                OnAttackStarted?.Invoke();
            }
        }
    }

    private void UpdateAnimationInput()
    {
        HorizontalAxisValue = Input.GetAxis("Horizontal");
    }

    private void UpdateVelocity()
    {
        //CHEAT MODE
        if (cheating) rb.gravityScale = 0;
        else rb.gravityScale = 4;
        //CHEAT MODE

        // if (!RampSliding && isGrounded) rb.constraints |= RigidbodyConstraints2D.FreezePositionX;
        // else rb.constraints &= ~RigidbodyConstraints2D.FreezePositionX;
        Vector2 vel;

        //CHEAT MODE
        if (cheating) vel = new Vector2(rb.velocity.x + velMultiplier.x * acceleration * Time.deltaTime, rb.velocity.y + velMultiplier.y * acceleration / 2 * Time.deltaTime);
        else vel = new Vector2(rb.velocity.x + velMultiplier.x * acceleration * Time.deltaTime, rb.velocity.y);
        //CHEAT MODE

        var drag = this.drag;
        if (lastSurfaceAngle == SurfaceAngle.FloorSlip) drag = 0;
        if (isGrounded)
        {
            vel.x = Mathf.MoveTowards(vel.x, 0, drag * Time.deltaTime);
        }

        //CHEAT MODE
        if (!cheating)
        {
            if (Mathf.Abs(vel.x) > speed)
                vel.x = vel.x > 0 ? speed : -speed;
        }
        //CHEAT MODE

        rb.velocity = vel;
        // rb.MovePosition(rb.position + new Vector2(vel.x,0));

        //OLD FOOTSTEPS
        //if (Time.deltaTime > 0 && isGrounded && velMultiplier.x != 0f)
        //{
        //    foreach (Surface surface in lastSurfaces)
        //    {
        //        PlaySurfaceClip(surface, SurfaceAudioType.Walk, false);
        //        feetAudioSource.loop = true;
        //    }
        //}
        //if (!isGrounded || velMultiplier.x == 0f) { feetAudioSource.Stop(); feetAudioSource.loop = false; }


        //CHEAT MODE
        if (!isGrounded && !cheating)
        {
            // if (rb.velocity.y < 0) rb.velocity += Vector2.up * Physics2D.gravity.y * (FallMultiplier - 1) * Time.deltaTime;  
            if (rb.velocity.y > 0 && !Input.GetButton(jumpKey)) rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
        //CHEAT MODE

        // rb.MovePosition((Vector2)transform.position + vel);
        //  rb.velocity = new Vector2(velMultiplier.x * Speed, rb.velocity.y);
    }

    private void LateUpdate()
    {
        // curNormal = transform.up;
        // isGrounded = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Test
        //transform.SetParent(collision.transform);

        lastSurfaces.Clear();
        var magnitude = collision.relativeVelocity.magnitude;
        var minMagnitude = 15;
        foreach (ContactPoint2D contact in collision.contacts)
        {
            var surface = contact.collider.GetComponent<Surface>();
            if (surface && !lastSurfaces.Contains(surface)) lastSurfaces.Add(surface);
        }

        if (!feetAudioSource || magnitude < minMagnitude) return;
        foreach (Surface surface in lastSurfaces)
        {
            soundComp.PlaySurfaceClip(surface, SurfaceAudioType.Land);
            //PlaySurfaceClip(surface, SurfaceAudioType.Land);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        foreach (ContactPoint2D contact in collision.contacts)
        {
            var surface = contact.collider.GetComponent<Surface>();
            if (surface && !lastSurfaces.Contains(surface)) lastSurfaces.Add(surface);

            lastContact = contact.point;
            if (contact.collider.gameObject.tag == "CannotWalljump") continue;

            var curSurface = GetSurfaceAngle(contact.normal);
            if ((int)curSurface > (int)SurfaceAngle.Wall) continue;

            if (curSurface == SurfaceAngle.Wall)
            {
                if (!wallJumpEnabled) continue;
                lastSurfaceAngle = SurfaceAngle.Wall;

                var side = contact.normal.x > 0 ? Walls.Left : Walls.Right;
                if (allowedWall == Walls.Any || allowedWall == side)
                {
                    allowedWall = side == Walls.Left ? Walls.Right : Walls.Left;
                    StopJumping();
                }
                else
                {
                    continue;
                }
            }
            else if (curSurface == SurfaceAngle.Floor)
            {
                if (collision.gameObject.tag == "Slippery") lastSurfaceAngle = SurfaceAngle.FloorSlip;
                else lastSurfaceAngle = SurfaceAngle.Floor;
                allowedWall = Walls.Any;

                // To avoid resetting jumps when jumping through platforms, only resets when feet collide
                var dist = Mathf.Abs(contact.point.y - col.bounds.min.y);
                Debug.DrawLine(contact.point, col.bounds.center);

                if (dist < 0.1f)
                    StopJumping();
            }

            lastNormal = contact.normal;
            // StopJumping();
        }
    }

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    //Test
    //    transform.parent = null;
    //}

    public void StartAttack()
    {
        weapon.WeaponActivate();
    }

    SurfaceAngle GetSurfaceAngle(Vector2 normal)
    {
        if (normal.y < wallNormalYRange.x)
        {
            return SurfaceAngle.Ceiling;
        }
        else if (wallNormalYRange.x < normal.y && normal.y < wallNormalYRange.y)
        {
            return SurfaceAngle.Wall;
        }
        else if (floorNormalXRange.x < normal.x && normal.x < floorNormalXRange.y)
        {
            return SurfaceAngle.Floor;
        }
        else
        {
            return SurfaceAngle.Ramp;
        }
    }

    private bool StartJumping()
    {
        if (curJumps >= maxJumps || Time.deltaTime == 0) return false;
        var distToContact = Vector2.Distance(transform.position, lastContact);
        // velMultiplier.y = 1;
        var velocityY = rb.velocity.y + jumpSpeed;
        if (velocityY < jumpSpeed) velocityY = jumpSpeed;
        var jumpForce = new Vector2(0, /* velMultiplier.y * */ velocityY);
        rb.velocity = new Vector2(rb.velocity.x, 0);

        // Debug.Log(distToContact);
        Debug.DrawLine(lastContact, lastContact + lastNormal, Color.cyan, 1f);
        if (distToContact < transform.localScale.magnitude)
        {
            if (lastSurfaceAngle == SurfaceAngle.Floor)
            {
                foreach (Surface surface in lastSurfaces)
                {
                    soundComp.PlaySurfaceClip(surface, SurfaceAudioType.Jump);
                    //PlaySurfaceClip(surface, SurfaceAudioType.Jump);
                }
            }
            else if (lastSurfaceAngle == SurfaceAngle.Wall && !isGrounded)
            {
                foreach (Surface surface in lastSurfaces)
                {
                    soundComp.PlaySurfaceClip(surface, SurfaceAudioType.Jump);
                    //PlaySurfaceClip(surface, SurfaceAudioType.Jump);
                }
                jumpForce += new Vector2(lastNormal.x * wallJumpSpeed, 0);
                Debug.DrawLine(transform.position, (Vector2)transform.position + jumpForce, Color.blue, 0.4f);
            }
        }
        else if (!isGrounded)
        {
            var pitch = feetAudioSource.pitch;
            feetAudioSource.pitch = 1.2f;
            AudioUtil.InstanstiateAudio(feetAudioSource, transform.position, true, midAirJumpClip);
            feetAudioSource.pitch = pitch;
        }

        rb.AddForce(jumpForce, ForceMode2D.Impulse);

        CancelInvoke("StopJumping");
        Invoke("IncrementCurJump", 0.1f);

        // Invoke("StopJumping", debugStopJumping);

        OnJumpStarted?.Invoke();
        OnGroundedChanged?.Invoke(false);

        return true;
    }

    public void PlayFootstepsSound()
    {
        foreach (Surface surface in lastSurfaces)
        {
            soundComp.PlaySurfaceClip(surface, SurfaceAudioType.Walk);
        }      
    }

    //private void PlaySurfaceClip(Surface surface, SurfaceAudioType type, bool instantiate = true)
    //{
    //    var surfaceAudio = surface.surfaceType.surfaceAudio.Find(audio => audio.Type == type);
    //    if (surfaceAudio == null) return;
    //    if (instantiate) AudioUtil.InstanstiateAudio(feetAudioSource, transform.position, true, surfaceAudio.Clip);
    //    else
    //    {
    //        feetAudioSource.clip = surfaceAudio.Clip;
    //        if (feetAudioSource.isPlaying) return;
    //        feetAudioSource.PlayOneShot(feetAudioSource.clip);
    //    }
    //}

    private void FaceLeft()
    {
        if (Time.deltaTime > 0)
        {
            // var weaponSR = weapon.GetComponentInChildren<SpriteRenderer>();
            // weaponSR.flipY = true;
            // sr.flipX = true;
        }
    }
    private void FaceRight()
    {
        if (Time.deltaTime > 0)
        {
            // var weaponSR = weapon.GetComponentInChildren<SpriteRenderer>();
            // weaponSR.flipY = false;
            // sr.flipX = false;
        }
    }

    private void IncrementCurJump()
    {
        curJumps++;
    }

    private void StopJumping()
    {
        if (curJumps > 0) OnGroundedChanged?.Invoke(true);
        curJumps = 0;
    }

    private bool RaycastGrounded()
    {
        isGrounded = Physics2D.Raycast(GroundedOriginPosition, Vector3.down, skinWidth*2, groundedMask);
        if (isGrounded) StopJumping();
        return isGrounded;
    }

    void OnDrawGizmos()
    {
        if (!col) return;
        Gizmos.DrawLine(GroundedOriginPosition, transform.position + Vector3.down * (col.bounds.extents.y + 0.1f));
    }

    float period = 0.5f;
    bool isJumping = false;

    // Keeps trying to jump across a time period to facilitate jumping quickly after a fall
    IEnumerator _JumpPeriod()
    {
        isJumping = true;
        var t = 0f;
        while (t < period)
        {
            t += Time.deltaTime;
            var success = StartJumping();
            if (success || !Input.GetButton("Jump")) break;
            yield return null;
        }
        isJumping = false;
    }
}
