﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Companion : MonoBehaviour
{
    public SpriteRenderer Body;
    public Weapon Weapon;
    public FollowGameObject Follow;
    // public Transport Transport;
    private Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Attack(GameObject target)
    {
        if (!target) return;
        AimWeapon(target);
        var attack = Weapon.WeaponActivate();
        if (attack) animator.SetTrigger("Attack");
    }

    public void SetTarget(GameObject target)
    {
        Follow.Target = target;
    }

    private void AimWeapon(GameObject target)
    {
        if (!Weapon || !target) return;
        var col = target.GetComponent<Collider2D>();
        if (!col) return;
        var targetPos = col.bounds.center;
        var newRight = targetPos - Weapon.transform.position;
        newRight.z = 0;
        Weapon.transform.right = newRight;
    }
}
