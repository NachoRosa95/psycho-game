﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEvents : MonoBehaviour
{
    public List<UnityEvent> Events = new List<UnityEvent>();

    public void InvokeEvent(int index)
    {
        try
        {
            Events[index].Invoke();
        }
        catch (System.Exception ex)
        {
            //Debug.LogError(ex.Message);
        }
    }
}
