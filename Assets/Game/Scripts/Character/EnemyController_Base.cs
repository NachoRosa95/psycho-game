﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class EnemyController_Base : MonoBehaviour
{
    public UnityEvent onAttack;

    [SerializeField] Animator animator;
    ProjectileWeapon weaponReference;
    bool attacking;
    private bool canAttack = true;

    public Animator Animator { get => animator; set => animator = value; }
    public ProjectileWeapon WeaponReference { get => weaponReference; set => weaponReference = value; }
    public bool CanAttack { get => canAttack; set => canAttack = value; }

    private void Awake()
    {
        if (!animator) animator = GetComponent<Animator>();
        if (!weaponReference) weaponReference = GetComponentInChildren<ProjectileWeapon>();
    }

    public virtual void StartAttack()
    {
        if (weaponReference.OnCooldown || attacking) return;
        Debug.Log("Start attack");
        attacking = true;
        animator.SetTrigger("Attack");
    }

    public virtual void Attack()
    {
        //Debug.Log("Attacking. I'm " + gameObject.name);
        weaponReference.WeaponActivate();
        attacking = false;
        onAttack.Invoke();
    }
}