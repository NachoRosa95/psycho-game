﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController_Bird : EnemyController_Base
{
    public override void StartAttack()
    {
        base.StartAttack();   
        Animator.SetTrigger("Cry");
    }
}
