﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[ExecuteInEditMode]
public class RendererModifier : MonoBehaviour
{
    [SerializeField] Color colorModifier;
    
    [SerializeField, ReadOnly]
    List<SpriteRenderer> list;
    
    Color previousColor;

    private void Awake()
    {
        if (list == null)
        {
            UpdateRenderers();
        }
    }

    private void Update()
    {
        ChangeColors();
    }

    [ContextMenu("Update Renderers")]
    private void UpdateRenderers()
    {
        list = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>());
    }
    
    private void OnValidate()
    {
        if (Application.isPlaying) return;
        ChangeColors();
    }

    void ChangeColors()
    {
        if (colorModifier == previousColor) return;
        foreach (var rend in list)
        {
            rend.color = colorModifier;
        }
        previousColor = colorModifier;
    }
}
