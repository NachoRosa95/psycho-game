﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExecuteAction : MonoBehaviour
{
    [SerializeField] ActionSO action;

    public void Execute()
    {
        action.Execute(gameObject);
    }
}
