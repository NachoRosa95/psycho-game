﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsItem : BehaviourNode
{
    GameObject target;
    FollowGameObject componentReference;
    Vector3 offset;
    [SerializeField] float minDistance;

    public override void Init(BaseAI IA)
    {
        target = IA.Blackboard.GetEntry<GameObject>("closestItem").value;
        if (!componentReference) componentReference = IA.Body.GetComponent<FollowGameObject>();
        offset = new Vector3(-1, 2, 0);
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (target != null)
        {
            if (debug) Debug.Log("Moving towards item");
            componentReference.Target = target;
            componentReference.Offset = offset;

            if (debug) Debug.Log("Position is:" + IA.Body.transform.position + " Desired Position is: " + (componentReference.Target.transform.position + offset));


            Vector3 dist = componentReference.Target.transform.position - IA.Body.transform.position;
            float distMagnitude = dist.magnitude;
            if(distMagnitude <= minDistance) return BehaviourResult.Success;
            else return BehaviourResult.Running;
        }
        else
        {
            if (debug) Debug.Log("Failed to move towards item");
            return BehaviourResult.Failure;
        }
    }
}
