﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionIndicatorComponent : MonoBehaviour
{
    [SerializeField] GameObject indicatorPrefab;
    [SerializeField] Vector3 offset = Vector3.up;
    [SerializeField] float duration;
    GameObject indicatorObject;

    public GameObject IndicatorObject
    {
        get { return indicatorObject; }
    }

    public void Show()
    {
        SpawnIndicator(transform.position + offset, Quaternion.identity);
    }

    public void StopShowing()
    {
        Destroy(indicatorObject);
    }

    void SpawnIndicator(Vector3 position, Quaternion rotation)
    {
        if (!indicatorObject)
        {
            indicatorObject = Instantiate(indicatorPrefab, position, rotation, transform);
            var fadeOut = indicatorObject.GetComponent<Fade>();
            if (fadeOut) fadeOut.Duration = duration;
        }
    }
    
}
