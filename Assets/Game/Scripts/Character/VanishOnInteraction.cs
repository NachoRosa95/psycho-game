﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanishOnInteraction : MonoBehaviour
{
    [SerializeField] bool deactivateHealthComponent;
    [SerializeField] bool deactivateCollider;

    HealthComponent healthComp;
    Collider2D collider;

    VanishObject objectToVanish;

    private void Awake()
    {
        if (deactivateHealthComponent) healthComp = GetComponent <HealthComponent>();
        if (deactivateCollider) collider = GetComponent<Collider2D>();
        objectToVanish = GetComponent<VanishObject>();
    }

    public void VanishObject()
    {
        Debug.Log("Should start vanishing");
        //objectToVanish.ChangeAlpha(0, 2);
        if (healthComp) healthComp.enabled = false;
        if (collider) collider.enabled = false;
    }
}
