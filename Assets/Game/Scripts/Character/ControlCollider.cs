﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCollider : MonoBehaviour
{
    CapsuleCollider2D col;

    private void Awake()
    {
        col = GetComponent<CapsuleCollider2D>();
    }

    public void OnActivated()
    {
        col.enabled = false;
    }
}
