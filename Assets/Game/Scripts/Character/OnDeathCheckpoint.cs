﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnDeathCheckpoint : MonoBehaviour
{
    public bool debug;

    [Space]
    [Header("On Respawn configuration")]
    [SerializeField] float respawnTime;

    CheckpointRegister checkpointRegister;
    CharacterControllerRB2D cc;
    AnimationController animator;

    [SerializeField]
    UnityEvent onDeath_Event;

    [SerializeField]
    UnityEvent onRespawn_Event;

    int initialLayer;

    private void Awake()
    {
        checkpointRegister = GetComponentInChildren<CheckpointRegister>();
        cc = GetComponentInChildren<CharacterControllerRB2D>();
        animator = GetComponentInChildren<AnimationController>();

        initialLayer = gameObject.layer;
    }

    void OnDeath()
    {
        if (debug) Debug.Log("OnDeath");

        if (checkpointRegister != null)
        {
            if (checkpointRegister.LastCheckpoint == null) Debug.LogError("Player has no LastCheckpoint", this);
            else
            {
                //Bloqueo movimiento del jugador y reinicio su velocidad
                cc.BlockMovement = true;
                cc.RB.velocity = Vector2.zero;

                //Cambio el layer para que los enemigos no me ataquen
                gameObject.layer = 0;

                //Inicio la oscuridad
                onDeath_Event.Invoke();

                //Ejecuto la animacion
                animator.Death = true;

                //Invoco el respawn
                Invoke("OnRespawn", respawnTime);
            }
        }

        HealthComponent health = GetComponentInChildren<HealthComponent>();

        if (health != null)
        {
            health.Health = checkpointRegister.LastHealth;
        }
    }

    void OnRespawn()
    {
        //Vuelvo al Idle de animacion
        animator.Death = false;

        //Vuelvo a mi layer original
        gameObject.layer = initialLayer;

        //Traslado al jugador a la posicion
        transform.position = checkpointRegister.LastCheckpoint.transform.position;
        var rb = GetComponent<Rigidbody>();
        if (rb) rb.velocity = Vector3.zero;

        //Apago la oscuridad
        onRespawn_Event.Invoke();

        //Permito el movimiento nuevamente
        cc.BlockMovement = false;
    }
}
