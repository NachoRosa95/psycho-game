﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePet : MonoBehaviour
{
    FollowGameObject followScript;
    BaseAI AI;

    private void Start()
    {
        followScript = GetComponent<FollowGameObject>();
        AI = GetComponentInChildren<BaseAI>();
    }

    public void ActivatePetMovement()
    {
        followScript.enabled = true;
    }

}
