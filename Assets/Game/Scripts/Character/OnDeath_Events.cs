﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnDeath_Events : MonoBehaviour
{
    public UnityEvent Events = new UnityEvent();

    void OnDeath()
    {
        try
        {
            Events.Invoke();
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
        }
    }
}
