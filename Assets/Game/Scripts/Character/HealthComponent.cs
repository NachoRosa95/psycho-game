﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class UnityEvent_float_string : UnityEvent<float, string>
{

}

public class HealthComponent : MonoBehaviour
{
    //[SerializeField] Canvas canvasLife;
    [SerializeField] float maxHealth;
    [SerializeField] float health;
    [SerializeField] string memoryName = "health";
    [SerializeField] float invincibilityTime;
    [SerializeField] bool debug;
    [SerializeField] Animator animator;

    private bool invincible;
    public bool Invincible
    {
        get
        {
            return invincible;
        }
        set
        {
            invincible = value;
            if (animator) animator.SetBool("Invincible", value);
        }
    }

    private UnityEvent_float_string onChanged = new UnityEvent_float_string();
    private float timeLastDamaged;

    public float MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    public float Health
    {
        get { return health; }
        set
        {
            var lastHealth = health;
            health = Mathf.Clamp(value, 0, maxHealth);
            if (OnChanged != null) OnChanged.Invoke(health, memoryName);
            if (health == 0 && lastHealth > 0) SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
        }
    }

    public UnityEvent_float_string OnChanged
    {
        get
        {
            return onChanged;
        }
    }

    private void Start()
    {
        Health = maxHealth;
        animator = GetComponent<Animator>();
    }

    public bool Damage(float amount, GameObject damageDealer)
    {
        if (debug) Debug.Log("Damage: " + amount + " - Invincible: " + Invincible, this);
        if (Invincible || amount <= 0) return false;
        Invincible = true;
        StartCoroutine("doDamageCooldown");
        DirectDamage(amount, damageDealer);
        return true;
    }

    public void DirectDamage(float amount, GameObject damageDealer)
    {
        if (amount <= 0) return;
        if (debug) Debug.Log("DirectDamage: " + amount, this);
        if (animator) animator.SetTrigger("Damaged");
        Health -= amount;
        SendMessage("OnDamage", damageDealer, SendMessageOptions.DontRequireReceiver);
    }

    public void Heal(float amount)
    {
        Health += amount;
        //Debug.Log(health);
    }

    IEnumerator doDamageCooldown()
    {
        yield return new WaitForSeconds(invincibilityTime);
        Invincible = false;
    }
}
