﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transport : MonoBehaviour
{
    Transportable transportableObject;
    //GameObject transportableObject;
    Collider2D col;

    public Transportable TransportableObject
    {
        get { return transportableObject; }
        set { transportableObject = value; }
    }

    public void MakeChildren()
    {
        transportableObject.MakeTransportableYourChildren(transform);
    }

    public void TurnOnCollider()
    {
        GetCollider().enabled = true;
    }

    public void TurnOffCollider()
    {
        GetCollider().enabled = false;
    }

    Collider2D GetCollider()
    {
        return transportableObject.Collider;
    }
}

