﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRegen : MonoBehaviour
{
    HealthComponent healthComponent;
    public float healthRegenTime;
    public float healthRegen;
    public float onDamagedCooldown;

    private bool onCooldown;

    void Awake()
    {
        healthComponent = GetComponent<HealthComponent>();
        InvokeRepeating(nameof(Heal), 0, healthRegenTime);
    }

    void Heal()
    {
        if (healthComponent == null || onCooldown) return;
        healthComponent.Heal(healthRegen);
    }

    void OnDamage()
    {
        // cares about source?
        onCooldown = true;
        StopCoroutine("doCooldown");
        StartCoroutine("doCooldown");
    }
    IEnumerator doCooldown()
    {
        yield return new WaitForSeconds(onDamagedCooldown);
        onCooldown = false;
    }
}
