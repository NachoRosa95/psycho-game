﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionIndicator : MonoBehaviour
{
    [SerializeField] GameObject indicatorPrefab;
    [SerializeField] Vector3 offset = Vector3.up;
    [SerializeField] float duration;
    SpriteRenderer indicatorSR;

    public SpriteRenderer IndicatorSR
    {
        get { return indicatorSR; }
    }

    public void Show(float duration = 0)
    {
        var d = duration == 0 ? this.duration : duration;
        SpawnIndicator(transform.position + offset, Quaternion.identity, d);
    }

    public void StopShowing()
    {
        Destroy(indicatorSR);
    }

    void SpawnIndicator(Vector3 position, Quaternion rotation, float duration)
    {
        if (!indicatorSR) indicatorSR = Instantiate(indicatorPrefab, position, rotation, transform).GetComponent<SpriteRenderer>();
        var fadeOut = indicatorSR.GetComponent<Fade>();
        if (fadeOut) fadeOut.Duration = duration;
        else return;
        if (fadeOut.enabled) fadeOut.Restart();
        else fadeOut.enabled = true;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var crossWidth = 0.2f;
        var origin = (Vector2)transform.position;
        var a = origin + (Vector2)offset + Vector2.one * crossWidth;
        var b = origin + (Vector2)offset - Vector2.one * crossWidth;
        var c = origin + (Vector2)offset + new Vector2(-1, 1) * crossWidth;
        var d = origin + (Vector2)offset + new Vector2(1, -1) * crossWidth;
        Gizmos.DrawLine(a, b);
        Gizmos.DrawLine(c, d);
    }
}
