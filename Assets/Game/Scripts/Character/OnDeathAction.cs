﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDeathAction : MonoBehaviour 
{
	[SerializeField] ActionSO action;

	void OnDeath()
	{
		action.Execute(gameObject);
	}
}