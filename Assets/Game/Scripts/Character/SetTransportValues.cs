﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTransportValues : BehaviourNode
{
    Transport componentReference;

    public override void Init(BaseAI IA)
    {
        if(!componentReference) componentReference = IA.Body.GetComponent<Transport>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if(IA.Blackboard.GetEntry<GameObject>("closestItem").value.GetComponent<Transportable>())
        {
            componentReference.TransportableObject = IA.Blackboard.GetEntry<GameObject>("closestItem").value.GetComponent<Transportable>();
            return BehaviourResult.Success;
        }
        else
        {
            Debug.LogError("Error - Closest Item doesn't have the 'Transportable' component");
            return BehaviourResult.Failure;
        }
        
       
    }
}
