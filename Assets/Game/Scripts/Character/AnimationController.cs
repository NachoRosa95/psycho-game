﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField]
    Animator animator;

    bool facingRight;
    bool moving;
    bool jumping;
    bool attack;
    bool grounded;
    bool death;

    Vector3 normalScale = new Vector3(1, 1, 1);
    Vector3 flippedScale = new Vector3(-1, 1, 1);

    public bool Moving
    {
        get => moving;
        set
        {
            if (value != moving) animator.SetBool("Walking", value);
            moving = value;
        }
    }
    public bool Jumping
    {
        get => jumping;
        set
        {
            if (value == true) animator.SetTrigger("Jump");
            jumping = value;
        }
    }
    public bool Attack
    {
        get => attack;
        set
        {
            if (value == true) animator.SetTrigger("Attack");
            attack = value;
        }
    }
    public bool Grounded
    {
        get => grounded;
        set
        {
            animator.SetBool("Grounded", value);
            grounded = value;
        }
    }

    public bool Death
    {
        get => death;
        set
        {
            animator.SetBool("Death", value);
            death = value;
        }
    }

    private void Awake()
    {
        if (!animator) animator = GetComponent<Animator>();
    }

    private void Update()
    {
        FlipCharacter();
    }

    public void GetHorizontalAxisValue(float value)
    {
        if (value > 0) facingRight = true;
        else if (value < 0) facingRight = false;

        Moving = value != 0;
    }

    void FlipCharacter()
    {
        if (death) return;

        if (facingRight) transform.localScale = normalScale;
        else transform.localScale = flippedScale;
    }
}
