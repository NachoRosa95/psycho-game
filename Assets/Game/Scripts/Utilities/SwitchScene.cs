﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{
    [SerializeField] int sceneID;
    
    public void Switch()
    {
        if (!Application.CanStreamedLevelBeLoaded(sceneID))
        {
            Debug.LogError("The requested scene does not exist or cannot be loaded", this);
            return;
        }
        SceneManager.LoadScene(sceneID, LoadSceneMode.Single);
        Metricas.DoAction(ActionType.SceneChange, sceneID, 1);
    }
}
