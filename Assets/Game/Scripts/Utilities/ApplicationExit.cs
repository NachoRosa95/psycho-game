﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationExit : MonoBehaviour
{

    [SerializeField] KeyCode exitKey;

	void Update ()
    {
	    if(Input.GetKey(exitKey))
        {
            Application.Quit();
        }
	}

}
