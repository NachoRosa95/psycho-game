﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    public float Duration = 1;
    [SerializeField] bool destroyOnEnd;
    [SerializeField] Vector2 alphaDelta;
    SpriteRenderer[] srs;
    float alpha;
    float startingTime;
    public bool debug;

    void Awake()
    {
        srs = GetComponentsInChildren<SpriteRenderer>();
        if (srs.Length == 0) End();
    }

    void OnEnable()
    {
        Restart();
    }

    public void Restart()
    {
        alpha = alphaDelta.x;
        foreach (SpriteRenderer sr in srs)
        {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha);
        }
        startingTime = Time.time;
    }

    void Update()
    {
        if (srs.Length == 0) return;
        UpdateAlpha();
        UpdateColor();

        if (alpha == alphaDelta.y) End();
    }

    void UpdateAlpha()
    {
        var t = (Time.time - startingTime) / Duration;
        alpha = Mathf.Lerp(alphaDelta.x, alphaDelta.y, t);
    }

    void UpdateColor()
    {
        foreach (SpriteRenderer sr in srs)
        {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha);
        }
    }

    void End()
    {
        if (debug)
            Debug.Log(Time.time - startingTime);

        if (destroyOnEnd) Destroy(gameObject);
        else enabled = false;
    }
}
