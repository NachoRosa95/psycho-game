﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeSequence : MonoBehaviour
{
    public float Duration = 1;
    [SerializeField] bool destroyOnEnd;
	[SerializeField] bool loop;
    [SerializeField] List<float> alphaSequence;
    SpriteRenderer[] srs;
    float alpha;
    float startingTime;
    int index;
    public bool debug;

    void Awake()
    {
        srs = GetComponentsInChildren<SpriteRenderer>();
        if (srs.Length == 0) Debug.LogError("Sprite renderers unassigned", this);
    }

    void OnEnable()
    {
        Restart();
		if(loop && destroyOnEnd) Debug.LogWarning("DestroyOnEnd and Loop are incompatible, Loop takes priority", this);
    }

    public void Restart()
    {
        if (alphaSequence.Count == 0) { Debug.LogError("Alpha sequence unassigned", this); return; }
        index = 0;
        alpha = alphaSequence[0];
        SetAlpha(alpha);
        startingTime = Time.time;
    }

    void Update()
    {
        if (srs.Length == 0 || alphaSequence.Count == 0) return;
        UpdateAlpha();
        SetAlpha(alpha);
    }

    void UpdateAlpha()
    {
        if (index + 1 > alphaSequence.Count - 1) { End(); return; }
        var t = (Time.time - startingTime) / Duration * alphaSequence.Count;
        var x = alphaSequence[index];
        var y = alphaSequence[index + 1];
        alpha = Mathf.Lerp(x, y, t);
        if (t >= 1) { index++; startingTime = Time.time; }
    }

    void SetAlpha(float alpha)
    {
        foreach (SpriteRenderer sr in srs)
        {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha);
        }
    }

    void End()
    {
        if (debug)
            Debug.Log(Time.time - startingTime);

		if(loop) Restart();
        else if (destroyOnEnd) Destroy(gameObject);
        else enabled = false;
    }
}
