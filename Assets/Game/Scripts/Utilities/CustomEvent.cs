﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEvent_Generic<T> : UnityEvent<T>
{

}

[System.Serializable]
public class UnityEvent_Bool : UnityEvent<bool>
{

}


[System.Serializable]
public class UnityEvent_Float : UnityEvent<float>
{

}
