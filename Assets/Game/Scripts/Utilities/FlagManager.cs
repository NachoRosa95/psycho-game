﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagManager : MonoBehaviour
{

    // FlagManager instance;


    static List<string> flags = new List<string>();

    private static void Update()
    {
        if (flags == null)
            Debug.LogError("Flag list is null!");
    }
    public static void SetFlag(string key, bool value)
    {
        bool contains;
        contains = flags.Contains(key);

        if (value)
        {
            if (!contains) flags.Add(key);
            else Debug.Log("Already contains the flag " + key);
        }
        else
        {
            if (contains) flags.Remove(key);
            else Debug.Log("Can't remove the flag " + "'" + key + "'" + " because it's not in the list");
        }
    }

    public static bool Contains(string key)
    {
        if (flags.Contains(key)) return true;
        else return false;
    }

    public static void ResetFlags()
    {
        flags.Clear();
    }

}
