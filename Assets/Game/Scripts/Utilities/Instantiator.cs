﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiator : MonoBehaviour
{
    [SerializeField] GameObject prefab;

    public void InstantiateObject()
    {
        //Debug.Log("Instantiated " + prefab.name);
        Instantiate(prefab, transform.position, transform.rotation);
    }
}
