﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEnableInvoke : MonoBehaviour
{
    public UnityEvent onEnable = new UnityEvent();

    private void OnEnable()
    {
        onEnable.Invoke();
    }
}
