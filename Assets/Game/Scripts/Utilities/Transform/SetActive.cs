﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActive : MonoBehaviour
{
    public List<GameObject> ActivateSet;
    public List<GameObject> DeactivateSet;

    void Execute()
    {
        foreach(GameObject go in ActivateSet)
        {
            go.SetActive(true);
        }
        foreach(GameObject go in DeactivateSet)
        {
            go.SetActive(false);
        }
    }
}
