﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepUpright : MonoBehaviour 
{
	void LateUpdate()
	{
		transform.up = Vector3.up;
	}
}
