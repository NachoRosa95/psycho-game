﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;

public class FollowRoute : WaypointEditor
{
    [SerializeField] List<Vector3> waypoints;
    [SerializeField] AnimationCurve ease;
    [SerializeField] bool loopOnEnd;
    [SerializeField] bool reverseOnEnd;
    [SerializeField] float speed = 1f;
    Vector3 initialPosition;
    List<Vector3> curRoute = new List<Vector3>();
    Vector3 curPos;
    Vector3 target;
    Rigidbody rb;
    Rigidbody2D rb2D;
    int index;
    float t;

    [SerializeField] bool startMovementOnEnable = true;
    bool movementStarted;

    public UnityAction OnEnd;

    void Awake()
    {
        initialPosition = transform.position;
        if (ease == null) ease = AnimationCurve.Constant(0, 1, 1);
        rb = GetComponent<Rigidbody>();
        rb2D = GetComponent<Rigidbody2D>();
        initialPosition = transform.position;
    }

    void OnEnable()
    {
        if (!startMovementOnEnable) return;

        StartMovement();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!startMovementOnEnable && !movementStarted)
        {
            StartMovement();
            movementStarted = true;
        }
    }

    void StartMovement()
    {
        index = speed > 0 ? 0 : waypoints.Count - 1;
        t = 0.7f;
        if (waypoints.Count > 0) { curRoute = waypoints; SetTarget(index); }
    }

    // For usage from an external script
    public void SetRoute(List<Vector3> route = null, UnityAction onEnd = null, float speed = 1)
    {
        if (route != null) curRoute = route;
        this.speed = speed;
        OnEnd = onEnd;
        enabled = true;
    }

    void FixedUpdate()
    {
        if (curRoute.Count == 0) return;
        MoveTowards(target);
    }

    void MoveTowards(Vector3 pos)
    {
        t = Mathf.MoveTowards(t, float.MaxValue * Mathf.Sign(speed), speed * Time.deltaTime);
        if (t > 1) { NextTarget(); t %= 1; }
        if (!enabled) return;
        var y = ease.Evaluate(t);
        var newPos = Vector3.Lerp(curPos, target, y);
        if (rb) rb.MovePosition(newPos);
        else if (rb2D) rb2D.MovePosition(newPos);
        else transform.position = newPos;
    }

    void NextTarget()
    {
        index += (int)Mathf.Sign(speed);
        if (index < 0 || index > curRoute.Count - 1) { End(); return; }
        SetTarget(index);
    }

    void SetTarget(int index)
    {
        curPos = transform.position;
        target = initialPosition + curRoute[index];
    }

    void End()
    {
        if (OnEnd != null) OnEnd.Invoke();
        if (reverseOnEnd) { curRoute.Reverse(); }
        if (loopOnEnd) { index = speed > 0 ? -1 : waypoints.Count; }
        else { enabled = false; return; }
        NextTarget();
    }

    public override List<Vector3> GetWaypoints()
    {
        return waypoints;
    }

    public override void SetWaypoints(List<Vector3> value)
    {
        waypoints = value;
    }

    void OnDrawGizmos()
    {
        if(!Application.isPlaying)
            initialPosition = transform.transform.position;
        if (waypoints.Count == 0) return;

        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, Vector3.one * 0.5f);

        Gizmos.color = Color.cyan;
        for (var i = 1; i < waypoints.Count; i++)
        {
            var a = waypoints[i - 1] + initialPosition;
            var b = waypoints[i] + initialPosition;
            DrawArrow(a, b, Vector3.forward, 0.4f);
            Gizmos.DrawCube(a, new Vector3(0.4f, 0.4f, 0));
        }

        Gizmos.color = Color.blue;
        if (loopOnEnd && !reverseOnEnd)
        {
            var a = waypoints[waypoints.Count - 1] + initialPosition;
            var b = waypoints[0] + initialPosition;
            DrawArrow(a, b, Vector3.forward, 0.4f);
            Gizmos.DrawCube(a, new Vector3(0.4f, 0.4f, 0));
        }
    }

    void DrawArrow(Vector3 pos1, Vector3 pos2, Vector3 normal, float width)
    {
        Vector3 right = Vector3.Cross(pos2 - pos1, normal).normalized;
        var pointA = pos1 + right * -width / 2;
        var pointB = pos1 + right * width / 2;
        var pointC = pos2;
        Gizmos.DrawLine(pointA, pointB);
        Gizmos.DrawLine(pointB, pointC);
        Gizmos.DrawLine(pointA, pointC);
    }
}
