﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    #region Exposed fields

    #endregion Exposed fields

    #region Internal fields

    #endregion Internal fields

    #region Properties

    #endregion Properties

    #region Custom Events

    #endregion Custom Events

    #region Events methods

    public void Pause(bool value)
    {
        TimeManager.Instance.Pause(value);
    }

    #endregion Events methods

    #region Public Methods

    #endregion Public Methods

    #region Non Public Methods

    #endregion Non Public Methods
}
