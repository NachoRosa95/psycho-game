﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    #region Exposed fields

    public static TimeManager Instance
    {
        get
        {
            if (!instance)
            {
                var go = GameObject.Find("TimeManager") ?? new GameObject("TimeManager");
                instance = go.GetComponent<TimeManager>() ?? go.AddComponent<TimeManager>();
            }
            return instance;
        }
    }

    #endregion Exposed fields

    #region Internal fields

    private static TimeManager instance;

    #endregion Internal fields

    #region Properties

    #endregion Properties

    #region Custom Events

    #endregion Custom Events

    #region Events methods

    public void Pause(bool value)
    {
        Time.timeScale = value ? 0 : 1;
        //Debug.Log("Pause " + value);
    }

    #endregion Events methods

    #region Public Methods

    #endregion Public Methods

    #region Non Public Methods

    #endregion Non Public Methods
}
