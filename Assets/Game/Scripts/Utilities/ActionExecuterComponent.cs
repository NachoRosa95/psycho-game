﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionExecuterComponent : MonoBehaviour
{
    [SerializeField] List<ActionSO> actions;

    public void ExecuteAction()
    {
        for (int i = 0; i < actions.Count; i++)
        {
            actions[i].Execute();
        }
    }
}
