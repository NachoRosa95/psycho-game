﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScene : MonoBehaviour
{
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelLoaded;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R)) ResetLevel();
    }

    void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        FlagManager.ResetFlags();
        Inventory.Instance.Items.Clear();
    }
}
