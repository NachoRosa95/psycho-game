﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] GameObject target;
    [SerializeField] Vector3 offset;

    private void Update()
    {
        FollowTarget();
    }

    void FollowTarget()
    {
        if (target == null) Debug.LogError("Error - Target is not assigned");

        transform.position = target.transform.position + offset;
    }
}
