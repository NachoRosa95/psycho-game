﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProjectileWeapon : Weapon
{
    [SerializeField] Animator animator;
    [SerializeField] GameObject barrel;
    [SerializeField] GameObject projectile;
    [SerializeField] bool staticProjectile;

    public GameObject Barrel { get => barrel; }

    public override bool WeaponActivate()
    {
        //Debug.Log("Fire");
        if (OnCooldown) return false;
        Fire();
        return true;
    }

    public override void WeaponDeactivate()
    {
        OnCooldown = true;
        CancelInvoke("Fire");
        if (!IsInvoking("StopCooldown"))
            Invoke("StopCooldown", fireRate);
    }
    private void StopCooldown()
    {
        OnCooldown = false;
    }

    private void Fire()
    {
        //Debug.Log("attacking");
        CancelInvoke("StopCooldown");
        Invoke("StopCooldown", fireRate);
        OnCooldown = true;

        if (animator) animator.SetTrigger("Attack");

        if (audioSource)
        {
            var curPitch = audioSource.pitch;
            audioSource.pitch += Random.Range(-0.1f, 0.1f);
            audioSource.PlayOneShot(audioSource.clip);
            audioSource.pitch = curPitch;
        }

        var newProjectile = Instantiate(projectile, barrel.transform.position, barrel.transform.rotation);
        if (staticProjectile) newProjectile.transform.parent = barrel.transform;
        newProjectile.GetComponent<Damager>().Weapon = this;

    }
}
