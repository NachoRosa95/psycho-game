﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float velocity;
    [SerializeField] float range;
    [SerializeField] bool staticProjectile;
    Vector3 startingPos;
    Rigidbody2D rb;

    public float Range { get { return range; } private set { range = value; } }

    private void Awake()
    {
        startingPos = transform.position;
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.right*velocity, ForceMode2D.Impulse);
    }

    private void Update()
    {
        transform.right = rb.velocity;

        if (staticProjectile) transform.rotation = transform.parent.rotation;

        if(Range > 0 && Vector3.Distance(transform.position,startingPos) > Range)
        {
            OnActivated();
        }
        //transform.Translate(new Vector3(0, 0, velocity * Time.deltaTime));
    }

    private void OnActivated()
    {
        Destroy(gameObject);
    }
}
