﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    [SerializeField] GameObject projectile;

    public override bool WeaponActivate()
    {
        if (OnCooldown) return false;
        Fire();
        return true;
    }

    public override void WeaponDeactivate()
    {

    }

    void Fire()
    {
        // var newProjectile = Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, transform.localRotation.eulerAngles.z), transform.parent);
        // if (transform.localRotation.eulerAngles.z == 180) newProjectile.transform.localScale = new Vector3(newProjectile.transform.localScale.x, -1, newProjectile.transform.localScale.z);

        var newProjectile = Instantiate(projectile, transform.position, transform.rotation, transform.parent);
        var damager = newProjectile.GetComponentInChildren<Damager>();
		if(damager) damager.Weapon = this;
        if(audioSource) audioSource.PlayOneShot(audioSource.clip);

		CancelInvoke("StopCooldown");
        Invoke("StopCooldown", fireRate);
        OnCooldown = true;
    }

    void StopCooldown()
    {
        OnCooldown = false;
    }
}
