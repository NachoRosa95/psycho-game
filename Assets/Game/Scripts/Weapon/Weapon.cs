﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] protected float fireRate;
    [SerializeField] protected AudioSource audioSource;

    protected GameObject weaponOwner;
    public GameObject WeaponOwner { get { return weaponOwner; } }
	
    bool onCooldown;

    public bool OnCooldown
    {
        get { return onCooldown; }
        protected set { onCooldown = value; }
    }

    public virtual void Awake()
    {
        if (audioSource == null) audioSource = GetComponent<AudioSource>();

        weaponOwner = transform.parent.gameObject;
    }
	
	public abstract bool WeaponActivate();
	public abstract void WeaponDeactivate();
	
}
