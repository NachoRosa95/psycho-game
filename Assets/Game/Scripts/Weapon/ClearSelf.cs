﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearSelf : MonoBehaviour
{
    [SerializeField] float lifeTime = 1;

    private void Awake()
    {
        if(lifeTime > 0) Invoke("DestroySelf", lifeTime);
    }

    void DestroySelf()
    {
        Destroy(gameObject);
    }
}
