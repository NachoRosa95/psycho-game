﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damager : MonoBehaviour
{
    [SerializeField] float damage = 10;
    [SerializeField] GameObject sparks;
    [SerializeField] bool directDamage;
    [SerializeField] bool canHitTriggers;
    [SerializeField] bool canHitColliders = true;
    public bool debug;

    [SerializeField] int ignoreLayer = 20;

    Weapon weapon;
    public Weapon Weapon { get { return weapon; } set { weapon = value; } }

    Collider2D col;

    void Awake()
    {
        col = GetComponentInChildren<Collider2D>();
        if (!col) Debug.LogError("No collider attached", this);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.isTrigger && !canHitTriggers) return;
        if (other.gameObject.layer == ignoreLayer) return;

        if (debug) Debug.Log("Trigger " + gameObject.layer + " X " + other.gameObject.layer.ToString());

        Hit(other.gameObject);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.isTrigger && !canHitColliders) return;

        if (debug) Debug.Log("Collision " + gameObject.layer + " X " + collision.gameObject.layer.ToString());

        Hit(collision.gameObject);
    }

    void Hit(GameObject other)
    {
        Damageable damageable = other.GetComponentInParent<Damageable>();
        if (damageable != null)
        {
            var owner = weapon == null ? null : weapon.WeaponOwner;
            var success = directDamage ? damageable.DirectDamage(damage, owner) : damageable.Damage(damage, owner);
            if (sparks && success)
            {
                var offset = new Vector3(Random.Range(-other.transform.lossyScale.x / 2, other.transform.lossyScale.x / 2), Random.Range(-other.transform.lossyScale.y / 2, other.transform.lossyScale.y / 2));
                Instantiate(sparks, other.transform.position + offset, transform.rotation);
            }
        }

        SendMessage("OnActivated", SendMessageOptions.DontRequireReceiver);
    }
}
