﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class PowerSource : MonoBehaviour
{
    public enum InvokingMode
    {
        OnStateChange,
        OnEveryStateSet
    }

    [SerializeField] bool state;
    [Tooltip("Should events be invoked even when the State hasn't changed")] public InvokingMode InvokeMode = InvokingMode.OnStateChange;
    public bool State
    {
        get { return state; }
        set
        {
            // If invoke mode is set to OnStateChange and the state hasn't changed no events are invoked
            if(InvokeMode == InvokingMode.OnStateChange && value == state) return;

            // Changing the State of a power source invokes the corresponding event
            if (value && OnActivated != null) OnActivated.Invoke();
            if (!value && OnDeactivated != null) OnDeactivated.Invoke();

            state = value;
        }
    }

    public UnityEvent OnActivated = new UnityEvent();
    public UnityEvent OnDeactivated = new UnityEvent();
}
