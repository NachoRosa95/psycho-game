﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingSymbol : PowerSource
{
    [SerializeField] float rotationSpeed;
    [SerializeField] int maxSteps;
    [SerializeField] int correctStep;

    private int step;
    private float desiredAngle;

    void Awake()
    {
        desiredAngle = GetAngle();
    }

    public void Execute()
    {
        if (!enabled) return;
        step++;
        if (step >= maxSteps) step = 0;

        desiredAngle = GetAngle();
    }

    void Update()
    {
        var newRotationVector = transform.rotation.eulerAngles;
        if (Mathf.Abs(desiredAngle - newRotationVector.z) < 0.01)
        {
            newRotationVector.z = desiredAngle;
        }
        else newRotationVector.z = Mathf.LerpAngle(newRotationVector.z, desiredAngle, rotationSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(newRotationVector);

        // Emits power if configured correctly
        if (step == correctStep % maxSteps) State = true;
        else State = false;
    }

    float GetAngle()
    {
        return step * 360f / maxSteps;
    }
}
