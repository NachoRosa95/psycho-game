﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class PowerGate : PowerSource
{
    [SerializeField] protected List<PowerSource> inputs = new List<PowerSource>();
    [SerializeField, Tooltip("Disables all of its inputs on success")] protected bool disableInputs;
    [SerializeField] protected bool initialState;

    // For gizmos
    List<Transform> activatedOutputs = new List<Transform>();
    List<Transform> deactivatedOutputs = new List<Transform>();

    public List<PowerSource> Inputs { get => inputs; }

    public virtual void Awake()
    {
        State = initialState;
    }

    public virtual bool GetSuccess()
    {
        return true;
    }

    protected void DisableInputs()
    {
        foreach (PowerSource input in inputs)
        {
            input.enabled = false;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "AndIcon.png", true);
        Gizmos.color = Color.yellow;
        foreach (PowerSource input in inputs)
        {
            if (input) Gizmos.DrawLine(transform.position, input.transform.position);
        }

        // Ugly way to update very rarely on low priority operations
        var random = Random.value;
        if (random < 0.005f)
        {
            activatedOutputs = UpdateEventOutputs(OnActivated);
            deactivatedOutputs = UpdateEventOutputs(OnDeactivated);
        }

        Gizmos.color = Color.green;
        foreach (Transform output in activatedOutputs)
        {
            Gizmos.DrawLine(transform.position, output.transform.position);
        }

        Gizmos.color = Color.red;
        foreach (Transform output in deactivatedOutputs)
        {
            Gizmos.DrawLine(transform.position, output.transform.position);
        }
    }

    List<Transform> UpdateEventOutputs(UnityEvent targetEvent)
    {
        var outputs = new List<Transform>();
        var eventCount = targetEvent.GetPersistentEventCount();
        for (var i = 0; i < eventCount; i++)
        {
            var obj = targetEvent.GetPersistentTarget(i);
            if (obj == null) continue;
            var comp = obj as Component;
            if (comp) outputs.Add(comp.transform);
            else
            {
                var go = obj as GameObject;
                if (go) outputs.Add(go.transform);
            }

        }
        return outputs;
    }
}
