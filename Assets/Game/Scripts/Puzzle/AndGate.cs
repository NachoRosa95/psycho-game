﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class AndGate : PowerGate
{
    void Update()
    {
        var success = GetSuccess();
        if (!success)
        {
            // On Failure
            if (State != initialState) State = initialState;
        }
        else
        {
            // On Success
            State = !initialState;
            if (disableInputs) DisableInputs();
        }
    }

    public override bool GetSuccess()
    {
        if (inputs.Any(input => input != null)) return inputs.All(input => input == null || input.State);
        else return false;
    }
}
