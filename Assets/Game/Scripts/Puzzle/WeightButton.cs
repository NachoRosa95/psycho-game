﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeightButton : PuzzleButton
{
    [SerializeField] float requiredMass;
    [SerializeField] Text label;
    private List<Rigidbody2D> bodies = new List<Rigidbody2D>();

    void OnTriggerEnter2D(Collider2D collider)
    {
        var rb = collider.GetComponent<Rigidbody2D>();
        if (!rb || rb.isKinematic || bodies.Contains(rb)) return;
        bodies.Add(rb);
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        var rb = collider.GetComponent<Rigidbody2D>();
        if (!rb || rb.isKinematic || !bodies.Contains(rb)) return;
        bodies.Remove(rb);
    }

    void OnDisable()
    {
        if (label) label.text = "(Disabled)";
    }

    void Update()
    {
        var totalMass = 0f;
        foreach (Rigidbody2D rb in bodies)
        {
            if(!rb) continue;
            totalMass += rb.mass;
        }
        State = totalMass >= requiredMass;
        if (label) label.text = string.Format("State {0}\nRequired: {1}\nOn Scale: {2}", State, requiredMass, totalMass);
    }
}
