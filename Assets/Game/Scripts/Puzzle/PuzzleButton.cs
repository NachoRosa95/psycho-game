﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PuzzleButton : PowerSource
{
    [SerializeField, Tooltip("If enabled, all cooldowns will be ignored and state will toggle on activation")] bool useToggle;
    [SerializeField, Tooltip("Time it stays on when activated")] float onTime = 1;
    [SerializeField, Tooltip("Time it stays disabled when deactivated")] float disabledTime = 0;
    [SerializeField, Tooltip("On Awake, State will be set to this and the corresponding event will be invoked ")] bool initialState = false;
    bool cooldown;
    Interactable interact;

    void Awake()
    {
        interact = GetComponent<Interactable>();
        State = initialState;
    }

    void OnEnable()
    {
        if (interact) interact.enabled = true;
    }

    void OnDisable()
    {
        if (interact) interact.enabled = false;
    }

    public void Activate()
    {
        if (!enabled) return;

        // If button is functioning as a toggle switch, all cooldowns are ignored
        if (useToggle) { State = !State; return; }

        if (cooldown) return;
        cooldown = true;

        State = !initialState;
        if (interact) interact.enabled = false;
        StartCoroutine("DoOnCooldown");
    }

    public void Deactivate()
    {
        if (!enabled) return;

        State = initialState;
        if (interact) interact.enabled = true;
        StartCoroutine("DoDisabledCooldown");
    }

    IEnumerator DoOnCooldown()
    {
        yield return new WaitForSeconds(onTime);
        Deactivate();
    }

    IEnumerator DoDisabledCooldown()
    {
        yield return new WaitForSeconds(disabledTime);
        cooldown = false;
    }
}
