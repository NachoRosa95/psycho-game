﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(HealthComponent))]
public class SplitMass : MonoBehaviour
{
    public int charges = 1;
    [SerializeField] Vector3 offset = new Vector3(1, 0, 0);
    [SerializeField] int pieces = 2;
    [SerializeField] float cooldown = 0.5f;
    [SerializeField] Transform parentOfCopy;
    SplitMass copy;
    Rigidbody2D body;
    HealthComponent health;
    bool init;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        health = GetComponent<HealthComponent>();
        Invoke("Init", cooldown);
    }

    public void OnDestroyed()
    {
        Invoke("Execute", 0.1f);
    }

    public void Execute()
    {
        if (!init || charges <= 0) return;
        var origin = transform.position - offset * pieces / 2f;
        var newMass = body.mass / pieces;
        var newScale = GetScaleFromMass(newMass);
        for (var i = 0; i < pieces; i++)
        {
            var copy = Instantiate(this, origin + offset * i, transform.rotation, parentOfCopy);
            copy.transform.localScale = newScale;
            copy.GetComponent<Rigidbody2D>().mass = newMass;
            copy.GetComponent<HealthComponent>().Invincible = true; ;
            copy.charges = charges - 1;
        }
        Destroy(gameObject);
    }

    Vector3 GetScaleFromMass(float mass)
    {
        var side = Mathf.Sqrt(mass);
        return new Vector3(side, side, 1);
    }

    void Init()
    {
        if (charges > 0) health.Invincible = false;
        init = true;
    }
}
