﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SequenceGate : PowerGate
{
    [SerializeField] List<int> sequence;
    private List<bool> previousStates = new List<bool>();
    int curSequenceIndex = 0;
    bool success;

    public override void Awake()
    {
        base.Awake();
        InitStates();
        if (sequence.Count == 0) Debug.LogError("Sequence unassigned", this);
    }

    void InitStates()
    {
        previousStates = new List<bool>();
        foreach (PowerSource input in inputs)
        {
            previousStates.Add(input.State);
        }
    }

    void Update()
    {
        if (!success) success = GetSuccess();
        if (!success)
        {
            // On Failure
            if (State != initialState) State = initialState;
        }
        else
        {
            // On Success
            State = !initialState;
            if (disableInputs) DisableInputs();
        }
    }

    public override bool GetSuccess()
    {
        if (previousStates.Count != inputs.Count)
        {
            InitStates();
        }
        for (var i = 0; i < inputs.Count; i++)
        {
            var input = inputs[i];
            var previousState = previousStates[i];
            previousStates[i] = input.State;
            if (input && input.State && !previousState)
            {
                return ActivateSequence(i);
            }
        }
        return false;
    }

    bool ActivateSequence(int index)
    {
        if (index != sequence[curSequenceIndex])
        {
            curSequenceIndex = 0;
            return false;
        }
        curSequenceIndex++;
        if (curSequenceIndex < sequence.Count) return false;
        return true;
    }
}
