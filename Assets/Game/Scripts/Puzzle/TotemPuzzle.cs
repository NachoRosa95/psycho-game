﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotemPuzzle : MonoBehaviour
{
    List<AttackSwitch> totems = new List<AttackSwitch>();
    List<EnemyController_Base> controllers = new List<EnemyController_Base>();

    [SerializeField] float cooldownBetweenAttacks;
    float lastTimeAttacked = 0;
    [SerializeField] float cooldownBetweenRounds;
    float lastTimeRound = 0;
    int currentTotem = 0;

    bool allowAttack;

    public int CurrentTotem 
    { 
        get => currentTotem;
        set 
        {
            currentTotem = value;
            if (currentTotem < 0 || currentTotem > controllers.Count - 1) 
            {
                currentTotem = 0;
                lastTimeRound = Time.time;
            }
        }
    }

    private void Awake()
    {
        var array = GetComponentsInChildren<AttackSwitch>();
        totems.AddRange(array);

        for (int i = 0; i < totems.Count; i++)
        {
            controllers.Add(totems[i].GetComponent<EnemyController_Base>());
        }
        if (controllers.Count <= 0) Debug.LogError("No totems found");

        currentTotem = 0;
        lastTimeAttacked = 0;
        lastTimeRound = 0;
    }

    private void Update()
    {
        if (!allowAttack) return;
        AttackRound();
    }

    public void AttackRound()
    {
        if (Time.time < lastTimeRound + cooldownBetweenRounds) return;
        if (Time.time < lastTimeAttacked + cooldownBetweenAttacks) return;

        lastTimeAttacked = Time.time;
        //Debug.Log("Starting attack: " + controllers[currentTotem].gameObject.name);
        if (controllers[currentTotem].CanAttack)
        {
            Debug.Log("Entra");
            controllers[currentTotem].StartAttack();
        }
        CurrentTotem++;
    }

    public void ResetPuzzle()
    {
        allowAttack = true;

        for (int i = 0; i < totems.Count; i++)
        {
            totems[i].Deactivate();
        }

        lastTimeRound = Time.time;
        lastTimeAttacked = Time.time;

    }
}
