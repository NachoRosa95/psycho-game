﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp_SoundComponent : MonoBehaviour
{
    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    //public void PlaySound_Footsteps()
    //{
    //    int random = Random.Range(0, 1);

    //    if (random != 0) audioSource.PlayOneShot(footstepClip1, 1);
    //    else audioSource.PlayOneShot(footstepClip2, 1);
    //}

    public void PlaySurfaceClip(Surface surface, SurfaceAudioType type)
    {
        var surfaceAudioList = surface.surfaceType.surfaceAudio.FindAll(audio => audio.Type == type);
        if (surfaceAudioList == null) return;

        int random = Random.Range(0, surfaceAudioList.Count);



        audioSource.clip = surfaceAudioList[random].Clip;
        //if (audioSource.isPlaying) return;
        audioSource.PlayOneShot(audioSource.clip, 0.5f);
    }
}
