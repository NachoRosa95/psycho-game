﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioUtil
{
    // copies audiosource properties to temp audiosource for playing at a position
    public static AudioSource InstanstiateAudio(AudioSource original, Vector3 pos, bool oneShot = false, AudioClip clip = null)
    {
        var tempClip = clip ?? original.clip;
        if (!tempClip) return null;
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = pos; // set its position
        AudioSource tempASource = tempGO.AddComponent<AudioSource>(); // add an audio source
        tempASource.clip = tempClip;
        if (original)
        {
            tempASource.outputAudioMixerGroup = original.outputAudioMixerGroup;
            tempASource.mute = original.mute;
            tempASource.bypassEffects = original.bypassEffects;
            tempASource.bypassListenerEffects = original.bypassListenerEffects;
            tempASource.bypassReverbZones = original.bypassReverbZones;
            tempASource.playOnAwake = original.playOnAwake;
            tempASource.loop = original.loop;
            tempASource.priority = original.priority;
            tempASource.volume = original.volume;
            tempASource.pitch = original.pitch;
            tempASource.panStereo = original.panStereo;
            tempASource.spatialBlend = original.spatialBlend;
            tempASource.reverbZoneMix = original.reverbZoneMix;
            tempASource.dopplerLevel = original.dopplerLevel;
            tempASource.rolloffMode = original.rolloffMode;
            tempASource.minDistance = original.minDistance;
            tempASource.spread = original.spread;
            tempASource.maxDistance = original.maxDistance;
        }
        else
        {
            Debug.LogWarning("Audio instantiated without an AudioSource reference. Default values applied.");
        }
        // set other aSource properties here, if desired
        if (oneShot) tempASource.PlayOneShot(tempASource.clip);
        else tempASource.Play(); // start the sound
        MonoBehaviour.Destroy(tempGO, tempASource.clip.length); // destroy object after clip duration (this will not account for whether it is set to loop)
        return tempASource; // return the AudioSource reference
    }

    public static float GetPitchFromTone(int tone, int transpose)
    {
        var pitch = Mathf.Pow(2, (tone + transpose) / 12f);
        return pitch;
    }
}
