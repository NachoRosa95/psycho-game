﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTest : MonoBehaviour
{
    [SerializeField] AudioClip clip;
    [SerializeField] float rate;
    [SerializeField] bool oneShot;
    [SerializeField] bool instantiate;
    float t;
    AudioSource thisSource;

    void Awake()
    {
        thisSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        t += Time.deltaTime;
        if (t < rate) return;
        t %= rate;
        PlayClip();
    }

    void PlayClip()
    {
        if (thisSource)
        {
            if(instantiate) PlayClipAtPoint(thisSource, transform.position, oneShot);
            else if (oneShot) thisSource.PlayOneShot(clip);
            else
            {
                var oldClip = thisSource.clip;
                thisSource.clip = clip;
                thisSource.Play();
                thisSource.clip = oldClip;
            }
        }
        else AudioSource.PlayClipAtPoint(clip, transform.position);
    }

    // copies audiosource properties to temp audiosource for playing at a position
    public static AudioSource PlayClipAtPoint(AudioSource audioSource, Vector3 pos, bool oneShot = false)
    {
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = pos; // set its position
        AudioSource tempASource = tempGO.AddComponent<AudioSource>(); // add an audio source
        tempASource.clip = audioSource.clip;
        tempASource.outputAudioMixerGroup = audioSource.outputAudioMixerGroup;
        tempASource.mute = audioSource.mute;
        tempASource.bypassEffects = audioSource.bypassEffects;
        tempASource.bypassListenerEffects = audioSource.bypassListenerEffects;
        tempASource.bypassReverbZones = audioSource.bypassReverbZones;
        tempASource.playOnAwake = audioSource.playOnAwake;
        tempASource.loop = audioSource.loop;
        tempASource.priority = audioSource.priority;
        tempASource.volume = audioSource.volume;
        tempASource.pitch = audioSource.pitch;
        tempASource.panStereo = audioSource.panStereo;
        tempASource.spatialBlend = audioSource.spatialBlend;
        tempASource.reverbZoneMix = audioSource.reverbZoneMix;
        tempASource.dopplerLevel = audioSource.dopplerLevel;
        tempASource.rolloffMode = audioSource.rolloffMode;
        tempASource.minDistance = audioSource.minDistance;
        tempASource.spread = audioSource.spread;
        tempASource.maxDistance = audioSource.maxDistance;
        // set other aSource properties here, if desired
        if(oneShot) tempASource.PlayOneShot(tempASource.clip);
        else tempASource.Play(); // start the sound
        MonoBehaviour.Destroy(tempGO, tempASource.clip.length); // destroy object after clip duration (this will not account for whether it is set to loop)
        return tempASource; // return the AudioSource reference
    }
}
