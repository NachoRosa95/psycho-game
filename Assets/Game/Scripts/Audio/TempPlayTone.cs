﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempPlayTone : MonoBehaviour
{
    [SerializeField] int tone;
    [SerializeField] int transpose;
    [SerializeField] AudioSource audioSource;

    void Awake()
    {
        audioSource.pitch = AudioUtil.GetPitchFromTone(tone, transpose);
    }

    public void Execute()
    {
        audioSource.pitch = AudioUtil.GetPitchFromTone(tone, transpose);
        audioSource.PlayOneShot(audioSource.clip);
    }
}
