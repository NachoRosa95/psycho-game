﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Surface", menuName = "Scriptable Object/Surface", order = 1)]
public class SurfaceTypeSO : ScriptableObject
{
    public string Name;
    public List<SurfaceAudio> surfaceAudio;
}

[System.Serializable]
public class SurfaceAudio
{
    public SurfaceAudioType Type;
    public AudioClip Clip;
}

[System.Serializable]
public enum SurfaceAudioType
{
    Walk, // Should be a looping sound and the tempo should match the walking cycle
    Jump, // Fired when jumping off this surface
    Land, // Fired when landing on this surface
    Drag  // Fired when an object is dragged across this surface ?
}