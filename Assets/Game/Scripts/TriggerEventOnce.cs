﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEventOnce : MonoBehaviour
{
    public UnityEvent onTriggerEnter;

    bool canActivate = true;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!canActivate) return;

        onTriggerEnter.Invoke();
        canActivate = false;
    }
}
