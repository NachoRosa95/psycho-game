﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDoorState : MonoBehaviour
{
    [SerializeField] SpriteRenderer lockedDoor;
    [SerializeField] SpriteRenderer closedDoor;
    [SerializeField] GameObject openDoor;
    [SerializeField] Collider2D thisCollider;
    [SerializeField] Interactable interactable;

    [SerializeField] SpriteRenderer outsideLockedDoor;
    [SerializeField] SpriteRenderer outsideClosedDoor;
    [SerializeField] GameObject outsideOpenDoor;

    private void Start()
    {
        openDoor.SetActive(false);
        if(outsideOpenDoor) outsideOpenDoor.SetActive(false);
    }

    public void OpenDoor()
    {
        openDoor.SetActive(true);
        if(outsideOpenDoor) outsideOpenDoor.SetActive(true);

        thisCollider.enabled = false;
        closedDoor.enabled = false;
        if (outsideClosedDoor) outsideClosedDoor.enabled = false;

        interactable.enabled = false;
    }

    public void UnlockDoor()
    {
        lockedDoor.enabled = false;
        if (outsideOpenDoor) outsideLockedDoor.enabled = false;

        closedDoor.enabled = true;
        if (outsideClosedDoor) outsideClosedDoor.enabled = enabled;
    }


}
