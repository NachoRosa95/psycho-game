﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGameObject : MonoBehaviour
{
    public GameObject Target;
    GameObject originalTarget;

    [SerializeField] string playerTag = "Player";
    [SerializeField] float speed;
    [SerializeField] float stoppingDistance;
    [SerializeField] Vector2 offset;
    Vector3 originalOffset;
    [SerializeField] SpriteRenderer sr;
    [SerializeField] Rigidbody2D rb;

    public Vector3 Offset
    {
        get { return offset; }
        set { offset = value; }
    }

    public float StoppingDistance
    {
        get { return stoppingDistance; }
    }

    void Awake()
    {
        if (!sr) sr = GetComponentInChildren<SpriteRenderer>();
        if (!rb) rb = GetComponent<Rigidbody2D>();
        originalTarget = Target;
        originalOffset = offset;
    }

    // Valor de ajuste para que la velocidad de Lerp se asemeje a la de MoveTowards
    private const float lerpMultiplier = 0.2f;

    void Update()
    {
        if (Target == null)
        {
            Target = originalTarget;
            if (originalTarget == null)
                Debug.LogError("Target unassigned", this); return;
        }

        Vector2 targetPos = Target.transform.position;

        if (Time.deltaTime > 0)
        {
            if (targetPos.x < transform.position.x) FlipX(true);
            else FlipX(false);
        }

        Vector3 newPos = transform.position;
        var dist = Vector3.Distance(transform.position, targetPos);
        if (dist > stoppingDistance)
        {
            if (Target.tag == playerTag)
            {
                targetPos += offset;
                offset = originalOffset;
                // Si el target es player la velocidad varia con la distancia para que la mascota no se quede atras
                newPos = Vector3.Lerp(transform.position, targetPos, speed * lerpMultiplier * Time.deltaTime);
            }
            else
            {
                // Si el target no es player la velocidad es constante
                newPos = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
            }
        }
        if (rb) rb.MovePosition(newPos);
        else transform.position = newPos;
    }

    void FlipX(bool value)
    {
        // sr.flipX = value;
        var scale = transform.localScale;
        scale.x = Mathf.Abs(scale.x);
        if (value) scale.x = -scale.x;
        transform.localScale = scale;
    }

}
