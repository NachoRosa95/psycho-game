﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableDialogueNode : BehaviourNode
{
    Interactable interactableReference;

    public override void Init(BaseAI IA)
    {
        if(!interactableReference) interactableReference = IA.Body.GetComponentInChildren<Interactable>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        interactableReference.enabled = false;
        return BehaviourResult.Success;
    }
}
