﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindClosestDestructibleNode : BehaviourNode
{
    List<Collider2D> objectsList;
    Collider2D closestObject;

    public override void Init(BaseAI IA)
    {
        objectsList = IA.Blackboard.GetEntry<List<Collider2D>>("detectedObjectList").value;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        float closestDistanceUntilNow = Mathf.Infinity;

        for (int i = 0; i < objectsList.Count; i++)
        {
            var curObject = objectsList[i];
            if (curObject == null || curObject.GetComponent<Destructible>() == null) continue;

            if (curObject != null)
            {
                if(objectsList[i].gameObject.GetComponent<HealthComponent>())
                {
                    float distance = Vector3.Distance(IA.Body.transform.position, curObject.transform.position);
                    if (closestDistanceUntilNow > distance)
                    {
                        closestDistanceUntilNow = distance;
                        closestObject = curObject;
                    }
                }
            }

        }
        

        if (closestObject != null)
        {
            IA.Blackboard.GetEntry<GameObject>("target").value = closestObject.gameObject;
            return BehaviourResult.Success;
        }

        return BehaviourResult.Failure;
    }
}
