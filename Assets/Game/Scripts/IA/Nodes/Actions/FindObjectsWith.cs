﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindObjectsWith : BehaviourNode
{
    [SerializeField] string inputListKey = "";
    [SerializeField] string outputListKey = "";
    [SerializeField] string requiredComponentType;
    [SerializeField] string requiredTag;
    private MemoryEntry<List<GameObject>> output;
    private MemoryEntry<List<Collider2D>> objectsEntry;
    private bool init;

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.GetComponent<Blackboard>();
        objectsEntry = bb.GetEntry<List<Collider2D>>(inputListKey);
        output = bb.GetEntry<List<GameObject>>(outputListKey);
        output.value = new List<GameObject>();
        init = true;
    }

    public override BehaviourResult Execute(BaseAI baseAI)
    {
        if (!init || objectsEntry.value == null) return BehaviourResult.Failure;

        output.value.Clear();
        foreach (Collider2D col in objectsEntry.value)
        {
            if (col == null) continue;
            if (requiredComponentType != "")
            {
                MonoBehaviour comp = col.gameObject.GetComponentInChildren(Type.GetType(requiredComponentType)) as MonoBehaviour;

                if (!comp || !comp.enabled) continue;
            }
            if (requiredTag != "")
            {
                if (col.gameObject.tag != requiredTag) continue;
            }
            if(debug) Debug.Log("Found object " + col.gameObject);
            output.value.Add(col.gameObject);
            if (debug) Debug.Log("Adding " + col.gameObject.name + " to " + outputListKey);
        }

        if (output.value.Count == 0) return BehaviourResult.Failure;
        return BehaviourResult.Success;
    }
}

