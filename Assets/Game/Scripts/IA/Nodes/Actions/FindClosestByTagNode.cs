﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindClosestByTagNode : BehaviourNode
{
    List<Collider2D> objectsList;
    Collider2D closestObject;

    [SerializeField] string objectiveTag;

    public override void Init(BaseAI IA)
    {
        objectsList = IA.Blackboard.GetEntry<List<Collider2D>>("detectedObjectList").value;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        float closestDistanceUntilNow = Mathf.Infinity;

        for (int i = 0; i < objectsList.Count; i++)
        {
            if(objectsList[i] != null)
            {
                if (objectsList[i].tag == objectiveTag)
                {
                    float distance = Vector3.Distance(IA.Body.transform.position, objectsList[i].transform.position);
                    if (closestDistanceUntilNow > distance)
                    {
                        closestDistanceUntilNow = distance;
                        closestObject = objectsList[i];
                    }
                }
            }
            
        }

        if (closestObject != null)
        {
            IA.Blackboard.GetEntry<GameObject>("target").value = closestObject.gameObject;
            return BehaviourResult.Success;
        }

        return BehaviourResult.Failure;
    }
}
