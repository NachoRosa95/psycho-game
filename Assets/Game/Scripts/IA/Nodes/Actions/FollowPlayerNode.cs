﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerNode : BehaviourNode
{
    public GameObject Target;

    [SerializeField] string playerTag = "Player";
    [SerializeField] float speed;
    [SerializeField] Vector3 offset;
    SpriteRenderer sr;

    // Valor de ajuste para que la velocidad de Lerp se asemeje a la de MoveTowards
    private const float lerpMultiplier = 2;

    public override void Init(BaseAI IA)
    {
        if (!sr) sr = IA.Body.GetComponent<SpriteRenderer>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (Target == null)
        {
            Debug.LogError("Companion.cs - Target unassigned", this);
            return BehaviourResult.Failure;
        }

        Vector2 targetPos = Target.transform.position + offset;

        if (targetPos.x < IA.Body.transform.position.x) sr.flipX = true;
        else sr.flipX = false;

        if (Target.tag == playerTag)
        {
            // Si el target es player la velocidad varia con la distancia para que la mascota no se quede atras
            IA.Body.transform.position = Vector3.Lerp(IA.Body.transform.position, targetPos, speed * lerpMultiplier * Time.deltaTime);
        }
        else
        {
            // Si el target no es player es la velocidad es constante
            IA.Body.transform.position = Vector3.MoveTowards(IA.Body.transform.position, targetPos, speed * Time.deltaTime);
        }

        return BehaviourResult.Success;
    }

}
