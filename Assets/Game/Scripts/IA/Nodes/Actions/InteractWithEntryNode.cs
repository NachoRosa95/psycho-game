﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractWithEntryNode : BehaviourNode
{
    [SerializeField] string key;
    MemoryEntry<GameObject> goEntry;

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.Blackboard;
        goEntry = bb.GetEntry<GameObject>(key);
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        var go = goEntry.value;
        if (!go) return BehaviourResult.Failure;

        var interact = go.GetComponentInChildren<Interactable>();
        if (!interact) return BehaviourResult.Failure;

        interact.Interact();
        return BehaviourResult.Success;
    }

}
