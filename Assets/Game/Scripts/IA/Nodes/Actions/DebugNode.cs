﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugNode : BehaviourNode
{
    [SerializeField] string message;
    public override BehaviourResult Execute(BaseAI IA)
    {
        Debug.Log(message);
        return BehaviourResult.Success;
    }
}
