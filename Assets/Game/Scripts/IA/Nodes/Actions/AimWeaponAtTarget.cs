﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimWeaponAtTarget : BehaviourNode
{
    GameObject target;
    GameObject weaponBarrel;

    Vector3 offset = new Vector3(0,1.5f,0);

    public override void Init(BaseAI IA)
    {
        target = IA.Blackboard.GetEntry<GameObject>("target").value;
        if (!target) Debug.LogError("Target is null, can't aim at him", this);

        var enemyAI = IA as EnemyIA;
        weaponBarrel = enemyAI.Body.GetComponentInChildren<ProjectileWeapon>().Barrel;
        if (!weaponBarrel) Debug.LogError("Can't find weapon barrel", this);
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        Vector3 dir = target.transform.position - weaponBarrel.transform.position;
        weaponBarrel.transform.right = dir + offset;
        //weaponBarrel.transform.rotation = Quaternion.LookRotation(Vector3.forward, dir);

        return BehaviourResult.Success;
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.red;

        //Vector3 dir = target.transform.position - weaponBarrel.transform.position;
        //Gizmos.DrawLine(weaponBarrel.transform.position, dir);
    }
}
