﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipNode : BehaviourNode
{
    [SerializeField] string targetKey = "target";
    [SerializeField] bool invert = false;
    ProjectileWeapon weapon;
    SpriteRenderer sr;
    MemoryEntry<GameObject> targetEntry;

    public override void Init(BaseAI IA)
    {
        if (!weapon)
            weapon = IA.Blackboard.GetEntry<ProjectileWeapon>("weapon").value;

        if (targetEntry == null)
            targetEntry = IA.Blackboard.GetEntry<GameObject>(targetKey);

        sr = IA.Body.GetComponent<SpriteRenderer>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (Time.deltaTime <= 0) return BehaviourResult.Failure;
        var target = targetEntry.value;
        if (target == null) return BehaviourResult.Failure;
        if (target.transform.position.x < transform.position.x) TurnLeft(IA);
        else TurnRight(IA);
        WeaponAimAdjustment(IA);
        return BehaviourResult.Success;
    }

    void WeaponAimAdjustment(BaseAI IA)
    {
        var target = targetEntry.value;
        var col = target.GetComponent<Collider2D>();
        var targetPos = col.bounds.center;
        var newRight = targetPos - weapon.transform.position;
        newRight.z = 0;
        weapon.transform.right = newRight;
    }

    void TurnRight(BaseAI AI)
    {
        AI.Body.transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    void TurnLeft(BaseAI AI)
    {
        AI.Body.transform.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
}
