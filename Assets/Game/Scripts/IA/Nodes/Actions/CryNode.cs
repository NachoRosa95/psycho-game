﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryNode : BehaviourNode
{
    AudioSource audioSource;


    public override void Init(BaseAI IA)
    {
        audioSource = GetComponentInParent<AudioSource>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (audioSource != null)
        {
            audioSource.pitch = 1 + Random.Range(-0.1f, 0.1f);

            audioSource.PlayOneShot(audioSource.clip);
            return BehaviourResult.Success;
        }
        else
        {
            Debug.LogError("AudioSource is null!");
            return BehaviourResult.Failure;
        }
    }

}
