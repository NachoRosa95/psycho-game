﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindItemsNode : BehaviourNode
{
    List<Collider2D> objectsList;
    Collider2D closestObject;

    public override void Init(BaseAI IA)
    {
        objectsList = IA.Blackboard.GetEntry<List<Collider2D>>("detectedObjectList").value;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        float closestDistanceUntilNow = Mathf.Infinity;

        for (int i = 0; i < objectsList.Count; i++)
        {
            if (objectsList[i] != null)
            {
                if (objectsList[i].gameObject.GetComponent<Transportable>())
                {
                    float distance = Vector3.Distance(IA.Body.transform.position, objectsList[i].transform.position);
                    if (closestDistanceUntilNow > distance)
                    {
                        closestDistanceUntilNow = distance;
                        closestObject = objectsList[i];
                    }
                }
            }
        }


        if (closestObject != null)
        {
            if (debug) Debug.Log("Item Found");

            IA.Blackboard.GetEntry<GameObject>("closestItem").value = closestObject.gameObject;
            return BehaviourResult.Success;
        }
        else
        {
            if (debug) Debug.Log("Item Not Found");
            return BehaviourResult.Failure;
        }

    }
}
