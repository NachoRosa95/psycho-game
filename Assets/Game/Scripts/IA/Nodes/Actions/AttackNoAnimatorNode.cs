﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackNoAnimatorNode : BehaviourNode
{
    ProjectileWeapon weaponReference;


    public override void Init(BaseAI IA)
    {
        if (!weaponReference) weaponReference = IA.Body.GetComponentInChildren<ProjectileWeapon>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        weaponReference.WeaponActivate();
        return BehaviourResult.Success;
    }
}
