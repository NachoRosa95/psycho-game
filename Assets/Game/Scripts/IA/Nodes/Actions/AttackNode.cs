﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackNode : BehaviourNode
{
    EnemyController_Base controller;

    public override void Init(BaseAI IA)
    { 
        if (!controller) controller = IA.Body.GetComponent<EnemyController_Base>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        controller.StartAttack();
        return BehaviourResult.Success;
    }

}