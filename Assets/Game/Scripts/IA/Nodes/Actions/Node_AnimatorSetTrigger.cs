﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node_AnimatorSetTrigger : BehaviourNode
{
    Animator animator;
    [SerializeField] string animatorTriggerName;

    public override void Init(BaseAI IA)
    {
        if (!animator) animator = IA.Body.GetComponentInChildren<Animator>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (debug) Debug.Log("Activating trigger: " + animatorTriggerName, this);
        animator.SetTrigger(animatorTriggerName);
        return BehaviourResult.Success;
    }
}
