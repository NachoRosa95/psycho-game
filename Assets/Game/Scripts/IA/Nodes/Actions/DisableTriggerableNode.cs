﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableTriggerableNode : BehaviourNode
{
    Triggerable2D triggerableReference;

    public override void Init(BaseAI IA)
    {
        if (!triggerableReference) triggerableReference = IA.Body.GetComponentInChildren<Triggerable2D>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        triggerableReference.enabled = false;
        return BehaviourResult.Success;
    }
}
