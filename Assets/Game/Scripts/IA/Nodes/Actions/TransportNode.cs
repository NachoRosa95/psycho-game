﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportNode : BehaviourNode
{
    Transport componentReference;

    public override void Init(BaseAI IA)
    {
        if (!componentReference) componentReference = IA.Body.GetComponent<Transport>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        componentReference.MakeChildren();
        return BehaviourResult.Success;
    }
}
