﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopShootingNode : BehaviourNode
{
    Animator animatorReference;

    public override void Init(BaseAI IA)
    {
        if (!animatorReference) animatorReference = IA.Animator;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if(animatorReference.runtimeAnimatorController != null) animatorReference.SetTrigger("LookAround");
        return BehaviourResult.Success;
    }
}
