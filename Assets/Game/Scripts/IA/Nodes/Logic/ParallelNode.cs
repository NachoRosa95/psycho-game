﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallelNode : BehaviourNode
{
    List<BehaviourNode> children = new List<BehaviourNode>();

    public override void Init(BaseAI IA)
    {
        children = new List<BehaviourNode>();
        for (int i = 0; i < transform.childCount; i++)
        {
            var node = transform.GetChild(i).GetComponent<BehaviourNode>();
            if (node && node.isActiveAndEnabled) children.Add(node);
            node.Init(IA);
        }
    }

    // Returns Success if all its members returned Success, executes them all regardless
    public override BehaviourResult Execute(BaseAI IA)
    {
        var result = BehaviourResult.Success;
        for (int i = 0; i < children.Count; i++)
        {
            var child = children[i];
            if (debug) Debug.Log(child);
            if (!child.isActiveAndEnabled) continue;
            BehaviourResult childResult = child.Execute(IA);
            if (childResult == BehaviourResult.Failure) result = childResult;
        }

        if (debug) Debug.Log("Parallel[" + children.Count + "] returned: " + result);
        return result;
    }
}