﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceNode : BehaviourNode
{
    //Variables que recuerdan en que nodo nos quedamos en caso de Running
    //y cual fue el ultimo iniciado
    int currentChild = 0;
    int lastInitedChild = -1;
    List<BehaviourNode> childs = new List<BehaviourNode>();

    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var node = transform.GetChild(i).GetComponent<BehaviourNode>();
            if(node) childs.Add(node);
        }
    }


    public override bool MustInterrupt(BaseAI baseIA)
    {
        for (int i = 0; i < childs.Count; i++)
        {
            var node = childs[i];
            if (node.Concurrent)
            {
                if (!node.MustInterrupt(baseIA))
                    return false;
            }
            else
            {
                return true;
            }
        }

        return true;
    }

    public override void End(BaseAI IA)
    {
        if (lastInitedChild >= 0)
            childs[currentChild].End(IA);

        currentChild = 0;
        lastInitedChild = -1;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {

        for (int i = 0; i < currentChild; i++)
        {
            var node = childs[i];
            if(!node.isActiveAndEnabled) continue;
            if(debug) Debug.Log(node);
            if (node.MustInterrupt(IA))
            {
                currentChild = i;
                lastInitedChild = -1;
                //End recursivo
            }
        }

        //Comienzo a recorrer los hijos donde me quede (o desde el principio)
        for (; currentChild < childs.Count; currentChild++)
        {
            var node = childs[currentChild];
            if(debug) Debug.Log(node);

            //Si en la ejecución anterior no inicie este lo inicio
            //y lo marco como el ultimo iniciado para prevenir
            //iniciarlo multiples veces en caso de running
            if (currentChild != lastInitedChild)
            {
                node.Init(IA);
                lastInitedChild = currentChild;
            }

            BehaviourResult result = node.Execute(IA);

            //Si el nodo fallo la ejecución reinicio el currentchild
            //para que la siguiente vez que se ejecute la secuencia arranque
            //de el principio, asi como tambien reinicio el ultimo iniciado
            //para asegurarse de que el primero se inicia correctamente
            if (result == BehaviourResult.Failure)
            {
                currentChild = 0;
                lastInitedChild = -1;
                node.End(IA);
                return BehaviourResult.Failure;
            }

            //Si el nodo dio running la secuencia entera da running, y como
            //retornamos el siguiente execute va a continuar desde el current
            //child, o sea que ejecuta nuevamente desde este nodo que dio running
            if (result == BehaviourResult.Running)
            {
                return BehaviourResult.Running;
            }
     
            //Si llegamos acá significa que el nodo actual dio success y entonces
            //finalizamos el nodo
            node.End(IA);
        }

        //Si llegamos hasta acá significa que la secuencia se ejecutó completa
        //y por ende reiniciamos los indices para que la siguiente ejecución
        //arranca desde un principio
        currentChild = 0;
        lastInitedChild = -1;
        return BehaviourResult.Success;
    }
}
