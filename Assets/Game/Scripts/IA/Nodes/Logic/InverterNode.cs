﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InverterNode : BehaviourNode
{
    BehaviourNode node;
    bool init;
    


    public override void Init(BaseAI baseIA)
    {
        if (transform.childCount > 1)
            Debug.LogError("NotNode Error - Only one child is supported");
        var child = transform.GetChild(0);
        if (!child)
            Debug.LogError("NotNode Error - Node has no children");
        node = child.GetComponent<BehaviourNode>();
        if (!node)
            Debug.LogError("NotNode Error - Child is missing a node");

        node.Init(baseIA);
    }

    public override BehaviourResult Execute(BaseAI baseIA)
    {
        if (debug) Debug.Log("Executing node");
        BehaviourResult result = node.Execute(baseIA);
        if (debug) Debug.Log("NotNode - " + result);

        if (result == BehaviourResult.Running)
        {
            return BehaviourResult.Running;
        }
        if (result == BehaviourResult.Failure)
        {
            return BehaviourResult.Success;
        }
        return BehaviourResult.Failure;
    }
}
