﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorNode : BehaviourNode
{
    int currentChild = 0;
    int lastInitedChild = -1;

    List<BehaviourNode> childs = new List<BehaviourNode>();

    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var node = transform.GetChild(i).GetComponent<BehaviourNode>();
            if(node) childs.Add(node);
        }
    }

    public override void End(BaseAI IA)
    {
        if (lastInitedChild >= 0)
            childs[currentChild].End(IA);

        currentChild = 0;
        lastInitedChild = -1;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        for (; currentChild < childs.Count; currentChild++)
        {
            var node = childs[currentChild];
            if(!node.isActiveAndEnabled) continue;
            if(debug) Debug.Log(node);

            if (currentChild != lastInitedChild)
            {
                node.Init(IA);
                lastInitedChild = currentChild;
            }

            BehaviourResult result = node.Execute(IA);

            if (result == BehaviourResult.Success)
            {
                currentChild = 0;
                lastInitedChild = -1;
                node.End(IA);
                return BehaviourResult.Success;
            }

            if (result == BehaviourResult.Running)
                return BehaviourResult.Running;

            node.End(IA);
        }

        currentChild = 0;
        lastInitedChild = -1;
        return BehaviourResult.Failure;
    }
}
