﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnSuccessNode : BehaviourNode
{
    public override BehaviourResult Execute(BaseAI IA)
    {
        return BehaviourResult.Success;
    }
}
