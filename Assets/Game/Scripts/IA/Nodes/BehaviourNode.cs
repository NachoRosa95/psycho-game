﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BehaviourResult { Success, Failure, Running }

public abstract class BehaviourNode : MonoBehaviour
{
    [SerializeField] bool concurrent;
    [SerializeField] protected bool debug;

    public bool Concurrent { get { return concurrent; } }


    public virtual void Init(BaseAI IA){ }
    public virtual void End(BaseAI IA) { }
    public abstract BehaviourResult Execute(BaseAI IA);

    public virtual bool MustInterrupt(BaseAI IA)
    {
        return Execute(IA) == BehaviourResult.Success;
    }
}