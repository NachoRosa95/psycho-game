﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CanSee : BehaviourNode
{
    [SerializeField] string onSightMemoryName;
    [SerializeField] string targetMemoryName;
    [SerializeField] string targetTag;
    private GameObject[] targets;
    private MemoryEntry<List<GameObject>> objectsOnSight;
    private MemoryEntry<GameObject> targetEntry;
    private MemoryEntry<float> targetDistEntry;
    private bool init;

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.GetComponent<Blackboard>();
        objectsOnSight = bb.GetEntry<List<GameObject>>(onSightMemoryName);
        targetEntry = bb.GetEntry<GameObject>(targetMemoryName);
        if (objectsOnSight.value == null)
        {
            objectsOnSight.value = new List<GameObject>();
        }
        init = true;
        targets = GameObject.FindGameObjectsWithTag(targetTag);
    }

    public override BehaviourResult Execute(BaseAI baseAI)
    {
        if (!init) return BehaviourResult.Failure;
        if (targets.Length == 0) { Debug.LogError("CanSee - target does not exist", this); return BehaviourResult.Failure; }
        var result = targets.FirstOrDefault(target => objectsOnSight.value.Contains(target));
        if (debug) Debug.Log(targetTag + "- CanSee:" + result);
        if (!result) return BehaviourResult.Failure;

        targetEntry.value = result;
        return BehaviourResult.Success;
    }
}