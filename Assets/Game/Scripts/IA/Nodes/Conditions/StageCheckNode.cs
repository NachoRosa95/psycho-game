﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageCheckNode : BehaviourNode
{
    [SerializeField] PsychologicAttribute attribute;
    [SerializeField] int stageToSurpass;
    MemoryEntry<int> currentAttributeStage;

    public override void Init(BaseAI IA)
    {
        currentAttributeStage = IA.Blackboard.GetEntry<int>(attribute.AttributeName + "CurrentStage");
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (debug)
            Debug.Log(attribute.ToString() + "'s stage is: " + currentAttributeStage.value, this);

        if (currentAttributeStage.value >= stageToSurpass)
        {
            return BehaviourResult.Success;
        }

        return BehaviourResult.Failure;  

    }
}
