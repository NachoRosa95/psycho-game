﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlreadyAttackedNode : BehaviourNode
{
    MemoryEntry<bool> enemyHasBeenAttackedEntry;

    public override void Init(BaseAI IA)
    {
        enemyHasBeenAttackedEntry = IA.Blackboard.GetEntry<bool>("entityHasBeenAttacked");
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (enemyHasBeenAttackedEntry.value) return BehaviourResult.Success;
        else return BehaviourResult.Failure;
    }
}
