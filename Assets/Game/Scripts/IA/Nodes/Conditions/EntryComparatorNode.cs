﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryComparatorNode : BehaviourNode
{
    [SerializeField] string memoryName1;
    [SerializeField] Operation operation;
    [SerializeField] string memoryName2;
    private MemoryEntry<float> targetEntry1;
    private MemoryEntry<float> targetEntry2;
    private bool init;

    private enum Operation
    {
        LessThan,
        Greater,
        Equals
    }

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.GetComponent<Blackboard>();
        targetEntry1 = bb.GetEntry<float>(memoryName1);
        targetEntry2 = bb.GetEntry<float>(memoryName2);
        init = true;
    }

    public override BehaviourResult Execute(BaseAI baseAI)
    {
        if (!init || targetEntry1 == null || targetEntry2 == null) return BehaviourResult.Failure;

        if (debug)
            Debug.Log(targetEntry1.value + " - " + targetEntry1.value, this);

        switch (operation)
        {
            case Operation.LessThan: return targetEntry1.value < targetEntry2.value ? BehaviourResult.Success : BehaviourResult.Failure;
            case Operation.Greater: return targetEntry1.value > targetEntry2.value ? BehaviourResult.Success : BehaviourResult.Failure;
            case Operation.Equals: return targetEntry1.value == targetEntry2.value ? BehaviourResult.Success : BehaviourResult.Failure;
        }

        Debug.Log("ComparatorNode.cs - Invalid operation");
        return BehaviourResult.Failure;
    }
}
