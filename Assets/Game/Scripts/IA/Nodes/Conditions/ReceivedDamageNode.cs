﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceivedDamageNode : BehaviourNode
{
    HealthComponent healthReference;
    float previousHealth;

    private void Awake()
    {
        healthReference = GetComponentInParent<HealthComponent>();
        previousHealth = healthReference.Health;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if(previousHealth > healthReference.Health)
        {
            if(debug) Debug.Log("Received Damage",this);
            //Dejo registro en el blackboard de que el enemigo fue atacado.
            IA.Blackboard.GetEntry<bool>("entityHasBeenAttacked").value = true;

            return BehaviourResult.Success;
        } 
        else
        {
            if (debug) Debug.Log("Hasn't received damage", this);
            return BehaviourResult.Failure;
        }
    }

    public override void End(BaseAI IA)
    {
        //previousHealth = healthReference.Health;
    }
}