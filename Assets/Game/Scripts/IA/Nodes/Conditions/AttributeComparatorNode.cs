﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributeComparatorNode : BehaviourNode
{
    [SerializeField] PsychologicAttribute attribute;
    [SerializeField] Operation operation;
    float lastValue;
    float currentValue;
    private bool init;

    private enum Operation
    {
        LessThan,
        Greater,
        Equals
    }

    public override void Init(BaseAI baseAI)
    {
        init = true;

    }

    public override BehaviourResult Execute(BaseAI baseAI)
    {
        if (!init) return BehaviourResult.Failure;

        currentValue = attribute.Weight;
        if (debug)
            print(attribute + " - " + currentValue + " (Current Value)");

        switch (operation)
        {
            case Operation.LessThan: return currentValue < lastValue ? BehaviourResult.Success : BehaviourResult.Failure;
            case Operation.Greater: return currentValue > lastValue ? BehaviourResult.Success : BehaviourResult.Failure;
            case Operation.Equals: return currentValue == lastValue ? BehaviourResult.Success : BehaviourResult.Failure;
        }

        Debug.Log("ComparatorNode.cs - Invalid operation");
        return BehaviourResult.Failure;
    }

    public override void End(BaseAI IA)
    {
        lastValue = currentValue;
    }
}
