﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInSightNode : BehaviourNode
{
    List<Collider2D> objectsList;
    [SerializeField] int playerLayer;
    

    public override void Init(BaseAI IA)
    {
        objectsList = IA.Blackboard.GetEntry<List<Collider2D>>("detectedObjectList").value;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        //Si detecta Al player,
        for (int i = 0; i < objectsList.Count; i++)
        {
            if (objectsList[i].gameObject.layer == playerLayer)
            {
                IA.Blackboard.GetEntry<GameObject>("target").value = objectsList[i].gameObject;

                if (debug) Debug.Log("Sensor detects an enemy", this);
                return BehaviourResult.Success;
            }
        }
        if (debug) Debug.Log("Sensor found nothing nearby", this);
        return BehaviourResult.Failure;
    }
}
