﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsInAnimatorState : BehaviourNode
{
    Animator animator;
    [SerializeField] string animatorStateName;
    [SerializeField] int animatorLayer = 0;

    public override void Init(BaseAI IA)
    {
        if (!animator) animator = IA.Body.GetComponentInChildren<Animator>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (!animator) Debug.LogError("No animator found", this);

        if (animator.GetCurrentAnimatorStateInfo(animatorLayer).IsName(animatorStateName))
        {
            if (debug) Debug.Log("Estoy en el estado indicado",this);
            return BehaviourResult.Success;
        }
        else
        {
            if (debug) Debug.Log("Estoy en un estado distinto al indicado", this);
            return BehaviourResult.Failure;
        }
    }
}
