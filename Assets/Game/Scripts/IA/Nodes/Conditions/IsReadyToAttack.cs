﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsReadyToAttack : BehaviourNode
{
    EnemyController_Base controller;

    public override void Init(BaseAI IA)
    {
        if (!controller) controller = IA.Body.GetComponent<EnemyController_Base>();
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        var weapon = controller.WeaponReference;
        if (weapon.OnCooldown)
        {
            if (debug) Debug.Log("Weapon is on cooldown", this);
            return BehaviourResult.Failure;
        }
        else
        {
            if (debug) Debug.Log("Weapon is ready to fire", this);
            return BehaviourResult.Success;
        }
    }
}
