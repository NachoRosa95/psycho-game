﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComparatorNode : BehaviourNode
{
    [SerializeField] string memoryName;
    [SerializeField] Operation operation;
    [SerializeField] float range;
    private MemoryEntry<float> targetEntry;
    private bool init;

    private enum Operation
    {
        LessThan,
        Greater,
        Equals
    }

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.GetComponent<Blackboard>();
        targetEntry = bb.GetEntry<float>(memoryName);
        init = true;
    }

    public override BehaviourResult Execute(BaseAI baseAI)
    {
        if (!init) return BehaviourResult.Failure;

        var value = targetEntry.value;
        if (debug)
            print(memoryName + " - " + value);

        switch (operation)
        {
            case Operation.LessThan: return value < range ? BehaviourResult.Success : BehaviourResult.Failure;
            case Operation.Greater: return value > range ? BehaviourResult.Success : BehaviourResult.Failure;
            case Operation.Equals: return value == range ? BehaviourResult.Success : BehaviourResult.Failure;
        }

        Debug.Log("ComparatorNode.cs - Invalid operation");
        return BehaviourResult.Failure;
    }
}
