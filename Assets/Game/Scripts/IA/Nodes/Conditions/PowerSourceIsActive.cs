﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSourceIsActive : BehaviourNode
{
    [SerializeField] string fromKey;

    MemoryEntry<GameObject> targetEntry;
    PowerSource powerSource;

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.Blackboard;
        targetEntry = bb.GetEntry<GameObject>(fromKey);
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        powerSource = targetEntry.value.GetComponent<PowerSource>();
        if (powerSource.State) return BehaviourResult.Success;
        else return BehaviourResult.Failure;
    }
}
