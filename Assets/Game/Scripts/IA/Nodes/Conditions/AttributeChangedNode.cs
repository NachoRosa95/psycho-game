﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributeChangedNode : BehaviourNode
{
    [SerializeField] PsychologicAttribute attribute;
    float lastWeight;
    float currentWeight;

    public override void Init(BaseAI IA)
    {
        currentWeight = attribute.Weight;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if(currentWeight > lastWeight)
        {
            lastWeight = currentWeight;
            return BehaviourResult.Success;
        }
        lastWeight = currentWeight;
        return BehaviourResult.Failure;
    }
}
