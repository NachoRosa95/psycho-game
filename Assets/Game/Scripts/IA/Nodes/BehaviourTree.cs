﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTree : MonoBehaviour
{
    BehaviourNode node;
    [SerializeField] BaseAI IA;

    void Awake()
    {
        var root = transform.GetChild(0).GetComponent<BehaviourNode>();
        if (root)
        {
            node = root;
            node.Init(IA);
        }
        else Debug.LogError("No BehaviourNode attached to first child", this);
    }

    void Update()
    {
        node.Execute(IA);
    }
}
