﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFloatNode : BehaviourNode
{
    [SerializeField] string memoryName;
    [SerializeField] float valueToSet;
    private MemoryEntry<float> targetEntry;
    private bool init;

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.Blackboard;
        targetEntry = bb.GetEntry<float>(memoryName);
        init = true;
    }

    public override BehaviourResult Execute(BaseAI baseAI)
    {
        if (!init) return BehaviourResult.Failure;

        targetEntry.value = valueToSet;
        if (debug) Debug.Log("Setting "+ memoryName + " to " + valueToSet);
        return BehaviourResult.Success;
    }
}