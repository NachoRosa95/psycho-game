﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosNode : BehaviourNode
{
    [SerializeField] string gameObjectMemoryName;
    [SerializeField] string memoryName;
    private MemoryEntry<GameObject> goEntry;
    private MemoryEntry<Vector3> targetEntry;
    private bool init;

    public override void Init(BaseAI IA)
    {
        var bb = IA.GetComponent<Blackboard>();
        goEntry = bb.GetEntry<GameObject>(gameObjectMemoryName);
        targetEntry = bb.GetEntry<Vector3>(memoryName);
        init = true;
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        if (!init || goEntry == null || goEntry.value == null) return BehaviourResult.Failure;

        var pos = goEntry.value.transform.position;
        if (debug) Debug.Log("Setting " + gameObjectMemoryName + " to " + pos);
        targetEntry.value = pos;
        return BehaviourResult.Success;
    }
}