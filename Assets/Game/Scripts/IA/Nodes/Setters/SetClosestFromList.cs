﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetClosestFromList : BehaviourNode
{
    [SerializeField] string listKey = "";
    [SerializeField] string outputKey = "";
    [SerializeField] string fromKey;
    private MemoryEntry<GameObject> fromEntry;
    private MemoryEntry<GameObject> output;
    private MemoryEntry<List<GameObject>> objectsEntry;
    private bool init;

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.GetComponent<Blackboard>();
        objectsEntry = bb.GetEntry<List<GameObject>>(listKey);
        output = bb.GetEntry<GameObject>(outputKey);
        fromEntry = fromKey == "" ? null : bb.GetEntry<GameObject>(fromKey);
        init = true;
    }

    public override BehaviourResult Execute(BaseAI baseAI)
    {
        if (!init || objectsEntry.value == null) return BehaviourResult.Failure;

        
        // If fromKey left empty, uses the AI's body as from
        var from = fromEntry != null ? fromEntry.value : baseAI.Body;
        GameObject closest = null;
        var minDist = float.MaxValue;
        foreach (GameObject to in objectsEntry.value)
        {
            var dist = Vector3.Distance(from.transform.position, to.transform.position);
            if (dist >= minDist) continue;
            minDist = dist;
            closest = to;
        }

        output.value = closest;
        if (debug) Debug.Log("Closest object found: " + closest);
        return closest ? BehaviourResult.Success : BehaviourResult.Failure;
    }
}
