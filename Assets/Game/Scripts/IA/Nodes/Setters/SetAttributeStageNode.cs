﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAttributeStageNode : BehaviourNode
{
    [SerializeField] PsychologicAttribute attribute;

    //Creamos una lista con los limites de cada stage. 
    //El primer elemento de la lista determina hasta que peso puede tener el atributo para considerarse dentro del stage 0.
    //El segundo elemento determina hasta que peso puede tener el atributo para estar en el stage 1, etc.
    [SerializeField] List<int> stagesLimitWeight = new List<int>();
    float currentAttributeWeight;
    int currentStage;

    MemoryEntry<int> currentAttributeStage;

    public override void Init(BaseAI IA)
    {
        currentAttributeWeight = attribute.Weight;
        currentAttributeStage = IA.Blackboard.GetEntry<int>(attribute.AttributeName + "CurrentStage");
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        for (int i = stagesLimitWeight.Count - 1; i >= 0; i--)
        {
            if (currentAttributeWeight <= stagesLimitWeight[i])
            {
                currentStage = i;
            }
        }

        currentAttributeStage.value = currentStage;

        if (debug)
            Debug.Log(attribute.ToString() + "'s value is: " + currentAttributeWeight, this);

        return BehaviourResult.Success;
    }
}
