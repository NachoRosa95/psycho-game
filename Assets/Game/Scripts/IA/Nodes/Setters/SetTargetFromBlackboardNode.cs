﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTargetFromBlackboardNode : BehaviourNode
{
    FollowGameObject movementComponent;
    [SerializeField] string key = "target";
    MemoryEntry<GameObject> targetEntry;

    private void Awake()
    {
        movementComponent = GetComponentInParent<FollowGameObject>();
    }

    public override void Init(BaseAI IA)
    {
        targetEntry = IA.Blackboard.GetEntry<GameObject>(key);
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        movementComponent.Target = targetEntry.value.gameObject;
        return BehaviourResult.Success;
    }
}
