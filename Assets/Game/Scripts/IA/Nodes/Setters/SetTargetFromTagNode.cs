﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTargetFromTagNode : BehaviourNode
{
    FollowGameObject movementComponent;
    GameObject target;
    [SerializeField] string targetName;

    private void Awake()
    {
        movementComponent = GetComponentInParent<FollowGameObject>();
    }

    public override void Init(BaseAI IA)
    {
        target = GameObject.FindWithTag(targetName);
        if (!target)
            Debug.LogError("Error - No target found with the tag: " + targetName);
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        movementComponent.Target = target;
        return BehaviourResult.Success;
    }
}
