﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDistance : BehaviourNode
{
    [SerializeField] string fromKey;
    [SerializeField] string toKey;
    [SerializeField] string outputKey;
    MemoryEntry<GameObject> fromEntry;
    MemoryEntry<GameObject> toEntry;
    MemoryEntry<float> output;

    public override void Init(BaseAI baseAI)
    {
        var bb = baseAI.Blackboard;
        if(fromKey != "") fromEntry = bb.GetEntry<GameObject>(fromKey);
        toEntry = bb.GetEntry<GameObject>(toKey);
        output = bb.GetEntry<float>(outputKey);
    }

    public override BehaviourResult Execute(BaseAI IA)
    {
        var from = fromEntry != null ? fromEntry.value : IA.Body;
        var to = toEntry.value;
        if (!to || !from) return BehaviourResult.Failure;
        var dist = Vector3.Distance(from.transform.position, to.transform.position);

        //float distX = from.transform.position.x - to.transform.position.x;
        //float distY = from.transform.position.y - to.transform.position.y;
        //float distZ = from.transform.position.z - to.transform.position.z;

        //if (debug) Debug.Log("Distance from target " + from +" to "+ to + " is " + distX+ " " + distY  + " " + distZ);

        output.value = dist;
        return BehaviourResult.Success;
    }
}
