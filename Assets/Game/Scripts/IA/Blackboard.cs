﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryEntry<T>
{
    public T value;
}

public class Blackboard : MonoBehaviour
{
    public static Blackboard Global { get; protected set; }

    [SerializeField] Blackboard parent;

    public Blackboard Parent
    {
        get { return parent; }
    }

    Dictionary<string, object> entries = new Dictionary<string, object>();
    public Dictionary<string, object> Entries { get { return entries; } }

    protected virtual void Awake()
    {
        if (!parent && transform.parent)
            parent = transform.parent.GetComponentInParent<Blackboard>();

        if (!parent)
            parent = Global;
    }

    public MemoryEntry<T> GetEntry<T>(string key)
    {
        var cur = this;

        // TODO: Recursive?
        do
        {   // If any parent contains an entry with the same key it returns it
            if (cur.Entries.ContainsKey(key))
                return cur.Entries[key] as MemoryEntry<T>;
            cur = cur.Parent;
        } while (cur != null);

        // If the key is not present in any parent it's created where GetEntry was called
        return CreateEntry<T>(key);
    }

    public MemoryEntry<T> CreateEntry<T>(string key)
    {
        var newEntry = new MemoryEntry<T>();
        entries.Add(key, newEntry);
        return newEntry;
    }
}

