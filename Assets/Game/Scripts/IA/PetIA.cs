﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetIA : BaseAI
{
    MemoryEntry<ProjectileWeapon> weaponEntry;

    void Start()
    {
        weaponEntry = Blackboard.GetEntry<ProjectileWeapon>("weapon");
        weaponEntry.value = Body.GetComponentInChildren<ProjectileWeapon>();
    }
}