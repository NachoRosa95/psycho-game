﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceSensor : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] string findWithTag;
    [SerializeField] string key;
    Blackboard blackboard;
    MemoryEntry<float> distEntry;

    void Awake()
    {
        blackboard = GetComponentInChildren<Blackboard>();
        distEntry = blackboard.GetEntry<float>(key);
        if(!target) target = GameObject.FindWithTag(findWithTag)?.transform;
    }

    void Update()
    {
        if (!blackboard || !target || distEntry == null) return;
        distEntry.value = Vector3.Distance(transform.position, target.position);
    }
}
