﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SightSensor : MonoBehaviour
{
    static Collider2D[] detectedCollidersInOverlap = new Collider2D[100];

    public bool debug;
    [SerializeField, TextArea(1,1)] string description; 
    [SerializeField] float overlapRadius;
    [SerializeField] Vector3 radiusOffset;
    [SerializeField] float coneAngle;
    [SerializeField] string listKey = "detectedObjectList";
    [SerializeField] LayerMask overlapLayers;
    [SerializeField] LayerMask obstacleLayers;


    float sensorRefreshTime;
    MemoryEntry<List<Collider2D>> detectedObjectsList;
    List<Collider2D> objectsList;
    Blackboard blackboard;


    List<Vector2> debugOrigins = new List<Vector2>();
    List<Vector2> debugDestinations = new List<Vector2>();
    List<bool> debugHits = new List<bool>();

    private void Awake()
    {
        blackboard = GetComponentInChildren<Blackboard>();

        sensorRefreshTime = 0.3f;
        objectsList = new List<Collider2D>();
        detectedObjectsList = blackboard.GetEntry<List<Collider2D>>(listKey);
        detectedObjectsList.value = objectsList;
    }

    void OnEnable()
    {
        //Tiramos un tiempo aleatorio de inicio para prevenir que todos los sensores se ejecuten al mismo tiempo
        float rndInit = Random.Range(0, 0.5f);
        InvokeRepeating("SensorDetection", rndInit, sensorRefreshTime);
    }

    void OnDisable()
    {
        CancelInvoke();
    }

    void SensorDetection()
    {
        objectsList.Clear();
        debugOrigins.Clear();
        debugDestinations.Clear();
        debugHits.Clear();

        //Checkeo de Overlap
        int detectedAmount = Physics2D.OverlapCircleNonAlloc(transform.position + radiusOffset, overlapRadius, detectedCollidersInOverlap, overlapLayers);
        var a = Physics2D.OverlapCircleAll(transform.position + radiusOffset, overlapRadius, overlapLayers);

        if(debug) Debug.Log("Detecto "+ a.Length + " elementos");

        if (detectedAmount > 0)
        {
            //Si hay alguien en el circulo, recorre el for
            for (int i = 0; i < detectedAmount; i++)
            {
                Collider2D target = detectedCollidersInOverlap[i];

                //Checkeo de cono
                //FIX: Si el angulo del cono es mayor a 1, no funciona mas.
                Vector2 lookdir2D = transform.right;
                Vector2 targetDir2D = (target.transform.position - transform.position).normalized;
                // float dot2D = Vector2.Dot(lookdir2D, targetDir2D);

                //Aca empezaria el checkeo de cono ( si lo activo)

                //Deteccion de raycast.
                float distToTarget = Vector2.Distance(target.transform.position, transform.position);
                RaycastHit2D hit2 = Physics2D.Raycast(transform.position, targetDir2D, distToTarget, obstacleLayers);
                bool hit = Physics2D.Raycast(transform.position, targetDir2D, distToTarget, obstacleLayers);


                debugHits.Add(hit);
                debugOrigins.Add(transform.position);

                if (hit)
                {
                    debugDestinations.Add(hit2.point);
                    objectsList.Add(hit2.collider);
                }
                else
                {
                    debugDestinations.Add(target.transform.position);
                    objectsList.Add(target);
                }

                detectedObjectsList.value = objectsList;

                #region Checkeo de cono
                ////Si los vectores estan lo suficientemente paralelos para considerar que entra dentro del rango de mi cono...
                //if (dot2D >= coneAngle)
                //{
                //    //Deteccion de raycast.
                //    float distToTarget = Vector2.Distance(target.transform.position, transform.position);
                //    RaycastHit2D hit2 = Physics2D.Raycast(transform.position, targetDir2D, distToTarget, obstacleLayers);
                //    bool hit = Physics2D.Raycast(transform.position, targetDir2D, distToTarget, obstacleLayers);


                //    debugHits.Add(hit);
                //    debugOrigins.Add(transform.position);

                //    if (hit)
                //    {
                //        debugDestinations.Add(hit2.point);
                //        objectsList.Add(hit2.collider);
                //    }
                //    else
                //    {
                //        debugDestinations.Add(target.transform.position);
                //        objectsList.Add(target);
                //    }

                //    detectedObjectsList.Value = objectsList;


                //}
                #endregion
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position + radiusOffset, overlapRadius);

        #region Checkeo de cono
        ////Arreglar gizmos, pero el sensor funciona como debe
        //Gizmos.color = Color.magenta;
        //Vector2 right = transform.right;

        //Vector2 angleA = Quaternion.AngleAxis(coneAngle * 180 / Mathf.PI, Vector3.forward) * right;
        //Vector2 angleB = Quaternion.AngleAxis(-coneAngle * 180 / Mathf.PI, Vector3.forward) * right;

        //Gizmos.DrawLine((Vector2)transform.position, (Vector2)transform.position + angleA * overlapRadius);
        //Gizmos.DrawLine((Vector2)transform.position, (Vector2)transform.position + angleB * overlapRadius);
        #endregion

        for (int i = 0; i < debugHits.Count; i++)
        {
            Gizmos.color = debugHits[i] ? Color.red : Color.green;
            Gizmos.DrawLine(debugOrigins[i], debugDestinations[i]);
        }
    }
}