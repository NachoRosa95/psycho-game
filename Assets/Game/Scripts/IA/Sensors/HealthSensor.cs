﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSensor : MonoBehaviour
{
    [SerializeField] string healthKey = "health";
    [SerializeField] string maxHealthkey = "maxHealth";
    [SerializeField] HealthComponent health;
    [SerializeField] Blackboard blackboard;

    MemoryEntry<float> healthEntry;
    MemoryEntry<float> maxHealthEntry;

    void Awake()
    {
        healthEntry = blackboard.GetEntry<float>(healthKey);
        maxHealthEntry = blackboard.GetEntry<float>(maxHealthkey);
        maxHealthEntry.value = health.MaxHealth;
        if(health) health.OnChanged.AddListener(RegisterChange);
    }

    void RegisterChange(float value, string key)
    {
        healthEntry.value = value;
    }
}
