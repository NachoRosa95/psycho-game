﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaSensor : MonoBehaviour
{
    [SerializeField] Blackboard bb;
    [SerializeField] Transform area;
    [SerializeField] LayerMask layerMask;
    [SerializeField] string objectsInAreaKey = "objectsInArea";
    [SerializeField] string initPosKey = "initPos";
    
    MemoryEntry<List<GameObject>> objectsInAreaEntry;
    MemoryEntry<Vector3> initPosEntry;

    void Awake()
    {
        initPosEntry = bb.GetEntry<Vector3>(initPosKey);
        initPosEntry.value = transform.position;
        objectsInAreaEntry = bb.GetEntry<List<GameObject>>(objectsInAreaKey);
        objectsInAreaEntry.value = new List<GameObject>();
        area.SetParent(null, true);
    }

    void Update()
    {
        objectsInAreaEntry.value = Execute();
    }

    List<GameObject> Execute()
    {
        var offset = area.lossyScale / 2;
        Collider2D[] objects = new Collider2D[16];
        Physics2D.OverlapAreaNonAlloc(area.position - offset, area.position + offset, objects, layerMask);
        List<GameObject> resultSet = new List<GameObject>();
        foreach (Collider2D col in objects)
        {
            if(col)
                resultSet.Add(col.gameObject);
        }
        return resultSet;
    }

    void OnDrawGizmos()
    {
        if (objectsInAreaEntry == null) return;
        Gizmos.color = Color.red;
        foreach (GameObject target in objectsInAreaEntry.value)
        {
            Gizmos.DrawLine(transform.position, target.transform.position);
            Gizmos.DrawWireSphere(target.transform.position, 2);
        }
    }
}
