﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIA : BaseAI
{
    MemoryEntry<HealthComponent> healthEntry;
    MemoryEntry<ProjectileWeapon> weaponEntry;

    public MemoryEntry<ProjectileWeapon> WeaponEntry { get => weaponEntry; set => weaponEntry = value; }

    void Start()
    {
        healthEntry = Blackboard.GetEntry<HealthComponent>("health");
        healthEntry.value = Body.GetComponent<HealthComponent>();

        weaponEntry = Blackboard.GetEntry<ProjectileWeapon>("weapon");
        weaponEntry.value = Body.GetComponentInChildren<ProjectileWeapon>();
    }
}