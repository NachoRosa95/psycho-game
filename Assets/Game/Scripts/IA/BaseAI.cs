﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAI : MonoBehaviour
{
    [SerializeField] GameObject body;
    [SerializeField] Blackboard blackboard;
    [SerializeField] Animator animator;

    #region GetSet
    public GameObject Body
    {
        get { return body; }
    }
    public Blackboard Blackboard
    {
        get { return blackboard; }
    }
    public Animator Animator
    {
        get { return animator; }
    }
    #endregion


    void Awake()
    {
        if (body == null && transform.parent != null)
            body = transform.parent.gameObject;
    }
}
