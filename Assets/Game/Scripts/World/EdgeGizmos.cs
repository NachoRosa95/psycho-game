﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeGizmos : MonoBehaviour
{
    private EdgeCollider2D edge;

    private void Awake()
    {
    }

    private void OnDrawGizmos()
    {
        if(edge == null)    edge = GetComponent<EdgeCollider2D>();
        //if (edge.pointCount == 0) return;

        Gizmos.color = Color.blue;
        for (int i = 1; i < edge.pointCount; i++)
        {
            var position = new Vector2(transform.position.x, transform.position.y) + edge.offset;
            var pointA = position + edge.points[i - 1];
            var pointB = position + edge.points[i];
            Gizmos.DrawLine(pointA, pointB);
        }
    }
}
