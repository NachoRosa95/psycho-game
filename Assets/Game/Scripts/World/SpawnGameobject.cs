﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGameobject : MonoBehaviour
{
    [SerializeField] GameObject prefab;
    [SerializeField] GameObject instance;
    [SerializeField] Transform parent;
    [SerializeField] bool spawnOnEnable = true;
    [SerializeField] bool destroyInstance = false;

    void OnEnable()
    {
        if (spawnOnEnable) Execute();
    }

    public void Execute()
    {
        if (!prefab) return;
        var newInstance = Instantiate<GameObject>(prefab, instance.transform.position, prefab.transform.rotation, parent ?? null);
        if (instance && destroyInstance) Destroy(instance);
        instance = newInstance;
    }
}
