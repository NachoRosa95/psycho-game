﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [System.Serializable]
// public class ParallaxLayer
// {
//     public Rect layerRect = new Rect(Vector2.zero, Vector2.one);
//     public Transform root;
//     public Color color;
// }

public class Parallax : MonoBehaviour
{
    [SerializeField] List<RectTransform> layers = new List<RectTransform>();
    [SerializeField] RectTransform levelRect;
    [SerializeField] Camera cam;
    [SerializeField] float tweakX;
    [SerializeField] float tweakY;


    void Awake()
    {
        if(!cam) cam = Camera.main;
    }

    void Update()
    {
        Execute();
    }

    void Execute()
    {
        if(!levelRect) return;
        var camSize = cam.orthographicSize;
        Vector2 relativePos = cam.transform.position - levelRect.position;
        var posMultiplier = relativePos / levelRect.rect.size * -2;
        posMultiplier.x = Mathf.Clamp(posMultiplier.x, -1, 1);
        posMultiplier.y = Mathf.Clamp(posMultiplier.y, -1, 1);
        foreach (RectTransform layer in layers)
        {
            if (!layer) continue;
            var newPos = layer.localPosition;
            var layerSize = layer.rect.size / 2 * layer.localScale;
            // var layerSize = new Vector2(renderer.sprite.bounds.extents.x * renderer.transform.lossyScale.x, renderer.sprite.bounds.extents.y * renderer.transform.lossyScale.y);
            newPos.x = posMultiplier.x * (layerSize.x - camSize * 2 - tweakX);
            newPos.y = posMultiplier.y * (layerSize.y - camSize - tweakY);
            layer.localPosition = newPos;
        }
    }

    void OnDrawGizmos()
    {
        if (!levelRect) return;
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(levelRect.position, levelRect.rect.size);
        if(!Application.isPlaying) Execute();
        // for (var i = 0; i < layers.Count; i++)
        // {
        //     var hue = (float)i / layers.Count;
        //     var layer = layers[i];
        //     if (layer == null) continue;
        //     Gizmos.color = Color.HSVToRGB(hue, 1, 1);
        //     Gizmos.DrawWireCube(layer.position, layer.);
        // }
    }
}
