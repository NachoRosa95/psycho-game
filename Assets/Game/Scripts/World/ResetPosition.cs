﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        CheckpointRegister checkpointRegister = other.GetComponentInParent<CheckpointRegister>();
        if(checkpointRegister != null)
        {
            other.transform.position = checkpointRegister.LastCheckpoint.transform.position;
        }
    }
}
