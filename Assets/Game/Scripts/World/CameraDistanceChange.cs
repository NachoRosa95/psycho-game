﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDistanceChange : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] float normalCameraSize;
    [SerializeField] float maxCameraSize;
    [SerializeField] float lerpTime;

    float currentCameraSize;
    bool changingSize;

    public float CurrentCameraSize 
    { 
        get => currentCameraSize;
        set
        {
            currentCameraSize = value;
            if(currentCameraSize > maxCameraSize - .2f)
            {
                currentCameraSize = maxCameraSize;
                changingSize = false;
            }

            if(currentCameraSize < normalCameraSize + .2f)
            {
                currentCameraSize = normalCameraSize;
                changingSize = false;
            }
        }
    }

    private void Awake()
    {
        CurrentCameraSize = mainCamera.orthographicSize;
        //coroutine = ChangeSizeCoroutine(CurrentCameraSize);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CurrentCameraSize = mainCamera.orthographicSize;
        changingSize = true;
        StopCoroutine("ChangeSizeCoroutine");
        StartCoroutine("ChangeSizeCoroutine", maxCameraSize);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CurrentCameraSize = mainCamera.orthographicSize;
        changingSize = true;
                StopCoroutine("ChangeSizeCoroutine");
        StartCoroutine("ChangeSizeCoroutine", normalCameraSize);
    }

    IEnumerator ChangeSizeCoroutine(float desiredSize)
    {
        while(changingSize == true)
        {
            mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, desiredSize, Time.deltaTime * lerpTime);
            CurrentCameraSize = mainCamera.orthographicSize;
            yield return 0;
        }
    }

}
