﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformParenting : MonoBehaviour
{
    [SerializeField] List<string> validTags = new List<string>();

    private Transform previousParent = null;
    private Vector3 previousScale = Vector3.one;
    private Quaternion previousRotation = Quaternion.identity;

    void Awake()
    {
        if (validTags.Count == 0) validTags.Add("Player");
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Checks if collision is valid
        if (!validTags.Contains(collision.gameObject.tag)) { Debug.LogWarning(collision.gameObject.tag); return; }
        // Prevents collisions from below
        if (collision.contacts[0].normal.y > 0) { return; }
        // Stores current parent of the object that collided
        if (collision.transform.parent != transform)
        {
            previousScale = collision.transform.localScale;
            previousRotation = collision.transform.rotation;
            previousParent = collision.transform.parent;
            collision.transform.rotation = transform.rotation;
        }
        // Sets its parent to this transform
        collision.transform.SetParent(transform);
        Debug.Log("Parented", this);
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        // Checks if collision is valid
        if (!validTags.Contains(collision.gameObject.tag)) return;
        // Sets the object's parent to its previous parent
        collision.transform.SetParent(previousParent);
        collision.transform.rotation = previousRotation;
        collision.transform.localScale = previousScale;

        Debug.Log("Unparented", this);
    }
}
