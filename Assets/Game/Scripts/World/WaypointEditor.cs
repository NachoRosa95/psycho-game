﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WaypointEditor : MonoBehaviour
{
    public abstract List<Vector3> GetWaypoints();
    public abstract void SetWaypoints(List<Vector3> value);
}
