﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointRegister : MonoBehaviour
{
    Checkpoint lastCheckpoint;

    public Checkpoint LastCheckpoint { get { return lastCheckpoint; } set { lastCheckpoint = value; } }

    float lastHealth;

    public float LastHealth { get { return lastHealth; } set { lastHealth = value; } }
}
