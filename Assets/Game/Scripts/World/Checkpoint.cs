﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider2D))]
public class Checkpoint : MonoBehaviour
{
    AudioSource audioSource;
    SpriteRenderer spriteRenderer;
    Collider2D col;
    Animator animator;
    [SerializeField] AudioClip checkpointClip;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        col = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        CheckpointRegister checkpointRegister = other.GetComponentInParent<CheckpointRegister>();

        if (checkpointRegister != null)
        {
            checkpointRegister.LastCheckpoint = this;
            spriteRenderer.color = Color.white; // temporary
            col.enabled = false;                //
            if (animator) animator.SetBool("Active", true);

            HealthComponent health = other.GetComponentInChildren<HealthComponent>();

            if (health != null)
            {
                checkpointRegister.LastHealth = health.Health;
            }
        }
    }

    void PlayClip()
    {
        audioSource.PlayOneShot(checkpointClip);
    }
}
