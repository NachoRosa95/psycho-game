using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Experimental.U2D;

public class VanishObject : MonoBehaviour
{
    [SerializeField] string materialPropertyName = "_AlphaValue";
    SpriteRenderer sr;
    SpriteShapeRenderer ssr;

    Material material;
    Material[] materials;

    bool dissappearing;
    public bool Dissappearing { get => dissappearing; set => dissappearing = value; }

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        ssr = GetComponent<SpriteShapeRenderer>();

        if (sr) 
            material = sr.material;
        else if (ssr)
            materials = ssr.materials;

    }

    public void ChangeAlpha(float newValue)
    {
        if (sr)
            material.SetFloat(materialPropertyName, newValue);
        else if (ssr)
            for (int i = 0; i < materials.Length; i++)
            {
                materials[i].SetFloat(materialPropertyName, newValue);
            }

    }
}
