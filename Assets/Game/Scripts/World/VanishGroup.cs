﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanishGroup : MonoBehaviour
{
    [SerializeField] string alphaParameterName = "_AlphaValue";
    [SerializeField] float vanishingSpeed;
    [SerializeField] float reappearingSpeed;
    [SerializeField] bool normalBehaviour = true;
    [SerializeField] bool vanishOnCollission = true;
    bool vanishing;
    bool reappearing;
    float fade;

    VanishObject[] children;

    public float Fade 
    { 
        get => fade;
        set
        {
            fade = value;

            if (fade < 0f)
            {
                fade = 0;
                vanishing = false;
            }

            if(fade > 1)
            {
                fade = 1;
                reappearing = false;
            }
        }
    }

    private void Awake()
    {
        children = GetComponentsInChildren<VanishObject>(true);
        if (children == null) Debug.LogError("Error - Vanish group has no children",this);

        vanishing = false;
        reappearing = false;

        if (normalBehaviour)
            fade = 1;
        else
            fade = 0;
    }

    private void Update()
    {
        if (!vanishing && !reappearing)
        {
            return;
        }

        var vanishDirection = vanishing ? -vanishingSpeed : reappearingSpeed;
        Fade += vanishDirection * Time.deltaTime;

        for (int i = 0; i < children.Length; i++)
        {
            children[i].ChangeAlpha(Fade);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(vanishOnCollission) TurnOnVanish();
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (vanishOnCollission) TurnOffVanish();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Triggered by " + collision.name);
        if (vanishOnCollission) TurnOnVanish();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (vanishOnCollission) TurnOffVanish();
    }

    public void TurnOnVanish()
    {
        if (normalBehaviour)
        {
            vanishing = true;
            reappearing = false;
        }
        else
        {
            vanishing = false;
            reappearing = true;
        }
    }

    public void TurnOffVanish()
    {
        if (normalBehaviour)
        {
            vanishing = false;
            reappearing = true;
        }
        else
        {
            vanishing = true;
            reappearing = false;
        }
    }
}
