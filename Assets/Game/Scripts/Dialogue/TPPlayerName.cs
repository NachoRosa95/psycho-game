﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TPPlayerName", menuName = "Scriptable Object/TextProcessor/PlayerName", order = 1)]
public class TPPlayerName : TextProcessorSO
{
    [SerializeField] string playerName;
    [SerializeField] string replacementTag;

    public override string Execute(string input)
    {
        var name = GetUserName();
        var result = input.Replace(replacementTag, name);
        return result;
    }

    private string GetUserName()
    {
        return UsersManager.current.name;
    }
}
