using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Condition", menuName = "Scriptable Object/Conditions/HasItem", order = 1)]
public class HasItem : ConditionSO
{
    public ItemSO Item;

    public override bool Execute()
    {
		// If there's any items whose Pickupable component has an ItemInfo associated with a name equal to Item's name returns true
		// In other words: 
		// foreach (GameObject item in Inventory.Instance.Items) 
		// 		if (item.GetComponent<Pickupable>().ItemInfo.name == Item.name) return true;
		// return false
        return Inventory.Instance.Items.Any(item => item.GetComponent<Pickupable>().ItemInfo.name == Item.name);
    }
}
