﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Remove Item", menuName = "Scriptable Object/Actions/Remove Item", order = 1)]
public class RemoveItem : ActionSO
{
    [SerializeField] string ItemName;

    public override bool Execute(GameObject source = null)
    {
        InventoryUI_Manager.Instance.DeleteSlot(ItemName);
        return true;
    }
}
