﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FlagCondition", menuName = "Scriptable Object/Conditions/HasFlag", order = 1)]
public class ContainsFlag : ConditionSO
{
    public FlagSO flag;

    public override bool Execute()
    {
        if (FlagManager.Contains(flag.flagKeyName))
            return true;
        else return false;
    }
}
