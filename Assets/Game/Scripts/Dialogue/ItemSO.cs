using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Scriptable Object/Items/ItemTest", order = 1)]
public class ItemSO : ScriptableObject 
{
	public string Name { get { return this.name; }}
	public string Type;

    public Sprite UI_Sprite;
}
