﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TPItemName", menuName = "Scriptable Object/TextProcessor/ItemName", order = 1)]
public class TPItemName : TextProcessorSO
{
    [SerializeField] ItemSO item;
    [SerializeField] string replacementTag;

    public override string Execute(string input)
    {
        var result = input.Replace(replacementTag, item.Name);
        return result;
    }
}
