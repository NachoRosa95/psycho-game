﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TextProcessorSO : ScriptableObject
{
    public abstract string Execute(string input);
}
