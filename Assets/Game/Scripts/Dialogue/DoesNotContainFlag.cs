﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FlagCondition", menuName = "Scriptable Object/Conditions/Doesn't Have Flag", order = 1)]
public class DoesNotContainFlag : ConditionSO
{
    public FlagSO flag;

    public override bool Execute()
    {
        if (FlagManager.Contains(flag.flagKeyName))
            return false;
        else return true;
    }
}
