﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AttributeCheck", menuName = "Scriptable Object/Conditions/AttributeCheck", order = 1)]
public class AttributeCheck : ConditionSO
{
    public enum ComparatorEnum
    {
        GreaterThan,
        Equals,
        LessThan
    }

    [SerializeField] PsychologicAttribute attribute;
    [SerializeField] ComparatorEnum operation;
    [SerializeField] float value;

    public override bool Execute()
    {
        if (attribute == null) return false;
        switch (operation)
        {
            case ComparatorEnum.GreaterThan:
                if (attribute.Weight > value) return true;
                break;
            case ComparatorEnum.Equals:
                if (attribute.Weight == value) return true;
                break;
            case ComparatorEnum.LessThan:
                if (attribute.Weight < value) return true;
                break;
        }
        return false;
    }
}
