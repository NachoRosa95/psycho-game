using System.Collections.Generic;
using UnityEngine;

public abstract class ConditionSO : ScriptableObject 
{
	public abstract bool Execute();
}
