﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActivateGO", menuName = "Scriptable Object/Actions/ActivateGameObject", order = 1)]
public class ActivateGameObject : ActionSO 
{
	[SerializeField] string UniqueName;

	public override bool Execute(GameObject source = null)
	{
		var target = GameObject.Find(UniqueName);
		if(!target) { Debug.LogError("GameObject with name "+UniqueName+" not found", this); return false; }

		var activate = target.GetComponent<Activatable>();
		if(!activate) activate = target.GetComponentInChildren<Activatable>();
        if(!activate) { Debug.LogError(UniqueName + " does not contain an Activatable", this); return false; }


        activate.Activate();
		return true;
	}
}