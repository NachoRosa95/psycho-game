using System.Collections.Generic;
using UnityEngine;

public abstract class ActionSO : ScriptableObject 
{
    public abstract bool Execute(GameObject source = null);
}
