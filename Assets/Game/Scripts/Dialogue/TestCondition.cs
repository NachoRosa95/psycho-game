using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Condition", menuName = "Scriptable Object/Conditions/TestCondition", order = 1)]
public class TestCondition : ConditionSO
{
    public bool Value;

    public override bool Execute()
    {
        return Value;
    }
}
