﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FlagSO", menuName = "Scriptable Object/Flag", order = 1)]
public class FlagSO : ScriptableObject
{
    public string flagKeyName;
}
