﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Triggerable2D : MonoBehaviour
{
    [SerializeField] List<string> validTags;
    public UnityEvent OnTriggerEnter = new UnityEvent();
    public UnityEvent OnTriggerExit = new UnityEvent();
    public UnityEvent OnTriggerStay = new UnityEvent();

    void Awake()
    {
        if (validTags.Count == 0) validTags.Add("Player");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!IsValid(other.gameObject)) return;
        OnTriggerEnter?.Invoke();
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!IsValid(other.gameObject)) return;
        OnTriggerExit?.Invoke();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (!IsValid(other.gameObject)) return;
        OnTriggerStay?.Invoke();
    }

    bool IsValid(GameObject gameObject)
    {
        return validTags.Contains(gameObject.tag);
    }
}
