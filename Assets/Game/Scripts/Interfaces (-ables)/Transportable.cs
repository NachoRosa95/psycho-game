﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transportable : MonoBehaviour
{
    Collider2D col;

    public Collider2D Collider
    {
        get { return col; }
    }

    private void Awake()
    {
        col = GetComponent<Collider2D>();
    }

    void TurnOnCollider()
    {
        if (col) col.enabled = true;
        else Debug.LogError("Error - Collider is null", this);
    }
    void TurnOffCollider()
    {
        if (col) col.enabled = false;
        else Debug.LogError("Error - Collider is null", this);
    }
    public void MakeTransportableYourChildren(Transform target)
    {
        transform.SetParent(target);
    }
}
