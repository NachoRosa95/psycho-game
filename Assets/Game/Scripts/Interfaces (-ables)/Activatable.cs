﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Activatable : MonoBehaviour
{
    public UnityEvent OnActivated = new UnityEvent();

    public void Activate()
    {
        OnActivated?.Invoke();
    }
}
