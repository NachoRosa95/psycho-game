﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Destructible : MonoBehaviour
{
    [SerializeField] float delay = 0.01f;
    public UnityEvent OnDestroyed = new UnityEvent();

    void OnDeath()
    {
        //animation etc
        OnDestroyed?.Invoke();
        Destroy(gameObject, delay);
    }
}
