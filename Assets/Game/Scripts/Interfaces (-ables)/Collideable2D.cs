﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Collideable2D : MonoBehaviour 
{
	[SerializeField] List<string> validTags;
	public UnityEvent OnCollided = new UnityEvent();

	void Awake()
	{
		if(validTags.Count == 0)
		{
			validTags = new List<string>();
			validTags.Add("Player");
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(!validTags.Contains(collision.gameObject.tag)) return;
		OnCollided?.Invoke();
	}
}
