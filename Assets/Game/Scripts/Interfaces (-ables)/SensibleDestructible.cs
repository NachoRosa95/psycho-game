﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SensibleDestructible : MonoBehaviour
{
    [SerializeField] ParticleSystem particles;
    [SerializeField] float delay;
    [SerializeField] AudioClip audioBeginToDestroy;
    [SerializeField] AudioClip audioDestroy;
    [SerializeField] Animator animator;

    [SerializeField] bool regenerablePlatform;
    [SerializeField] float regenTimer;

    VanishGroup vanishComponent;
    Collider2D collider;
    [SerializeField] Collider2D trigger;

    const int ignoreLayer = 12;

    private void Awake()
    {
        vanishComponent = GetComponent<VanishGroup>();
        collider = GetComponent<Collider2D>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == ignoreLayer) return;

        if (regenerablePlatform)
        {
            if (particles) particles.Play();
            AudioSource.PlayClipAtPoint(audioBeginToDestroy, transform.position);
            Invoke(nameof(DestroyPlatformTemporarily), delay);
            if(trigger) trigger.enabled = false;
            if (!animator) return;
            animator.SetTrigger("Break");
            
        }
        else Destroy(gameObject, delay);
    }

    void DestroyPlatformTemporarily()
    {
        AudioSource.PlayClipAtPoint(audioDestroy, transform.position);
        //vanishComponent.VanishOnCollision = true;
        vanishComponent.TurnOnVanish();
        collider.enabled = false;
        StartCoroutine(nameof(RegenerateCoroutine));
    }


    IEnumerator RegenerateCoroutine()
    {
        yield return new WaitForSeconds(regenTimer);
        //vanishComponent.VanishOnCollision = false;
        vanishComponent.TurnOffVanish();
        collider.enabled = true;
        if (trigger) trigger.enabled = true;
    }
}
