﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damageable : MonoBehaviour
{
    public UnityEvent OnObjectDamaged = new UnityEvent();

    GameObject damageDealer;
    public GameObject DamageDealer { get { return damageDealer; } set { damageDealer = value; } }

    HealthComponent health;
    public HealthComponent Health { get { return health; } set { health = value; } }

    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip damagedClip;
    [SerializeField] float pitchVariation;
    private float initialPitch;

    private void Awake()
    {
        health = GetComponent<HealthComponent>();
        if (!audioSource) audioSource = GetComponent<AudioSource>();
        if (audioSource) initialPitch = audioSource.pitch;
    }

    public bool Damage(float amount, GameObject damageDealer = null)
    {
        BroadcastMessage("OnDamaged", SendMessageOptions.DontRequireReceiver);

        if (health == null) return false;
        var damaged = health.Damage(amount, damageDealer);
        if(!damaged) return false;
        if (audioSource && audioSource.enabled)
        {
            audioSource.pitch = Random.Range(initialPitch - pitchVariation, initialPitch + pitchVariation);
            AudioUtil.InstanstiateAudio(audioSource, transform.position, true, damagedClip);
            audioSource.pitch = initialPitch;
        }
        SendMessage("OnDamaged", SendMessageOptions.DontRequireReceiver);
        OnObjectDamaged?.Invoke();
        return true;
    }

    public bool DirectDamage(float amount, GameObject damageDealer = null)
    {
        if (health == null) return false;
        health.DirectDamage(amount, damageDealer);
        return true;
    }

}
