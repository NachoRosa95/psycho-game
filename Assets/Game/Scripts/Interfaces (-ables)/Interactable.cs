﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class Interactable : MonoBehaviour
{
    public UnityEvent OnInteracted = new UnityEvent();

    [SerializeField] GameObject interactLabelPrefab;
    [SerializeField] Canvas canvas;
    [SerializeField] string buttonName = "Interact";
    [SerializeField] string labelText;
    [SerializeField] float cooldown = 1;
    GameObject interactLabel;
    bool onCooldown;

    void Awake()
    {
        if (canvas == null) canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        if (!canvas) { Debug.LogError("Gameobject with name 'Canvas' does not exist or doesn't contain a component of type Canvas", this); }
    }

    void OnDestroy()
    {
        if (interactLabel) interactLabel.SetActive(false);
    }

    void Update()
    {
        if (interactLabel == null || !interactLabel.activeSelf) return;

        if (Input.GetButtonDown(buttonName)) Interact();
    }

    public void ShowLabel()
    {
        if (!enabled) return;
        if (onCooldown) return;
        if (interactLabel == null) interactLabel = CreateLabel();
        if (!interactLabel) return;

        var text = "[" + buttonName + "] " + labelText;
        var textLabel = interactLabel.GetComponent<Text>();
        if (textLabel != null) textLabel.text = text;

        var textMesh = interactLabel.GetComponent<TextMeshProUGUI>();
        if (textMesh) textMesh.text = text;

        interactLabel.SetActive(true);
    }

    public void HideLabel()
    {
        if (interactLabel == null) interactLabel = CreateLabel();
        if (!canvas || !interactLabel) return;

        interactLabel.SetActive(false);
    }

    GameObject CreateLabel()
    {
        if (!canvas) return null;
        var newLabel = Instantiate(interactLabelPrefab, canvas.transform);
        return newLabel;
    }

    void StopCooldown()
    {
        onCooldown = false;
    }

    public void Interact()
    {
        if (!enabled) return;
        onCooldown = true;
        Invoke("StopCooldown", cooldown);
        OnInteracted?.Invoke();
        if(interactLabel) interactLabel.SetActive(false);
    }

    private void OnDisable()
    {
        StopCooldown();
        HideLabel();
    }

}
