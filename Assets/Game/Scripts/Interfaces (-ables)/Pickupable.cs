﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickupable : MonoBehaviour
{
    public ItemSO ItemInfo;
    [SerializeField] AudioClip audioClip;
    private AudioSource audioSource;
	private Animator animator;

    void Awake()
    {
        if (ItemInfo == null)
        {
            Debug.LogError("Pickupable.cs - ItemInfo unassigned", this);
        }
        audioSource = GetComponent<AudioSource>();
		animator = GetComponent<Animator>();
    }

    public void Activate()
    {
        if (!enabled) return;
        var inventory = Inventory.Instance.Items;
        if (!inventory.Contains(gameObject))
        {
            Inventory.Instance.Items.Add(gameObject);
            //Debug.Log(gameObject);
            InventoryUI_Manager.Instance.SetNewSlot(gameObject, ItemInfo);
        }
        Invoke("Disable", 0.2f);
		if(animator) animator.SetBool("Active", true);
        AudioUtil.InstanstiateAudio(audioSource, transform.position, true, audioClip);
        enabled = false;
    }

    void Disable()
    {
        gameObject.SetActive(false);
    }
}
