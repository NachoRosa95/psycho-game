﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AttributeCheat : MonoBehaviour
{
    [SerializeField] GameObject attributeEmpaty;
    [SerializeField] GameObject attributeAgresive;
    [SerializeField] GameObject attributeReflexive;
    [SerializeField] GameObject attributeImpulsivity;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            attributeEmpaty.SetActive(false);
            attributeAgresive.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            attributeEmpaty.SetActive(true);
            attributeAgresive.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            attributeReflexive.SetActive(true);
            attributeImpulsivity.SetActive(true);
        }
    }
}
