﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class TypewriterEffect : MonoBehaviour
{
    [TextArea] public string Text;
    [SerializeField] AudioSource audioSource;
    [SerializeField] string pauseChars = " .,;";
    [SerializeField] string pauseMultipliers = "2555";
    [SerializeField] float skipMultiplier = 0.05f;

    Text textComponent;
    TextMeshProUGUI textMesh;
    Settings settings;
    string curText;
    float elapsedTime;
    float interval;
    int index;
    AudioClip defaultClip;

    void Awake()
    {
        defaultClip = audioSource.clip;
    }

    void OnEnable()
    {
        if (!textComponent) textComponent = GetComponent<Text>();
        if (!textMesh) textMesh = GetComponent<TextMeshProUGUI>();
        if (!textComponent && !textMesh) Debug.LogError("No text component attached", this);

        settings = Settings.Instance;
        if (!settings) Debug.LogError("No settings found", this);

        curText = "";
        index = 0;
        elapsedTime = 0;
    }

    void Update()
    {
        if (!settings || (!textComponent && !textMesh)) { enabled = false; return; }

        elapsedTime += Time.unscaledDeltaTime;
        if (elapsedTime < interval) return;
        interval = settings.TextSpeed;
        elapsedTime %= interval;

        if (index >= Text.Length)
        {
            enabled = false;
            return;
        }

        DoSkip();

        if (textComponent) textComponent.text = curText;
        if (textMesh) textMesh.text = curText;
        audioSource.PlayOneShot(audioSource.clip ?? defaultClip);

        index++;
    }

    void DoSkip()
    {
        string curWord = Text[index].ToString();
        bool skip = false;
        if (pauseChars.Contains(Text[index].ToString())) { interval *= pauseMultipliers[pauseChars.IndexOf(Text[index])] - '0'; }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            skip = true;
        }
        else
        {
            var joysticks = Input.GetJoystickNames();
            if (joysticks.Length > 0 && joysticks.Any(joystick => !string.IsNullOrEmpty(joystick)))
            {
                if (Input.GetButton("Skip")) skip = true;
            }
        }
        if (skip)
        {
            interval *= skipMultiplier;
            do
            {
                index++;
                if (index < Text.Length)
                    curWord += Text[index];
            } while (index + 1 < Text.Length && Text[index] != ' ');
        }
        curText += curWord;
    }
}
