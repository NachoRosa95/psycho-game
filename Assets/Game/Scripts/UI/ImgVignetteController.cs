﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImgVignetteController : MonoBehaviour
{
    [SerializeField] HealthComponent health;
    [SerializeField] List<CanvasGroup> canvasGroups;
    [SerializeField] List<Animator> animators;
    [SerializeField] AudioLowPassFilter lowPassFilter;
    [SerializeField] AudioSource hurtAudio;
    [SerializeField] float maxIntensity;
    [SerializeField] float interpolationSpeed;
    [SerializeField] float maxPercentage;
    [SerializeField] float minPercentage;
    [SerializeField] float minVolume;
    [SerializeField] float maxVolume;
    private float desiredAmount;
    // private float maxOffset = 0.5f;
    private const float minFrequency = 1500;
    private const float maxFrequency = 5000;

    private void Awake()
    {
        desiredAmount = 0;
        health.OnChanged.AddListener((value, key) => OnHealthChanged(value, key));
    }

    private void OnEnable()
    {
        ChangeIntensity(0);
    }

    private void OnDisable()
    {
        ChangeIntensity(0);
    }

    private void Update()
    {
        if (canvasGroups.Count == 0) return;
        // var intensity = Mathf.Lerp(vigSettings.intensity.value, desiredAmount + eval, t % 1f * interpolationSpeed);
        var intensity = Mathf.MoveTowards(canvasGroups[0].alpha, desiredAmount, interpolationSpeed * Time.deltaTime);

        var freqDiff = maxFrequency - minFrequency;
        var freqOffset = freqDiff * desiredAmount;
        if (lowPassFilter) lowPassFilter.cutoffFrequency = maxFrequency - freqOffset;

        var volumeDiff = maxVolume - minVolume;
        var volumeOffset = volumeDiff * desiredAmount;
        if (hurtAudio) hurtAudio.volume = minVolume + volumeOffset;

        intensity *= maxIntensity;
        ChangeIntensity(intensity);
    }

    void OnHealthChanged(float value, string key)
    {
        var percentage = health.Health / health.MaxHealth;
        if (percentage < maxPercentage)
            desiredAmount = 1 - (percentage - minPercentage) / (maxPercentage - minPercentage);
        else
            desiredAmount = 0;
    }

    void ChangeIntensity(float intensity)
    {
        foreach (CanvasGroup group in canvasGroups)
        {
            group.alpha = intensity;
        }
        foreach (Animator animator in animators)
        {
            animator.SetFloat("Intensity", intensity);
        }
    }

}
