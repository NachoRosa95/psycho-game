﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class VignetteController : MonoBehaviour
{
    [SerializeField] HealthComponent health;
    [SerializeField] PostProcessProfile profile;
    [SerializeField] AnimationCurve vignetteAnimationCurve;
    [SerializeField] AudioLowPassFilter lowPassFilter;
    [SerializeField] float maxIntensity = 1;
    [SerializeField] float animationIntensity = 1;
    [SerializeField] float interpolationSpeed;
    [SerializeField] float maxPercentage;
    [SerializeField] float minPercentage;
    private float desiredAmount;
    private float t;
    private float eval;
    // private float maxOffset = 0.5f;
    private const float minFrequency = 1500;
    private const float maxFrequency = 5000;

    private void Awake()
    {
        desiredAmount = 0;
        health.OnChanged.AddListener((value, key) => OnHealthChanged(value, key));
    }

    private void OnEnable()
    {
        ChangeIntensity(profile, 0);
    }

    private void OnDisable()
    {
        ChangeIntensity(profile, 0);
    }

    private void Update()
    {
        t += Time.deltaTime;
        if (desiredAmount > minPercentage) eval = vignetteAnimationCurve.Evaluate(t % 1f);
        var vigSettings = profile.GetSetting<Vignette>();
        // var intensity = Mathf.Lerp(vigSettings.intensity.value, desiredAmount + eval, t % 1f * interpolationSpeed);
        desiredAmount = Mathf.Clamp01(desiredAmount);
        var intensity = Mathf.MoveTowards(vigSettings.intensity.value, desiredAmount + animationIntensity * eval, interpolationSpeed * Time.deltaTime);

        var freqDiff = maxFrequency - minFrequency;
        var freqOffset = freqDiff * desiredAmount;
        if (lowPassFilter) lowPassFilter.cutoffFrequency = maxFrequency - freqOffset;

        intensity *= maxIntensity;
        ChangeIntensity(profile, intensity);
    }

    void OnHealthChanged(float value, string key)
    {
        var percentage = health.Health / health.MaxHealth;
        if (percentage < maxPercentage)
            desiredAmount = 1 - (percentage - minPercentage) / (maxPercentage - minPercentage);
        else
            desiredAmount = 0;
    }

    void ChangeIntensity(PostProcessProfile profile, float intensity)
    {
        var chromSettings = profile.GetSetting<ChromaticAberration>();
        var vigSettings = profile.GetSetting<Vignette>();
        vigSettings.opacity.value = intensity;
        chromSettings.intensity.value = intensity * 2;
    }

}
