﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionWheel : MonoBehaviour
{
    public Animator[] animators;
    int lastLength;

    void Awake()
    {
        InvokeRepeating("RefreshList", 0, 0.1f);
    }

    void Update()
    {
        UpdateOffset();
    }

    void UpdateOffset()
    {
        if (animators.Length == lastLength) return;
        foreach (Animator animator in animators)
        {
            animator.SetTrigger("Reset");
        }
        Invoke("RefreshAnimators",0.01f);
        lastLength = animators.Length;
    }

    void RefreshList()
    {
        animators = GetComponentsInChildren<Animator>();
    }

    void RefreshAnimators()
    {
        for (var i = 0; i < animators.Length; i++)
        {
            var animator = animators[i];
            var offset = (float)i / animators.Length;
            animator.SetFloat("Offset", offset);
        }
    }

    void OnEnable()
    {
        foreach (Animator animator in animators)
        {
            animator.enabled = true;
        }
    }
}
