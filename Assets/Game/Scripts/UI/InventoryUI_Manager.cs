﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI_Manager : MonoBehaviour
{
    private static InventoryUI_Manager instance;
    public static InventoryUI_Manager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject("Inventory");
                DontDestroyOnLoad(go);
                instance = go.AddComponent<InventoryUI_Manager>();
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    List<GameObject> inventorySlots = new List<GameObject>();
    [SerializeField] GameObject slotUIPrefab;
    [SerializeField] int slotOffsetX;


    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            DeleteSlot("Raspberry");
        }
    }

    public void SetNewSlot(GameObject item, ItemSO itemInfo)
    {
        //Create the Slot prefab and assign it as child
        var newSlot = Instantiate(slotUIPrefab);
        var rectTransform = newSlot.GetComponent<RectTransform>();
        rectTransform.parent = this.transform;

        inventorySlots.Add(newSlot);

        //Search the child object with the script.
        //We need it to replace it's sprite.
        var inventoryUI_Slot = newSlot.GetComponentInChildren<InventoryUI_Slot>();
        inventoryUI_Slot.ItemName = itemInfo.name;

        var itemSlotImage = inventoryUI_Slot.GetComponent<Image>();
        itemSlotImage.sprite = itemInfo.UI_Sprite;

        UpdateSlotPositions();
    }

    public void DeleteSlot(string name)
    {
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            var inventoryUI_Slot = inventorySlots[i].GetComponentInChildren<InventoryUI_Slot>();
            if (name == inventoryUI_Slot.ItemName)
            {
                Destroy(inventorySlots[i]);
                inventorySlots.Remove(inventorySlots[i]);
                
                UpdateSlotPositions();
            }
        }
    }

    public void UpdateSlotPositions()
    {
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            var rectTransform = inventorySlots[i].GetComponent<RectTransform>();

            Vector3 newPosition = new Vector3(slotOffsetX * i, 0, 0);
            rectTransform.localPosition = newPosition;
        }
    }
}
