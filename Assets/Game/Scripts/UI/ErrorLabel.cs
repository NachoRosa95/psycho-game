﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ErrorLabel : MonoBehaviour
{
    TextMeshProUGUI text;
    [SerializeField] float fadeTimeMultiplier;

    private void Awake()
    {
        if (!text) text = GetComponent<TextMeshProUGUI>();
        text.alpha = 0;
    }

    private void OnEnable()
    {
    }

    public void ShowMessage(string message)
    {
        text.text = message;
        text.alpha = 1;
        StartCoroutine(nameof(Fade), true);
    }

    IEnumerator Fade(bool fadeAway)
    {
        if (fadeAway)
        {
            for (float i = 1; i >= 0; i -= Time.deltaTime * fadeTimeMultiplier)
            {
                text.alpha = i;
                //text.color = new Color(1, 1, 1, i);
                yield return null;
            }
            text.alpha = 0;
        }
        //else
        //{
        //    // loop over 1 second
        //    for (float i = 0; i <= 1; i += Time.deltaTime)
        //    {
        //        text.alpha = i;
        //        //text.color = new Color(1, 1, 1, i);
        //        yield return null;
        //    }
        //}
    }
}
