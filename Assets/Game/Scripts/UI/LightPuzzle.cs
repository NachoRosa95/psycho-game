﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightPuzzle : PowerSource
{
    // [SerializeField] Transform buttonPrefab;
    // [SerializeField] int width = 3;
    // [SerializeField] int height = 3;
    // [SerializeField] float buttonSpacingX = 0.1f;
    // [SerializeField] float buttonSpacingY = 0.1f;
    [SerializeField] bool[,] matrix;
    [SerializeField] Transform[,] buttonMatrix;
    [SerializeField] List<Vector2> initState;
    [SerializeField] bool debug;
    // [SerializeField] bool reset;

    Canvas canvas;
    GridLayoutGroup grid;
    private int width;
    private int height;
    private int moveCount = 0;

    private void Awake()
    {
        grid = GetComponent<GridLayoutGroup>();
        BuildPuzzle();
        ResetPuzzle();
    }

    // void Update()
    // {
    //     if (reset)
    //     {
    //         reset = false;
    //         ResetPuzzle();
    //     }
    // }

    public void PressTile(int i, int j)
    {
        if (debug) Debug.Log(i + " " + j);
        if (!enabled) return;
        Activate(i, j);
        Activate(i - 1, j);
        Activate(i + 1, j);
        Activate(i, j - 1);
        Activate(i, j + 1);
        moveCount++;
        // PrintMatrix();

        if (Win()) OnWin();
    }

    void Activate(int i, int j, bool? forceState = null)
    {
        if (i < 0 || i > height - 1 || j < 0 || j > width - 1)
            return;
        matrix[i, j] = !matrix[i, j];
        if (forceState.HasValue) matrix[i, j] = forceState.Value;
        var button = buttonMatrix[i, j];
        UpdateButton(button, matrix[i, j]);
    }

    void UpdateButton(Transform button, bool state)
    {
        if (!button) return;
        var image = button.GetComponent<Image>();
        image.color = state ? Color.white : Color.gray;
    }


    bool Win()
    {
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (!matrix[i, j])
                    return false;
            }
        }
        return true;
    }

    void OnWin()
    {
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                var button = buttonMatrix[i, j];
                var image = button.GetComponent<Image>();
                image.color = Color.green;
                enabled = false;
            }
        }
        State = true;
    }

    void BuildPuzzle()
    {
        var children = new List<Transform>(transform.GetComponentsInChildren<Transform>());
        children.Remove(transform);
        width = grid.constraintCount;
        height = (int)Mathf.Ceil(children.Count / width);
        if(debug) Debug.Log(width);
        if (debug) Debug.Log(height);

        matrix = new bool[height, width];
        buttonMatrix = new Transform[height, width];

        // var buttonTransform = buttonPrefab.GetComponent<RectTransform>();
        // var posMarker = new Vector2(buttonSpacingX + buttonTransform.rect.width / 2, buttonSpacingY + buttonTransform.rect.height / 2);
        var k = 0;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (k >= children.Count) break;
                var newButton = children[k];
                k++;
                if (!newButton) continue;
                // var newButton = Instantiate(buttonPrefab, transform);
                // var newRect = newButton.GetComponent<RectTransform>();
                // newRect.anchoredPosition = posMarker;
                // posMarker.x += buttonTransform.rect.width + buttonSpacingX;
                // var puzzleButton = newButton.GetComponent<PuzzleTile>();
                // puzzleButton.Puzzle = this;
                // puzzleButton.I = i;
                // puzzleButton.J = j;
                var button = newButton.GetComponent<Button>();
                if (debug) Debug.Log(i + " " + j);
                var posI = i;
                var posJ = j;
                button.onClick.AddListener(() => PressTile(posI, posJ));
                buttonMatrix[i, j] = newButton;
                var image = newButton.GetComponent<Image>();
                image.color = matrix[i, j] ? Color.white : Color.gray;
            }
            // posMarker.y += buttonTransform.rect.height + buttonSpacingY;
            // posMarker.x = buttonSpacingX + buttonTransform.rect.width / 2;
        }
    }

    public void ResetPuzzle()
    {
        State = false;
        moveCount = 0;
        matrix = new bool[height, width];
        foreach (Transform button in buttonMatrix)
        {
            UpdateButton(button, false);
        }
        foreach (Vector2 pos in initState)
        {
            Activate((int)pos.x, (int)pos.y);
        }
    }

    public void PrintMatrix()
    {
        for (int i = height - 1; i >= 0; i--)
        {
            string line = "";
            for (int j = 0; j < width; j++)
            {
                var state = matrix[i, j] ? "X" : "O";
                line += "[" + state + "] ";
            }
            print(line);
        }
        print("___________________");

    }

    // private void OnDrawGizmos()
    // {
    //     if (matrix == null) matrix = new bool[height, width];                // Asco total
    //     if (buttonPrefab == null) return;

    //     Gizmos.color = Color.blue;

    //     //  Dimensiones del canvas
    //     var thisTransform = GetComponent<RectTransform>();
    //     var scaledWidth = thisTransform.rect.width * thisTransform.localScale.x;
    //     var scaledHeight = thisTransform.rect.height * thisTransform.localScale.y;

    //     // Dimensiones del puzzle
    //     var buttonTransform = buttonPrefab.GetComponent<RectTransform>();
    //     var sizeX = (buttonTransform.rect.width + buttonSpacingX) * width + buttonSpacingX;
    //     var sizeY = (buttonTransform.rect.height + buttonSpacingY) * height + buttonSpacingY;
    //     sizeX *= transform.localScale.x;
    //     sizeY *= transform.localScale.y;

    //     // Centro del puzzle a generar
    //     var center = new Vector3(thisTransform.position.x + sizeX / 2 - scaledWidth / 2, thisTransform.position.y + sizeY / 2 - scaledHeight / 2, 0);

    //     Gizmos.DrawWireCube(center, new Vector3(sizeX, sizeY, 0));
    // }
}
