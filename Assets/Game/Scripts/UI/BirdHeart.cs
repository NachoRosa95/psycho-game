﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdHeart : MonoBehaviour
{
    public void TurnOff()
    {
        gameObject.SetActive(false);
    }

    public void TurnOn()
    {
        gameObject.SetActive(true);
    }
}
