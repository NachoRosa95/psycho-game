﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class DialogueHandler : MonoBehaviour
{
    public DialogueSO Dialogue;
    [SerializeField] TypewriterEffect typeWriter;
    [SerializeField] List<Button> buttonAnswers;
    [SerializeField] List<TextProcessorSO> textProcessors = new List<TextProcessorSO>();
    private AudioSource audioSource;
    private Animator animator;
    private int buttonIndex;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    void OnEnable()
    {
        if (animator)
        {
            animator.enabled = true;
            animator.SetBool("Open", true);
        }
        Redraw();
    }

    void OnDisable()
    {
        if (animator) animator.SetBool("Open", false);
    }

    void SetActiveDialogue(int value)
    {
        var active = value > 0;
        TimeManager.Instance.Pause(active);
        gameObject.SetActive(active);
    }

    void Redraw()
    {
        if (Dialogue == null) return;
        if (Dialogue.name == "Root") { if (!ProcessRoot()) { Debug.LogError("There is a Root without a valid set of conditions. It should have a default.", this); return; } }
        audioSource.clip = Dialogue.AudioClip;
        Dialogue.DialogueText = GetLanguageText(Dialogue);
        typeWriter.Text = ProcessText(Dialogue.DialogueText);
        typeWriter.enabled = false; //
        typeWriter.enabled = true;  // better way to do this ???
        DeactivateButtons();
        UpdateButtons();
    }

    bool ProcessRoot()
    {
        foreach (DialogueOption option in Dialogue.OptionAnswers)
        {
            // If all conditions in this option are true
            bool valid = option.Conditions.All(condition => condition == null || condition.Execute());
            if (valid)
            {
                Dialogue = option.TargetDialogue;
                return true;
            }
        }

        // If none of the conditions are true 
        return false;
    }

    string GetLanguageText(DialogueSO dialogue)
    {
        var manager = LanguageSystem.LanguageManager.Instance;
        if (!manager) return Dialogue.DialogueText;

        return manager.GetText(dialogue.Key);
    }

    string GetLanguageText(DialogueOption option)
    {
        var manager = LanguageSystem.LanguageManager.Instance;
        if (!manager) return option.OptionText;

        return manager.GetText(option.Key);
    }

    string ProcessText(string text)
    {
        string processedText = text;
        foreach (TextProcessorSO tp in textProcessors)
        {
            processedText = tp.Execute(processedText);
        }
        return processedText;
    }

    void UpdateButtons()
    {
        for (var i = 0; i < Dialogue.OptionAnswers.Count; i++)
        {
            // Short for a foreach loop that checks if all conditions are true
            var enabled = Dialogue.OptionAnswers[i].Conditions.All(condition => condition == null || condition.Execute() == true);
            if (enabled)
                UpdateButton(buttonAnswers[buttonIndex], Dialogue.OptionAnswers[i]);
        }

        var joysticks = Input.GetJoystickNames();
        // If there are any non-null joysticks
        if (joysticks.Length > 0 && joysticks.Any(joystick => !string.IsNullOrEmpty(joystick)))
        {
            // Select the first button so that the gamepad is able to navigate through them
            // Must wait a frame to select the button or it doesn't work for some reason
            StartCoroutine("SelectLater", buttonAnswers[0].gameObject);
        }
    }

    void DeactivateButtons()
    {
        for (var i = 0; i < buttonAnswers.Count; i++)
        {
            buttonAnswers[i].gameObject.SetActive(false);
        }
        buttonIndex = 0;
    }

    void UpdateButton(Button button, DialogueOption option)
    {
        var text = button.gameObject.GetComponentInChildren<Text>();
        var textMesh = button.gameObject.GetComponentInChildren<TextMeshProUGUI>();
        if (!button || (!text && !textMesh) || option.OptionText == "") return;

        option.OptionText = GetLanguageText(option);
        var optionText = ProcessText(option.OptionText);
        if (text) text.text = optionText;
        if (textMesh) textMesh.text = optionText;
        button.gameObject.SetActive(true);
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => SwitchDialogue(option.TargetDialogue));
        foreach (ActionSO action in option.Actions)
        {
            if (action != null) button.onClick.AddListener(() => action.Execute());
        }
        if (option.Attribute != null) button.onClick.AddListener(() => option.Attribute.Inject(option.Weight));
        buttonIndex++;
    }

    void SwitchDialogue(DialogueSO dialogue)
    {
        if (dialogue == null) { OnDisable(); return; }
        this.Dialogue = dialogue;
        Redraw();
    }

    IEnumerator SelectLater(GameObject go)
    {
        yield return null;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(go);
    }
}
