﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class AttributeLabel : MonoBehaviour
{   
    [SerializeField] Color increaseColor = Color.cyan;
    [SerializeField] Color decreaseColor = Color.red;
    [SerializeField] float colorLerpDuration = 1;
    [SerializeField] PsychologicAttribute attribute;
    Color idleColor;
    Text text;
    TextMeshProUGUI textMesh;
    float lastValue;
    float t;



    void Awake()
    {       
        text = GetComponent<Text>();
        textMesh = GetComponent<TextMeshProUGUI>();
        if (text) idleColor = text.color;
        if (textMesh) idleColor = textMesh.color;
        UpdateText();
    }

    void Update()
    {       

        var value = attribute.Weight;
        t += Time.deltaTime;

        if (text) text.color = Color.Lerp(text.color, idleColor, t / colorLerpDuration);
        if (textMesh) textMesh.color = Color.Lerp(textMesh.color, idleColor, t / colorLerpDuration);
        // Debug.Log(text.color);
        if (value == lastValue) return;
        if(text && value > lastValue) text.color = increaseColor;
        if(text && value < lastValue) text.color = decreaseColor;
        if(textMesh && value > lastValue) textMesh.color = increaseColor;
        if(textMesh && value < lastValue) textMesh.color = decreaseColor;
        UpdateText();
        lastValue = value;
        t = 0;
    }

    void UpdateText()
    {
        var value = attribute.Weight;
        if (text) text.text = attribute.name + string.Format(": {0:F2}", value);
        if (textMesh) textMesh.text = attribute.name + string.Format(": {0:F2}", value); ;
    }
}
