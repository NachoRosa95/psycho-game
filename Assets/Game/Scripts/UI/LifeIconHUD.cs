﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeIconHUD : MonoBehaviour
{
    [SerializeField] HealthComponent playerhealth;
    [SerializeField] List<BirdHeart> heart = new List<BirdHeart>();

    private void Awake()
    {
        HealthComponent playerhealth = GetComponent<HealthComponent>();
    }

    private void Update()
    {
        DeactiveIcons();
        ActivateIcons();       
    }

    private void ActivateIcons()
    {
        for (int i = 0; i < heart.Count; i++)
        {
            if (playerhealth.Health == 4)
            {
                heart[0].TurnOn();
            }

            if (playerhealth.Health >= 3)
            {
                heart[1].TurnOn();
            }

            if (playerhealth.Health >= 2)
            {
                heart[2].TurnOn();
            }

            if (playerhealth.Health >= 1)
            {
                heart[3].TurnOn();
            }

            //if(playerhealth.Health == 0)
            //{
            //    for (int j = 0; j < heart.Count; j++)
            //    {
            //        heart[j].TurnOn();
            //    }
            //}
        }
    }

    private void DeactiveIcons()
    {
        for (int i = 0; i < heart.Count; i++)
        {
            if(playerhealth.Health == 3)
            {
                heart[0].TurnOff();
            }
            if (playerhealth.Health == 2)
            {
                heart[1].TurnOff();
            }
            if (playerhealth.Health == 1)
            {
                heart[2].TurnOff();
            }
            if (playerhealth.Health == 0)
            {
                heart[3].TurnOff();
            }
        }
    }
}
