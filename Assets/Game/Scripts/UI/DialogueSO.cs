﻿// Objective: Mass Effect style dialogue system integrating psych attributes
// Reqs: 		 
//	Tone of speaker: friendly, aggressive, neutral (sometimes)
//	Gather info option: Optional, increases empathy and may unlock features
//	Respond option: friendly, aggressive, neutral (sometimes). Increases empathy or aggressiveness.
//  Cooperate option: empathy boost, affects story

using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using LanguageSystem;

[CreateAssetMenu(fileName = "Dialogue", menuName = "Scriptable Object/Dialogue", order = 1)]
public class DialogueSO : ScriptableObject
{
    // [HideInInspector] public string key;
    public AudioClip AudioClip;
    public string Key;
    [TextArea(1, 8)]
    public string DialogueText;
    [Space(10)]
    public List<DialogueOption> OptionAnswers = new List<DialogueOption>();
}

[System.Serializable]
public class DialogueOption
{
    [TextArea(1, 8)]
    public string OptionText;
    public string Key;
    [Space(10)]
    public PsychologicAttribute Attribute;
    public float Weight;
    [Space(10)]
    public DialogueSO TargetDialogue;
    public List<ConditionSO> Conditions = new List<ConditionSO>();
    public List<ActionSO> Actions = new List<ActionSO>();
    public IntentEnum Intent;

    public enum IntentEnum
    {
        Neutral,
        Friendly,
        Aggressive
    }
}
