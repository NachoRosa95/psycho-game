﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowDialogue : MonoBehaviour
{
    [SerializeField] DialogueSO dialogue;
    [SerializeField] DialogueHandler dialogueHandler;
    [SerializeField] Transform root;
    // Added invert field for characters that start facing Right (most of them)
    [SerializeField] bool invert = true;
    GameObject player;

    void Awake()
    {
        player = GameObject.FindWithTag("Player");
    }

    public void OnInteracted()
    {
        Execute();
    }

    void OnDamaged()
    {
        var target = GetComponent<Interactable>();
        if (target)
        {
            target.enabled = false;
        }
    }

    void Execute()
    {
        if (dialogue == null) return;
        dialogueHandler.Dialogue = dialogue;
        dialogueHandler.gameObject.SetActive(true);
        if (!player) return;
        var flip = player.transform.position.x < root.position.x;
        FlipRenderers(flip);
    }

    void FlipRenderers(bool flip = true)
    {
        var newScale = root.transform.localScale;
        if (invert)
        {
            newScale.x = flip ? -1 : 1;
        }
        else
        {
            newScale.x = flip ? 1 : -1;
        }
        root.transform.localScale = newScale;
    }
}
