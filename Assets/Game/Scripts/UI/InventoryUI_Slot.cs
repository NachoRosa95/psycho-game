﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI_Slot : MonoBehaviour
{
    string itemName;

    public string ItemName { get => itemName; set => itemName = value; }
}
