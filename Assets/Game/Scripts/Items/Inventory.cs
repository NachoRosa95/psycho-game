﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour 
{
	public List<GameObject> Items = new List<GameObject>();

    private static Inventory instance;

    public static Inventory Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject("Inventory");
                DontDestroyOnLoad(go);
                instance = go.AddComponent<Inventory>();
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }
}
