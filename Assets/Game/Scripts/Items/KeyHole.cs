﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class KeyHole : MonoBehaviour
{
    [SerializeField] GameObject key;
    [SerializeField] AudioClip openClip;
    [SerializeField] AudioClip closeClip;
    [SerializeField] float closeTime;
    Collider2D col;
    SpriteRenderer spriteRenderer;
    AudioSource audioSource;
    Animator animator;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        col = GetComponent<Collider2D>();
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    void OnTriggeredStay()
    {
        CloseDoor(closeTime);
    }

    void OnTriggeredEnter()
    {
        var inventory = Inventory.Instance.Items;
        if (inventory.Contains(key))
        {
            OpenDoor();
        }
    }

    public void OpenDoor()
    {
        if (audioSource) audioSource.PlayOneShot(openClip);
        if (animator) animator.SetTrigger("Open");
        col.enabled = false;
        // spriteRenderer.enabled = false;
        SendMessage("OnSolved", gameObject, SendMessageOptions.DontRequireReceiver);
        if (closeTime > 0) CloseDoor(closeTime);
    }

    public void CloseDoor()
    {
        // TODO: avoid closing when player is on top of the door
        if (audioSource) audioSource.PlayOneShot(closeClip);
        if (animator) animator.SetTrigger("Close");
        col.enabled = true;
        // spriteRenderer.enabled = true;
    }

    public void CloseDoor(float delay)
    {
        CancelInvoke("CloseDoor");
        Invoke("CloseDoor", delay);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if (key && key.activeInHierarchy) Gizmos.DrawLine(transform.position, key.transform.position);
    }
}
