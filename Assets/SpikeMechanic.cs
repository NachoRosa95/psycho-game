﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeMechanic : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float moveBackSpeed;

    Vector3 startingPosition;

    bool isMovingDown;
    bool isOn;

    private void Awake()
    {
        startingPosition = transform.position;
    }

    void Update()
    {
        if (isOn && !isMovingDown)
        {
            transform.position += new Vector3(0, speed * Time.deltaTime, 0);
        }

        if (isMovingDown)
        {
            if (transform.position.y >= startingPosition.y)
            {
                transform.position += new Vector3(0, -moveBackSpeed * Time.deltaTime, 0);
            }
            else
            {
                isMovingDown = false;
            }
        }
    }

    public void TurnOnSpikes()
    {
        isOn = true;
    }

    public void ResetSpikes()
    {
        isOn = false;
        transform.position = startingPosition;
    }

    public void MoveSpikesDown()
    {
        isMovingDown = true;
    }
}
