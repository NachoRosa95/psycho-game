﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class Test1
    {
        // A Test behaves as an ordinary method
        //[Test]
        //public void Test1SimplePasses()
        //{
        //    // Use the Assert class to test conditions
        //}

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator Test1WithEnumeratorPasses()
        {
            PsychologicAttribute psychologicAttribute = new PsychologicAttribute();

            psychologicAttribute.Inject(1);

            yield return null;

            Assert.AreEqual(1, psychologicAttribute.Weight);
        }
    }
}
