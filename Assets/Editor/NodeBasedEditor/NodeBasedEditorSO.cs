﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NodeBasedEditorSO : ScriptableObject
{
    public List<EditorNode> Nodes = new List<EditorNode>();
    public System.Type ContentType;
}