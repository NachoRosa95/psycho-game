using System;
using UnityEditor;
using UnityEngine;

public class Connection
{
    public ConnectionPoint inPoint;
    public ConnectionPoint outPoint;
    public Action<Connection> OnClickRemoveConnection;

    public Connection() { }

    public Connection(ConnectionPoint inPoint, ConnectionPoint outPoint, Action<Connection> OnClickRemoveConnection)
    {
        this.inPoint = inPoint;
        this.outPoint = outPoint;
        this.OnClickRemoveConnection = OnClickRemoveConnection;
    }

    public void Draw()
    {
        if (inPoint == null || outPoint == null)
        {
            OnClickRemoveConnection.Invoke(this);
            return;
        }

        if (!inPoint.Connections.Contains(this) || !outPoint.Connections.Contains(this))
        {
            OnClickRemoveConnection.Invoke(this);
            return;
        }

        Handles.DrawBezier(
            inPoint.Rect.center,
            outPoint.Rect.center,
            inPoint.Rect.center + Vector2.left * 50f,
            outPoint.Rect.center - Vector2.left * 50f,
            new Color(1, 1, 1, 0.5f),
            null,
            2f
        );

        if (Handles.Button((inPoint.Rect.center + outPoint.Rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
        {
            var genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove Connection"), false, () => OnClickRemoveConnection?.Invoke(this));
            genericMenu.ShowAsContext();
        }
    }
}