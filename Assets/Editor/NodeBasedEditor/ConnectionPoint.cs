using System;
using System.Collections.Generic;
using UnityEngine;

// Inputs can have multiple connections, Outputs only one
public enum ConnectionPointType { In, Out }


public class ConnectionPoint
{
    // public string id;

    public List<Connection> Connections = new List<Connection>();

    public Rect Rect;

    public ConnectionPointType Type;

    public EditorNode Node;

    public object Source;

    public GUIStyle Style;

    public Action<ConnectionPoint> OnClickConnectionPoint;

    public ConnectionPoint() { }

    public ConnectionPoint(EditorNode node, ConnectionPointType type, GUIStyle style, Action<ConnectionPoint> OnClickConnectionPoint, string id = null)
    {
        this.Node = node;
        this.Type = type;
        this.Style = style;
        this.OnClickConnectionPoint = OnClickConnectionPoint;
        Rect = new Rect(0, 0, 10f, 20f);

        // this.id = id ?? Guid.NewGuid().ToString();
    }

    void Destroy()
    {

    }

    public void Draw()
    {
        if (Type == ConnectionPointType.In)
            Rect.y = Node.Rect.y + (Node.Rect.height * 0.5f) - Rect.height * 0.5f;
        if (Type == ConnectionPointType.Out)
            Rect.y = Node.Rect.y + Node.Rect.height + (Node.outPoints.IndexOf(this)) * (Node.Rect.height - Node.ContentBorder);

        switch (Type)
        {
            case ConnectionPointType.In:
                Rect.x = Node.Rect.x - Rect.width + 8f;
                break;

            case ConnectionPointType.Out:
                Rect.x = Node.Rect.x + Node.Rect.width - 8f;
                break;
        }

        if (GUI.Button(Rect, "", Style))
        {
            if (OnClickConnectionPoint != null)
            {
                OnClickConnectionPoint(this);
            }
        }
    }
}