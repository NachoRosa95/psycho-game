using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.IO;
using LanguageSystem;

public class NodeBasedEditor : EditorWindow
{
    private static System.Type contentType;
    private static LanguageType selectedLanguage;

    private GUIStyle nodeStyle;
    private GUIStyle selectedNodeStyle;
    private GUIStyle inPointStyle;
    private GUIStyle outPointStyle;
    private GUIStyle selectionRectStyle;

    private ConnectionPoint selectedInPoint;
    private ConnectionPoint selectedOutPoint;

    private Vector2 offset;
    private Vector2 unsnappedDrag;
    private Vector2 drag;

    private float menuBarHeight = 20f;
    private Rect menuBar;

    private int instanceIndex;
    private string fileName;
    private static NodeBasedEditorSO editorSO;
    private static NodeBasedEditor window;

    private Vector2 nodeDimensions = new Vector2(200, 140);

    private const float snapToPixel = 20;
    private const string filePath = "Assets/Resources/NodeBasedEditor/";

    // Scale feature (WIP)
    private Vector2 scale = new Vector2(1, 1);
    private Vector2 pivotPoint;

    private bool saveOnLanguageSwitch = true;

    private bool rectSelecting = false;
    private Vector2 rectSelectOrigin = Vector2.zero;
    private Rect rectSelection = new Rect();
    private Color selectionColor = new Color(0.4f, 0.5f, 1, 0.8f);

    [MenuItem("Window/Dialogue Editor")]
    private static void OpenDialogueWindow()
    {
        window = GetWindow<NodeBasedEditor>();
        window.titleContent = new GUIContent("Node Based Editor");
        contentType = typeof(DialogueSO);
        editorSO = null;
    }

    [MenuItem("Window/Attribute Editor")]
    private static void OpenWindow()
    {
        window = GetWindow<NodeBasedEditor>();
        window.titleContent = new GUIContent("Attribute Editor");
        contentType = typeof(PsychologicAttribute);
        editorSO = null;
    }

    private void OnEnable()
    {
        nodeStyle = new GUIStyle();
        nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node0.png") as Texture2D;
        nodeStyle.border = new RectOffset((int)snapToPixel, (int)snapToPixel, (int)snapToPixel, (int)snapToPixel);

        selectedNodeStyle = new GUIStyle();
        selectedNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node0 on.png") as Texture2D;
        selectedNodeStyle.border = new RectOffset(10, 10, 10, 10);

        inPointStyle = new GUIStyle();
        inPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn left.png") as Texture2D;
        inPointStyle.active.background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn left on.png") as Texture2D;
        inPointStyle.border = new RectOffset(4, 4, 12, 12);

        outPointStyle = new GUIStyle();
        outPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
        outPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
        outPointStyle.border = new RectOffset(4, 4, 12, 12);


        if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);

        instanceIndex = Resources.FindObjectsOfTypeAll<NodeBasedEditorSO>().Length;

        OnSelectionChange();

        if (editorSO == null) return;
        if (contentType != typeof(DialogueSO)) return;

        LanguageManager.Instance.RecreateLanguagesEntries();
        if (!selectedLanguage) selectedLanguage = LanguageManager.Instance.StartingLanguage;
        Load(editorSO);

        UpdateLanguage();
    }

    private void OnGUI()
    {

        // ProcessScale();

        DrawGrid(snapToPixel, 0.2f, Color.gray);
        DrawGrid(snapToPixel * 5, 0.4f, Color.gray);


        DrawNodes();
        DrawConnectionLine(Event.current);

        DrawMenuBar();

        // DrawRectSelect();

        ProcessNodeEvents(Event.current);
        ProcessEvents(Event.current);


        if (GUI.changed) { if (editorSO) EditorUtility.SetDirty(editorSO); Repaint(); }
    }

    void OnSelectionChange()
    {
        var editor = Selection.activeObject as NodeBasedEditorSO;
        if (editor) Load(editor);

        if (editorSO == null) return;

        EditorNode node = null;

        var content = Selection.activeObject as ScriptableObject;
        if (content)
        {
            node = GetNodeWithContent(content);
            SelectNode(node);
            FocusNode(node);
        }

        Repaint();
    }

    private void ProcessScale()
    {
        pivotPoint = new Vector2(Screen.width / 2, Screen.height / 2);
        GUIUtility.ScaleAroundPivot(scale, pivotPoint);
    }

    private void DrawMenuBar()
    {
        menuBar = new Rect(0, 0, position.width, menuBarHeight);
        var caret = 20;

        GUILayout.BeginArea(menuBar, EditorStyles.toolbar);
        GUILayout.BeginHorizontal();

        if (GUILayout.Button(new GUIContent("New Editor"), EditorStyles.toolbarButton, GUILayout.Width(100)))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Dialogue"), false, () => NewEditor<DialogueSO>());
            menu.AddItem(new GUIContent("PsychologicAttribute"), false, () => NewEditor<PsychologicAttribute>());
            var size = new Vector2(0, 16);
            menu.DropDown(new Rect(new Vector2(caret, menuBarHeight) - size, size));
            EditorGUIUtility.ExitGUI();
        }
        caret += 100;

        if (GUILayout.Button(new GUIContent("Clear Nodes"), EditorStyles.toolbarButton, GUILayout.Width(100)))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Confirm"), false, OnClickClearNodes);
            var size = new Vector2(0, 16);
            menu.DropDown(new Rect(new Vector2(caret, menuBarHeight) - size, size));
            EditorGUIUtility.ExitGUI();
        }
        caret += 100;

        if (GUILayout.Button(new GUIContent("Delete Subassets"), EditorStyles.toolbarButton, GUILayout.Width(100)))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Confirm"), false, DeleteSelectedSubassets);
            var size = new Vector2(0, 16);
            menu.DropDown(new Rect(new Vector2(caret, menuBarHeight) - size, size));
            EditorGUIUtility.ExitGUI();
        }
        caret += 100;

        // if (GUILayout.Button(new GUIContent("(DEBUG) Clear Content"), EditorStyles.toolbarButton, GUILayout.Width(200)))
        // { DeleteAllContent(); AssetDatabase.SaveAssets(); }

        // if (GUILayout.Button(new GUIContent("Delete Editor"), EditorStyles.toolbarButton, GUILayout.Width(100)))
        // {
        //     DeleteEditor();
        // }

        if (GUILayout.Button(new GUIContent("Force Save"), EditorStyles.toolbarButton, GUILayout.Width(100)))
        {
            AssetDatabase.SaveAssets();
        }
        caret += 100;

        if (!selectedLanguage) selectedLanguage = LanguageManager.Instance.StartingLanguage;
        if (contentType == typeof(DialogueSO))
        {
            if (GUILayout.Button("Language: " + selectedLanguage.LanguageName, EditorStyles.toolbarDropDown, GUILayout.Width(150)))
            {
                GenericMenu menu = new GenericMenu();
                var langManager = LanguageManager.Instance;
                foreach (LanguageType language in langManager.LanguageTypes)
                {
                    menu.AddItem(new GUIContent(language.LanguageName), false, SelectLanguage, language);
                }
                var size = new Vector2(0, 16);
                menu.DropDown(new Rect(new Vector2(caret, menuBarHeight) - size, size));
                EditorGUIUtility.ExitGUI();
            }
            caret += 150;

            saveOnLanguageSwitch = GUILayout.Toggle(saveOnLanguageSwitch, new GUIContent(" Save on Language Switch"), GUILayout.Width(180));

            if (GUILayout.Button(new GUIContent("Save Keys"), EditorStyles.toolbarButton, GUILayout.Width(100)))
            { SaveKeys(); }
            caret += 100;

        }

        GUILayout.Space(5);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    void SaveKeys()
    {
        if (editorSO == null) return;
        foreach (EditorNode node in editorSO.Nodes)
        {
            node.SaveKeys();
        }
        LanguageManager.Instance.SaveToFile();
    }

    void SelectLanguage(object obj)
    {
        if (saveOnLanguageSwitch)
        {
            SaveKeys();
        }
        LanguageType lang = (LanguageType)obj;
        if (!lang) return;
        selectedLanguage = lang;
        UpdateLanguage();
    }

    void UpdateLanguage()
    {
        Debug.Log("Updating languages...");
        if (editorSO == null) return;
        foreach (EditorNode node in editorSO.Nodes)
        {
            node.UpdateKeys(selectedLanguage);
        }
        Debug.Log("Success");
    }

    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
    {
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        offset += drag * 0.5f;
        Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        for (int i = 0; i < widthDivs; i++)
        {
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
        }

        for (int j = 0; j < heightDivs; j++)
        {
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    private void DrawRectSelect()
    {
        if (!rectSelecting) return;
        rectSelection.size = Event.current.mousePosition - rectSelectOrigin;

        Color oldColor = GUI.color;
        GUI.color = selectionColor;
        GUI.Box(rectSelection, "");
        GUI.color = oldColor;
        GUI.changed = true;
    }

    private void DrawNodes()
    {
        if (editorSO == null || editorSO.Nodes == null) return;
        var connectionsToDraw = new List<Connection>();
        foreach (EditorNode node in editorSO.Nodes)
        {
            if (node == null) { Debug.LogError("Null node"); continue; }
            node.Draw();
            if (node.inPoint != null)
                foreach (Connection connection in node.inPoint.Connections)
                {
                    if (!connectionsToDraw.Contains(connection)) connectionsToDraw.Add(connection);
                }
        }

        foreach (Connection connection in connectionsToDraw)
        {
            connection.Draw();
        }
    }

    private void DrawConnectionLine(Event e)
    {
        if (selectedInPoint != null && selectedOutPoint == null)
        {
            Handles.DrawBezier(
                selectedInPoint.Rect.center,
                e.mousePosition,
                selectedInPoint.Rect.center + Vector2.left * 50f,
                e.mousePosition - Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        if (selectedOutPoint != null && selectedInPoint == null)
        {
            Handles.DrawBezier(
                selectedOutPoint.Rect.center,
                e.mousePosition,
                selectedOutPoint.Rect.center - Vector2.left * 50f,
                e.mousePosition + Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }
    }

    private void ProcessEvents(Event e)
    {
        drag = Vector2.zero;

        switch (e.type)
        {
            case EventType.ContextClick:
                ClearConnectionSelection();
                ProcessContextMenu(e.mousePosition);
                break;

            case EventType.MouseDrag:
                if (e.button == 2)
                {
                    OnDrag(e.delta);
                }
                break;
            case EventType.ScrollWheel:
                OnScroll(e.delta);
                break;

            case EventType.MouseDown:
                if (e.button == 0)
                {
                    // rectSelecting = true;
                    // rectSelectOrigin = e.mousePosition;
                    // rectSelection = new Rect(e.mousePosition, Vector2.zero);
                    ProcessSelection(e.mousePosition);
                }
                break;
            case EventType.MouseUp:
                if (e.button == 0)
                {
                    rectSelecting = false;
                }
                break;
        }
    }

    private void ProcessNodeEvents(Event e)
    {
        if (editorSO == null) return;
        if (editorSO.Nodes != null)
        {
            for (int i = editorSO.Nodes.Count - 1; i >= 0; i--)
            {
                bool guiChanged = editorSO.Nodes[i].ProcessEvents(e);

                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }
    }

    private void ProcessContextMenu(Vector2 mousePosition)
    {
        var genericMenu = new GenericMenu();
        if (!editorSO)
        {
            genericMenu.AddItem(new GUIContent("New Dialogue Editor"), false, () => NewEditor<DialogueSO>());
            genericMenu.AddItem(new GUIContent("New Attribute Editor"), false, () => NewEditor<PsychologicAttribute>());
        }
        else if (editorSO.ContentType == typeof(DialogueSO))
        {
            genericMenu.AddItem(new GUIContent("Add Dialogue"), false, () => AddNode<DialogueSO>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add Root"), false, () => AddNode<DialogueSO>(mousePosition, true));
        }
        else if (editorSO.ContentType == typeof(PsychologicAttribute))
        {
            genericMenu.AddItem(new GUIContent("Add Attribute"), false, () => AddNode<PsychologicAttribute>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add Sum"), false, () => AddNode<SumAttribute>(mousePosition));
        }
        genericMenu.ShowAsContext();
    }

    private void ProcessSelection(Vector2 mousePosition)
    {
        if (editorSO == null) return;
        EditorNode selected = null;
        foreach (int i in Enumerable.Range(1, editorSO.Nodes.Count))
        {
            var node = editorSO.Nodes[editorSO.Nodes.Count - i];
            if (node.Rect.Contains(mousePosition)) { selected = node; break; }
        }
        foreach (EditorNode node in editorSO.Nodes)
        {
            node.Select(node == selected);
        }
    }

    private EditorNode AddNode<T>(Vector2 position, bool root = false) where T : ScriptableObject
    {
        if (editorSO == null)
        {
            Debug.LogError("Attempted to add node to null editor");
            NewEditor<T>();
        }
        // Undo.RecordObject(editorSO, "Add Node");
        var content = CreateContent<T>(root ? "Root" : "");
        var newNode = CreateNode(position, content);
        editorSO.Nodes.Add(newNode);
        AssetDatabase.SaveAssets();
        return newNode;
    }

    private EditorNode CreateNode(Vector2 mousePosition, ScriptableObject content)
    {
        EditorNode newNode = new EditorNode(mousePosition, nodeDimensions.x, nodeDimensions.y, nodeStyle, selectedNodeStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint,
        OnClickRemoveNode, snapToPixel, content);
        GUI.changed = true;
        SetKey(newNode);
        newNode.UpdateKeys(selectedLanguage);
        newNode.SaveKeys();
        return newNode;
    }

    private ScriptableObject CreateContent<T>(string name = "")
    {
        var content = ScriptableObject.CreateInstance(typeof(T));
        if (name == "")
        {
            var typeName = typeof(T).ToString();
            var i = editorSO.Nodes.Count;
            name = typeName.Substring(0, Mathf.Min(typeName.Length, 4));
            var editorPath = AssetDatabase.GetAssetPath(editorSO);
            while (AssetDatabase.LoadAssetAtPath(filePath + editorSO.name + "/" + name + i + ".asset", typeof(ScriptableObject)) != null)
            {
                i++;
            }
            content.name = name + string.Format("{0:D3}", i);
        }
        else content.name = name;
        Debug.Log(name);
        AssetDatabase.AddObjectToAsset(content, editorSO);
        AssetDatabase.SaveAssets();
        return content;
    }

    private void OnClickClearNodes()
    {
        if (editorSO == null) return;
        while (editorSO.Nodes.Count > 0)
        {
            RemoveNode(editorSO.Nodes[0]);
        }
        editorSO.Nodes.Clear();
    }

    private void OnClickRemoveNode(EditorNode node)
    {
        RemoveNode(node);
        AssetDatabase.SaveAssets();
    }

    private void RemoveNode(EditorNode node)
    {
        node.Destroy();
        RemoveSubasset(node.Content);
        editorSO.Nodes.Remove(node);
        GUI.changed = true;
    }

    private void DeleteSelectedSubassets()
    {
        Selection.objects.All(obj => obj is DialogueSO && RemoveSubasset(obj));
        Selection.objects.All(obj => obj is PsychologicAttribute && RemoveSubasset(obj));
        AssetDatabase.SaveAssets();
    }

    private bool RemoveSubasset(UnityEngine.Object obj)
    {
        if (obj == null) return false;
        DestroyImmediate(obj, true);
        return true;
    }

    private void OnClickInPoint(ConnectionPoint inPoint)
    {
        selectedInPoint = inPoint;

        if (selectedOutPoint != null)
        {
            if (selectedOutPoint.Node != selectedInPoint.Node)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickOutPoint(ConnectionPoint outPoint)
    {
        selectedOutPoint = outPoint;

        if (selectedInPoint != null)
        {
            if (selectedOutPoint.Node != selectedInPoint.Node)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickRemoveConnection(Connection connection)
    {
        RemoveConnection(connection);
    }

    public void RemoveConnection(Connection connection)
    {
        if (connection == null) return;

        if (connection.inPoint != null) connection.inPoint.Connections.Remove(connection);
        if (connection.outPoint != null)
        {
            var dialogueOption = connection.outPoint.Source as DialogueOption;
            if (dialogueOption != null)
            {
                dialogueOption.TargetDialogue = null;
                connection.outPoint.Connections.Remove(connection);
            }

            var relation = connection.outPoint.Source as PsychologicRelation;
            if (relation != null)
            {
                relation.Target = null;
                connection.outPoint.Connections.Remove(connection);
            }
        }
    }

    private void ResetConnection()
    {
        foreach (Connection connection in selectedOutPoint.Connections) connection.inPoint.Connections.Remove(connection);
        selectedOutPoint.Connections.Clear();
        var newConnection = new Connection(selectedInPoint, selectedOutPoint, OnClickRemoveConnection);
        selectedInPoint.Connections.Add(newConnection);
        selectedOutPoint.Connections.Add(newConnection);
    }

    private void CreateConnection()
    {
        ResetConnection();

        var option = selectedOutPoint.Source as DialogueOption;
        if (option != null)
        {
            var target = selectedInPoint.Node.Content as DialogueSO;
            option.TargetDialogue = target;
        }

        var relation = selectedOutPoint.Source as PsychologicRelation;
        if (relation != null)
        {
            var target = selectedInPoint.Node.Content as PsychologicAttribute;
            relation.Target = target;
        }

        AssetDatabase.SaveAssets();
    }

    private void ClearConnectionSelection()
    {
        selectedInPoint = null;
        selectedOutPoint = null;
    }

    private void OnDrag(Vector2 delta)
    {
        unsnappedDrag += delta;
        if (Mathf.Abs(unsnappedDrag.x) > snapToPixel)
        {
            var remainder = unsnappedDrag.x % snapToPixel;
            drag.x = unsnappedDrag.x - remainder;
            unsnappedDrag.x %= snapToPixel;
        }
        if (Mathf.Abs(unsnappedDrag.y) > snapToPixel)
        {
            var remainder = unsnappedDrag.y % snapToPixel;
            drag.y = unsnappedDrag.y - remainder;
            unsnappedDrag.y %= snapToPixel;
        }

        GUI.changed = true;

        if (editorSO == null) return;

        if (editorSO.Nodes != null)
        {
            for (int i = 0; i < editorSO.Nodes.Count; i++)
            {
                editorSO.Nodes[i].Drag(drag);
            }
        }
    }

    private void OnScroll(Vector2 delta)
    {
        var amount = 0.5f;
        var offset = delta.y < 0 ? amount : -amount;
        scale += new Vector2(offset, offset);
        if (scale.x < 1) scale = new Vector2(1, 1);
        GUI.changed = true;
    }

    private void NewEditor<T>()
    {
        contentType = typeof(T);
        ClearConnectionSelection();
        editorSO = ScriptableObject.CreateInstance<NodeBasedEditorSO>();

        if (contentType != null) editorSO.ContentType = contentType;
        // fileName = contentType.ToString() + "Editor" + (instanceIndex + 1) + ".asset";
        var i = 0;
        var editorName = "Editor";
        while (AssetDatabase.LoadAssetAtPath(filePath + editorName + i + ".asset", typeof(ScriptableObject)) != null)
        {
            i++;
        }
        AssetDatabase.CreateAsset(editorSO, filePath + editorName + i + ".asset");
        editorSO.name = editorName;
        instanceIndex++;
        if (contentType == typeof(DialogueSO)) AddNode<DialogueSO>(new Vector2(nodeDimensions.x, nodeDimensions.y), true);
        Selection.SetActiveObjectWithContext(editorSO, editorSO);
    }

    private void DeleteEditor()
    {
        if (editorSO == null) return;
        var path = filePath + fileName;
        AssetDatabase.DeleteAsset(path);
        AssetDatabase.SaveAssets();
        editorSO = null;
    }

    void Load(NodeBasedEditorSO editor)
    {
        if (!selectedLanguage) selectedLanguage = LanguageManager.Instance.StartingLanguage;

        // Assign editor
        editorSO = editor;
        fileName = editor.name + ".asset";
        selectedInPoint = null;
        selectedOutPoint = null;
        if (editorSO.ContentType == null) editorSO.ContentType = GetEditorType();
        contentType = editorSO.ContentType;
        if (contentType == typeof(DialogueSO))
        {
            var manager = LanguageManager.Instance;
            manager.RecreateLanguagesEntries();
        }
        var text = contentType == typeof(DialogueSO) ? "Dialogue Editor" : "Attribute Editor";
        if (window) window.titleContent = new GUIContent(text);


        var path = AssetDatabase.GetAssetPath(editor);
        var assets = AssetDatabase.LoadAllAssetsAtPath(path);
        var contents = assets.Where(asset => asset is DialogueSO || asset is PsychologicAttribute);

        // var log = path + "\n";
        // foreach (object element in contents)
        // {
        //     log += "[" + element + "] ";
        // }
        // Debug.Log(log);

        // Initialize nodes
        if (editorSO.Nodes.Count > 0)
            foreach (int i in Enumerable.Range(0, editorSO.Nodes.Count))
            {
                var node = editorSO.Nodes[i];
                if (node.Content == null) node.Content = contents.ElementAt(i) as ScriptableObject;
                node.Init(node.Position, nodeDimensions.x, nodeDimensions.y, nodeStyle, selectedNodeStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint,
            OnClickRemoveNode, snapToPixel);

                var att = node.Content as PsychologicAttribute;

                var dialogue = node.Content as DialogueSO;
                if (dialogue)
                {
                    SetKey(node);
                    node.UpdateKeys(selectedLanguage);
                    node.SaveKeys();
                }
            }

        // Create connections
        foreach (EditorNode node in editorSO.Nodes)
        {
            if (node.outPoints != null)
                foreach (ConnectionPoint outPoint in node.outPoints)
                {
                    var option = outPoint.Source as DialogueOption;
                    ScriptableObject content = null;

                    if (option != null)
                    {
                        content = option.TargetDialogue;
                    }

                    var relation = outPoint.Source as PsychologicRelation;
                    if (relation != null)
                    {
                        content = relation.Target;
                    }

                    var targetNode = GetNodeWithContent(content);
                    if (targetNode == null) continue;
                    var inPoint = targetNode.inPoint;
                    OnClickOutPoint(outPoint);
                    OnClickInPoint(inPoint);
                }
        }

        UpdateLanguage();
        Repaint();
        AssetDatabase.SaveAssets();
    }

    public static EditorNode GetNodeWithContent(ScriptableObject content)
    {
        if (content == null || editorSO.Nodes == null) return null;
        return editorSO.Nodes.FirstOrDefault(node => node.Content == content);
    }

    private void SelectNode(EditorNode node)
    {
        if (node == null) return;
        foreach (EditorNode n in editorSO.Nodes)
        {
            n.Select(n == node);
        }
    }

    private void FocusNode(EditorNode node)
    {
        if (node == null) return;
        var center = new Vector2(this.position.width * 0.3f, this.position.height * 0.3f);
        var offset = center - node.Position;
        OnDrag(offset);
    }

    public void DeleteAllContent()
    {
        if (editorSO == null) return;
        foreach (EditorNode node in editorSO.Nodes)
        {
            node.Content = null;
        }
    }

    private System.Type GetEditorType()
    {
        if (editorSO.Nodes.Count == 0) return null;
        var content = editorSO.Nodes[0].Content;
        return !content ? null : content.GetType();
    }

    private string SetKey(EditorNode node)
    {
        var dialogue = node.Content as DialogueSO;
        if (dialogue)
        {
            var key = AssetDatabase.GetAssetPath(dialogue) + "/" + dialogue.name;
            dialogue.Key = key;
            return key;
        }
        return "";
    }
}