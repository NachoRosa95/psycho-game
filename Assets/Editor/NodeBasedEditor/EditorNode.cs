using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using LanguageSystem;

[System.Serializable]
public class EditorNode
{
    public ScriptableObject Content;
    public Vector2 Position;

    [NonSerialized] public Rect Rect;
    [NonSerialized] public ConnectionPoint inPoint;
    [NonSerialized] public List<ConnectionPoint> outPoints = new List<ConnectionPoint>();
    [NonSerialized] public float ContentBorder;

    public LanguageType Language
    {
        get { return language; }
        set
        {
            if (value != language) { /*On language changed*/ }
            language = value;
        }
    }

    private GUIStyle style;
    private GUIStyle defaultNodeStyle;
    private GUIStyle selectedNodeStyle;
    private GUIStyle outPointStyle;
    private GUIStyle inPointStyle;

    private LanguageType language;

    private Action<EditorNode> OnRemoveNode;
    private Action<ConnectionPoint> onClickOutPoint;
    private Action<ConnectionPoint> onClickInPoint;

    private Rect curRect;
    private Vector2 dragOffset;
    private bool isDragged;
    private bool isSelected;
    private float snapToPixel;

    public EditorNode(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle,
        Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<EditorNode> OnClickRemoveNode, float snapToPixel, ScriptableObject content)
    {
        Content = content;
        Init(position, width, height, nodeStyle, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode, snapToPixel);
    }

    public void Init(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle,
        Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<EditorNode> OnClickRemoveNode, float snapToPixel)
    {
        this.snapToPixel = snapToPixel;
        Rect = new Rect(position.x, position.y, width, height);
        style = nodeStyle;
        this.onClickOutPoint = OnClickOutPoint;
        this.onClickInPoint = OnClickInPoint;
        this.outPointStyle = outPointStyle;
        this.inPointStyle = inPointStyle;
        defaultNodeStyle = nodeStyle;
        selectedNodeStyle = selectedStyle;
        OnRemoveNode = OnClickRemoveNode;
        ContentBorder = 10f;
        SetPosition(position);
        Init();
    }

    private void Init()
    {
        GeneratePoints();
    }

    public void GeneratePoints()
    {
        inPoint = CreateInPoint();
        outPoints = new List<ConnectionPoint>();
        if (!Content) return;

        var dialogue = Content as DialogueSO;
        if (dialogue)
            foreach (DialogueOption option in dialogue.OptionAnswers)
            {
                var newOutPoint = CreateOutPoint(option);
                // Debug.Log(newOutPoint);
                outPoints.Add(newOutPoint);
            }

        var att = Content as PsychologicAttribute;
        if (att)
            foreach (PsychologicRelation relation in att.Relations)
            {
                var newOutPoint = CreateOutPoint(relation);
                // Debug.Log(newOutPoint);
                outPoints.Add(newOutPoint);
            }
    }

    public bool SaveKeys()
    {
        var dialogue = Content as DialogueSO;
        if (!dialogue) return false;
        if (dialogue.DialogueText.Contains("[LanguageError]")) return false;
        Debug.Log(language + " - Saving key " + dialogue.Key + " with value: " + dialogue.DialogueText);
        var _man = LanguageManager.Instance;
        _man.SetText(dialogue.Key, dialogue.DialogueText, language, true);
        var i = 0;
        foreach (DialogueOption option in dialogue.OptionAnswers)
        {
            Debug.Log(option);
            if (option.Key.Length < 5) option.Key = GenerateKey(option);
            _man.SetText(option.Key, option.OptionText, language, true);
            i++;
        }

        return true;
    }

    public void UpdateKeys(LanguageType language)
    {
        var dialogue = Content as DialogueSO;
        if (!dialogue) return;
        Language = language;
        var text = LanguageManager.Instance.GetText(dialogue.Key, language);
        Debug.Log(language + " - Updating key " + dialogue.Key + " with value: " + text);
        dialogue.DialogueText = text;
        var i = 0;
        foreach (DialogueOption option in dialogue.OptionAnswers)
        {
            if (option.Key == "") option.Key = GenerateKey(option);
            option.OptionText = LanguageManager.Instance.GetText(dialogue.Key + "_" + i, language);
            i++;
        }
    }

    private string GenerateKey(DialogueOption option)
    {
        var dialogue = Content as DialogueSO;
        if (!dialogue) return "";
        var index = dialogue.OptionAnswers.IndexOf(option);
        if (index == -1) return "";
        return dialogue.Key + "_" + index;
    }

    public void Drag(Vector2 delta)
    {
        Rect.position += delta;
        Position = Rect.position;
    }

    public void Draw()
    {
        var wordWrap = EditorStyles.textField.wordWrap;
        EditorStyles.textField.wordWrap = true;
        curRect = Rect;

        var defaultStyle = new GUIStyle();
        defaultStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node0 on.png") as Texture2D;
        defaultStyle.border = new RectOffset(10, 10, 10, 10);
        GUI.Box(Rect, "", style ?? defaultStyle);

        if (inPoint != null) inPoint.Draw();

        curRect.yMin += ContentBorder;
        curRect.xMin += ContentBorder;
        curRect.xMax -= ContentBorder;
        GUILayout.BeginArea(curRect);

        EditorGUI.BeginDisabledGroup(true);
        Content = (ScriptableObject)EditorGUILayout.ObjectField(Content, typeof(ScriptableObject), false);
        EditorGUI.EndDisabledGroup();

        var dialogue = Content as DialogueSO;
        var att = Content as PsychologicAttribute;

        if (dialogue)
        {
            DrawNode(dialogue);
            GUILayout.EndArea();
            foreach (DialogueOption option in dialogue.OptionAnswers)
            {
                DrawSubnode(option);
            }
        }
        else if (att)
        {
            DrawNode(att);
            GUILayout.EndArea();
            foreach (PsychologicRelation relation in att.Relations)
            {
                DrawSubnode(relation);
            }
        }
        else
        {
            GUILayout.EndArea();
        }

        if (outPoints != null) foreach (ConnectionPoint outPoint in outPoints) if (outPoint != null) outPoint.Draw();

        EditorStyles.textField.wordWrap = wordWrap;
    }

    private void DrawNode(DialogueSO dialogue)
    {
        if (!dialogue) return;

        if (inPoint != null) inPoint.Draw();

        if (!Content) { return; }

        // Undo.RecordObject(Dialogue, "Edit Dialogue");

        GUILayout.BeginHorizontal();
        GUILayout.Label("Key", GUILayout.Width(40));
        EditorGUI.BeginDisabledGroup(true);
        dialogue.Key = EditorGUILayout.TextField(dialogue.Key);
        EditorGUI.EndDisabledGroup();
        GUILayout.EndHorizontal();

        EditorGUILayout.LabelField("Dialogue");
        dialogue.DialogueText = EditorGUILayout.TextArea(dialogue.DialogueText, GUILayout.MinHeight(40f));
        GUILayout.BeginHorizontal();
        dialogue.AudioClip = (AudioClip)EditorGUILayout.ObjectField(dialogue.AudioClip, typeof(AudioClip), false);
        GUILayout.FlexibleSpace();
        if (GUILayout.Button(" + ")) { AddSubnode(dialogue); }
        if (GUILayout.Button(" - ")) { RemoveSubnode(); }
        GUILayout.EndHorizontal();
        curRect = Rect;
        curRect.y += Rect.height - ContentBorder;

    }

    private void DrawNode(PsychologicAttribute att)
    {
        if (!att) return;

        if (!Content) { GUILayout.EndArea(); return; }

        var labelWidth = 60;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Name", GUILayout.Width(labelWidth));
        att.name = EditorGUILayout.TextArea(att.name);
        GUILayout.EndHorizontal();

        att.AttributeName = att.name;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Weight", GUILayout.Width(labelWidth));
        att.Weight = EditorGUILayout.FloatField(att.Weight);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("ID", GUILayout.Width(labelWidth));
        att.ID = (AttributeID)EditorGUILayout.EnumPopup(att.ID);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button(" + ")) { AddSubnode(att); }
        if (GUILayout.Button(" - ")) { RemoveSubnode(); }
        GUILayout.EndHorizontal();

        curRect = Rect;
        curRect.y += Rect.height - ContentBorder;

    }

    private void DrawSubnode(DialogueOption option)
    {
        curRect.height = Rect.height;
        GUI.Box(curRect, "", style ?? default(GUIStyle));
        curRect.yMin += ContentBorder;
        curRect.xMin += ContentBorder;
        curRect.xMax -= ContentBorder;

        GUILayout.BeginArea(curRect);
        GUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.LabelField("Target", GUILayout.MaxWidth(40));
        EditorGUILayout.ObjectField(option.TargetDialogue, typeof(DialogueSO), false);
        EditorGUI.EndDisabledGroup();
        GUILayout.EndHorizontal();

        EditorGUILayout.LabelField("Answer");
        option.OptionText = EditorGUILayout.TextArea(option.OptionText, GUILayout.MinHeight(40f));
        GUILayout.BeginHorizontal();

        var att = (PsychologicAttribute)EditorGUILayout.ObjectField(option.Attribute, typeof(PsychologicAttribute), false);
        if (att != option.Attribute) Debug.Log(option.Attribute + " => " + att);
        option.Attribute = att;

        var debugWt = option.Weight;
        option.Weight = EditorGUILayout.FloatField(option.Weight);
        if (debugWt != option.Weight) Debug.Log(option.Weight);

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Conditions: " + option.Conditions.Count);
        GUILayout.Label("Actions: " + option.Actions.Count);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();

        var curY = curRect.y;
        curRect = Rect;
        curRect.y = curY + Rect.height - 2 * ContentBorder;
    }

    private void DrawSubnode(PsychologicRelation relation)
    {
        var labelWidth = 60;

        curRect.height = Rect.height;
        GUI.Box(curRect, "", style ?? default(GUIStyle));
        curRect.yMin += ContentBorder;
        curRect.xMin += ContentBorder;
        curRect.xMax -= ContentBorder;

        GUILayout.BeginArea(curRect);

        GUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.LabelField("Target", GUILayout.Width(labelWidth));
        EditorGUILayout.ObjectField(relation.Target, typeof(PsychologicAttribute), false);
        EditorGUI.EndDisabledGroup();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Weight Multiplier", GUILayout.Width(labelWidth * 2));
        relation.WeightMultiplier = EditorGUILayout.FloatField(relation.WeightMultiplier);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Conditional", GUILayout.Width(labelWidth * 2));
        relation.Conditional = EditorGUILayout.Toggle(relation.Conditional);
        GUILayout.EndHorizontal();

        if (!relation.Conditional)
        {
            EditorGUI.BeginDisabledGroup(true);
        }

        GUILayout.BeginHorizontal();
        GUILayout.Label("Upper Bound", GUILayout.Width(labelWidth * 2));
        relation.Upper = EditorGUILayout.FloatField(relation.Upper);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Lower Bound", GUILayout.Width(labelWidth * 2));
        relation.Lower = EditorGUILayout.FloatField(relation.Lower);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Output", GUILayout.Width(labelWidth * 2));
        relation.Output = EditorGUILayout.FloatField(relation.Output);
        GUILayout.EndHorizontal();

        if (!relation.Conditional)
        {
            EditorGUI.EndDisabledGroup();
        }

        GUILayout.EndArea();

        var curY = curRect.y;
        curRect = Rect;
        curRect.y = curY + Rect.height - 2 * ContentBorder;
    }

    public bool ProcessEvents(Event e)
    {
        switch (e.type)
        {
            case EventType.KeyDown:
                if (e.keyCode == KeyCode.Delete && isSelected) OnClickRemoveNode();
                break;
            case EventType.MouseDown:
                if (e.button == 0)
                {
                    if (Rect.Contains(e.mousePosition))
                    {
                        dragOffset = Rect.position - e.mousePosition;
                        //     Selection.SetActiveObjectWithContext(Dialogue, Dialogue);
                        //     Select(true);
                        // }
                        // else
                        // {
                        //     Select(false);
                    }
                }

                if (e.button == 1 && isSelected && Rect.Contains(e.mousePosition))
                {
                    ProcessContextMenu();
                    e.Use();
                }
                break;

            case EventType.MouseUp:
                isDragged = false;
                break;

            case EventType.MouseDrag:
                if (e.button == 0 && isDragged)
                {
                    SetPosition(e.mousePosition + dragOffset);
                    e.Use();
                    return true;
                }
                break;
        }

        return false;
    }

    public void Select(bool value)
    {
        isSelected = value;
        isDragged = value;
        style = value ? selectedNodeStyle : defaultNodeStyle;
        GUI.changed = true;
    }

    public void Destroy()
    {
        var dialogue = Content as DialogueSO;
        if (!dialogue) return;
        var _man = LanguageManager.Instance;
        _man.RemoveKey(dialogue.Key);
        var i = 0;
        foreach (DialogueOption option in dialogue.OptionAnswers)
        {
            _man.RemoveKey(option.Key);
            i++;
        }
    }

    private void ProcessContextMenu()
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);
        genericMenu.ShowAsContext();
    }

    private void OnClickRemoveNode()
    {
        if (OnRemoveNode != null)
        {
            OnRemoveNode(this);
        }
    }

    public void SetPosition(Vector2 position)
    {
        var snappedPosition = GetSnappedPosition(position);
        Rect.position = snappedPosition;
        // var center = new Vector2(position.x - Rect.width / 2, position.y - Rect.height / 2);
        Position = snappedPosition;
        GUI.changed = true;
    }

    private Vector2 GetSnappedPosition(Vector2 position)
    {
        var snappedX = Mathf.Round(position.x / snapToPixel) * snapToPixel;
        var snappedY = Mathf.Round(position.y / snapToPixel) * snapToPixel;
        return new Vector2(snappedX, snappedY);
    }

    private void AddSubnode(DialogueSO dialogue)
    {
        if (!dialogue) return;
        var option = new DialogueOption();
        dialogue.OptionAnswers.Add(option);
        option.Key = GenerateKey(option);
        var outPoint = CreateOutPoint(option);
        outPoints.Add(outPoint);
        SaveKeys();
    }

    // private void AddSubnode(PsychologicAttribute att)
    // {
    //     GenericMenu genericMenu = new GenericMenu();
    //     genericMenu.AddItem(new GUIContent("Relation"), false, () => AddSubnode(att, false));
    //     genericMenu.AddItem(new GUIContent("Conditional Relation"), false, () => AddSubnode(att, true));
    //     genericMenu.ShowAsContext();
    // }

    private void AddSubnode(PsychologicAttribute att)
    {
        if (!att) return;
        var relation = new PsychologicRelation();
        att.Relations.Add(relation);
        var outPoint = CreateOutPoint(relation);
        outPoints.Add(outPoint);
    }

    private void RemoveSubnode()
    {
        if (outPoints.Count == 0) return;
        var outPoint = outPoints[outPoints.Count - 1];

        var dialogue = Content as DialogueSO;
        if (dialogue)
            dialogue.OptionAnswers.Remove(outPoint.Source as DialogueOption);

        var att = Content as PsychologicAttribute;
        if (att)
            att.Relations.Remove(outPoint.Source as PsychologicRelation);

        outPoint.Connections.Clear();
        outPoints.Remove(outPoint);
    }

    private ConnectionPoint CreateOutPoint(object obj)
    {
        var outPoint = new ConnectionPoint(this, ConnectionPointType.Out, outPointStyle, onClickOutPoint);
        outPoint.Source = obj;
        return outPoint;
    }

    private ConnectionPoint CreateInPoint()
    {
        return new ConnectionPoint(this, ConnectionPointType.In, inPointStyle, onClickInPoint);
    }

    private string ToString<T>(IEnumerable<T> collection, string name = "collection")
    {
        var result = name + ": ";
        foreach (T item in collection) result += "[" + item + "] ";
        return result;
    }
}