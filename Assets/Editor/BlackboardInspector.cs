﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Blackboard))]
public class BlackboardInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Blackboard bb = target as Blackboard;

        foreach (var entry in bb.Entries)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(entry.Key, GUILayout.Width(100));


            if (entry.Value is MemoryEntry<int>)
            {
                MemoryEntry<int> intVal = (MemoryEntry<int>)entry.Value;
                EditorGUILayout.IntField(intVal.value);
            }

            if (entry.Value is MemoryEntry<float>)
            {
                MemoryEntry<float> intVal = (MemoryEntry<float>)entry.Value;
                EditorGUILayout.FloatField(intVal.value);
            }


            if (entry.Value is MemoryEntry<GameObject>)
            {
                MemoryEntry<GameObject> goValue = (MemoryEntry<GameObject>)entry.Value;
                EditorGUILayout.ObjectField(goValue.value, typeof(GameObject), true);
            }

            // Siempre da false ? ? ? ? ?
            //if (entry.Value is MemoryEntry<List<GameObject>>)
            //    Debug.Log(entry.Value is MemoryEntry<IEnumerable>);

            if (entry.Value is MemoryEntry<List<GameObject>>)
            {
                MemoryEntry<List<GameObject>> mem = entry.Value as MemoryEntry<List<GameObject>>;
                List<GameObject> items = mem.value as List<GameObject>;

                //GUILayout.Label(entry.Key, GUILayout.Width(100));
                //EditorGUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.EndHorizontal();

                EditorGUILayout.BeginVertical();
                foreach (Object item in items)
                {
                    EditorGUILayout.ObjectField(item, item.GetType(), true);
                }
                EditorGUILayout.EndVertical();
            }

            //if (entry.Value is MemoryEntry<List<Node>>)
            //{
            //    MemoryEntry<List<Node>> mem = entry.Value as MemoryEntry<List<Node>>;
            //    List<Node> items = mem.value as List<Node>;

            //    //GUILayout.Label(entry.Key, GUILayout.Width(100));
            //    //EditorGUILayout.EndHorizontal();

            //    GUILayout.BeginHorizontal();
            //    GUILayout.EndHorizontal();

            //    EditorGUILayout.BeginVertical();
            //    foreach (Object item in items)
            //    {
            //        EditorGUILayout.ObjectField(item, item.GetType());
            //    }
            //    EditorGUILayout.EndVertical();
            //}

            //if (entry.Value is MemoryEntry<List<Node>>)
            //{
            //    Debug.Log("Is list");
            //    MemoryEntry<List<Node>> mem = entry.Value as MemoryEntry<List<Node>>;
            //    List<Node> items = mem.value as List<Node>;

            //    EditorGUILayout.BeginFadeGroup(1);
            //    foreach(Object item in items)
            //    {
            //        GUILayout.EndHorizontal();
            //        GUILayout.BeginHorizontal();
            //        EditorGUILayout.ObjectField(item, item.GetType());
            //    }
            //    EditorGUILayout.EndFadeGroup();
            //}

            GUILayout.EndHorizontal();
        }

        this.Repaint();
    }
}