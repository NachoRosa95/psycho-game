﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaypointEditor), true)]
public class WaypointInspector : Editor
{
    WaypointEditor editor;

    public void OnSceneGUI()
    {
        if (EditorApplication.isPlaying) return;

        editor = target as WaypointEditor;
        var waypoints = editor.GetWaypoints();
        EditorGUI.BeginChangeCheck();
        var labelStyle = new GUIStyle();
        labelStyle.fontSize = 10;
        labelStyle.alignment = TextAnchor.MiddleCenter;
        labelStyle.fontStyle = FontStyle.Bold;
        for (var i = 0; i < waypoints.Count; i++)
        {
            Handles.color = Color.cyan; 
            waypoints[i] = Handles.FreeMoveHandle(editor.transform.position + waypoints[i], Quaternion.identity, 0.5f, Vector3.one * 10f, Handles.CubeHandleCap) - editor.transform.position;
            Handles.Label(editor.transform.position + waypoints[i] + Vector3.up * 0.25f, "" + i, labelStyle);
            // Handles.CapFunction func = (id, position, rotation, size, type) => Handles.CircleHandleCap(0, waypoints[i], Quaternion.identity, 0.1f, EventType.Repaint);
            // waypoints[i] = Handles.FreeMoveHandle(waypoints[i], Quaternion.identity, 0.1f, Vector3.one * 0.1f, func);
        }

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(editor, "Changed Waypoint");
            editor.SetWaypoints(waypoints);
        }
    }

    public override void OnInspectorGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            editor = target as WaypointEditor;
            var waypoints = editor.GetWaypoints();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+")) AddWaypoint(waypoints);
            if (GUILayout.Button("-")) RemoveWaypoint(waypoints);
            GUILayout.EndHorizontal();
            editor.SetWaypoints(waypoints);
        }

        DrawDefaultInspector();
    }

    void AddWaypoint(List<Vector3> waypoints)
    {
        Undo.RecordObject(editor, "Added Waypoint");
        var pos = waypoints.Count > 0 ? waypoints[waypoints.Count - 1] + (Vector3)Vector2.one : Vector3.zero;
        waypoints.Add(pos);
    }

    void RemoveWaypoint(List<Vector3> waypoints)
    {
        Undo.RecordObject(editor, "Removed Waypoint");
        if (waypoints.Count == 0) return;
        waypoints.RemoveAt(waypoints.Count - 1);
    }
}