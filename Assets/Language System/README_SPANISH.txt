**** LANGUAGE TEXT SYSTEM 1.0 ****

	Creado por Federico Pardo (Spardox)
	Buenos Aires, Argentina
	2019

********* INTRODUCCION *********

Este es un sistema de localizacion de texto que te permitirá poder implementar tu juego en diferentes idiomas. 
Cuenta con una escena de prueba, una ventana para editar las entradas de tus idiomas y mucho más.

El funcionamiento principal de este sistema se basa en diferentes entradas compuestas de una Key (palabra identificadora)
y un Value (el contenido de esa Key, que varía en cada idioma). 
Todos los lenguajes deben tener las mismas keys, y lo único que cambia son los values de las mismas.
Estas entradas se guardan en .json y se precargan desde el manager con el botón correspondiente.
Los json se pueden editar a mano desde cualquier editor de texto o usando la ventana "Language Text Editor".

Ejemplo archivo inglés:

{
	"items": 
	[
		{
		 "key":"game_title",
		 "value":"Language System"
		},
		{
		 "key":"credits",
		 "value":"Created by Federico Pardo, 2019."
		}
	]
}

Ejemplo archivo español:

{
	"items": 
	[
		{
		 "key":"game_title",
		 "value":"Sistema de lenguaje"
		},
		{
		 "key":"credits",
		 "value":"Creado por Federico Pardo, 2019"
		}
	]
}


********* INSTRUCCIONES *********

1) Importar el Asset a tu proyecto de Unity.

2) Poner el prefab "LanguageManager" en tu escena inicial. Este obje

3) Crear todos los .json de cada lenguaje, con sus Key y sus Values. 
Se puede crear más de un archivo para el mismo lenguaje para una mejor organización.
Por ejemplo un archivo EnglishMenu para guardar las entradas del menú y otro LevelMenu.

3.A) Se puede hacer desde cualquier editor de texto, respetando el formato del ejemplo. 
Se debe guardar como .json y agregar al proyecto.

3.B) Se puede hacer desde la ventana "Language Text Editor" que se accede desde la pestaña Window.
Dispone de guardar, cargar y crear nuevo. 

4) Los .json deben estar contenidos dentro de la misma carpeta. En el ejemplo se usa la carpeta "LanguagesFiles".
La ruta hacia la carpeta se puede cambiar desde el LanguageManager en el campo "FolderPath".

5) Crear todos los scriptable objects de cada lenguaje. Para hacerlo ir a Create-LanguageType.
Se debe completar el campo de nombre y agregar los nombres de los archivos que corresponden a ese lenguaje.

6) Agregar los lenguajes creados en el paso anterior a la lista del LanguageManager.
Seleccionar el lenguaje inicial del juego desde el campo "Starting Language Type".

7) [IMPORTANTE] Clickear el botón "Recreate Language Entries" para crear/actualizar todas las entradas de los lenguajes.

**********************************

USOS:

A) Text Language Component: agregar este componente a cualquier objeto que posee un Text.
En su campo "key" se debe especificar la key que mostrará ese campo de texto. 
El contenido se actualiza automáticamente cuando se cambia el lenguaje.

B) BtnLang: agregar este componente al botón que cambiará el lenguaje de su juego.
En su campo "type" se debe especificar el lenguaje al que cambiará.

C) BtnSelectLanguage: prefab de botón que cuando se selecciona se cambia del lenguaje actual al especificado en el componente.

D) [IMPORTANTE] Para actualizar el lenguaje de un texto que cambia en runtime se debe implementar la interfaz IUseLanguage.
Esta interfaz agrega una funcion de suscripción en donde deben suscribirse al evento OnLanguageChanged del LanguageManager.
La función de suscripcion se debe llamar internamente desde el Awake();
También agrega la función UpdateLanguage que sirve como Handler del evento. Allí sucederá la actualización del texto.
Para pedir un value al Manager se debe llamar a la función LanguageManager.Instance.GetText(key) y pasarle una Key como parámetro.
Esto devolverá el value de esa Key en el lenguaje actual.

**********************************

Cualquier pregunta contactarse con: fedelks94@gmail.com / fede-2212@hotmail.com

Test your JSON files here: https://jsonformatter.curiousconcept.com/



