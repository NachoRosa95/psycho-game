**** LANGUAGE TEXT SYSTEM 1.0 ****

    Created by Federico Pardo (Spardox)
    Buenos Aires, Argentina
    2019

********* INTRODUCTION *********

This is a text location system that will allow you to implement your game in different languages.
It has a test scene, a window to edit the entries of your languages and much more.

The main operation of this system is based on different inputs composed of a Key (identifier word)
and a Value (the content of that Key, which varies in each language).
All languages must have the same keys, and the only thing that changes are the values of them.
These entries are saved in .json and are preloaded from the manager with the corresponding button.
The json can be edited by hand from any text editor or using the "Language Text Editor" window.

Example english file:

{
	"items": 
	[
		{
		 "key":"game_title",
		 "value":"Language System"
		},
		{
		 "key":"credits",
		 "value":"Created by Federico Pardo, 2019."
		}
	]
}

Example spanish file:

{
	"items": 
	[
		{
		 "key":"game_title",
		 "value":"Sistema de lenguaje"
		},
		{
		 "key":"credits",
		 "value":"Creado por Federico Pardo, 2019"
		}
	]
}


********* INSTRUCTIONS *********

1) Import the Asset to your Unity project.

2) Put the prefab "LanguageManager" in your initial scene. This object will persist throughout the game.

3) Create all the .json of each language, with its Key and Values.
You can create more than one file for the same language for a better organization.
For example, an EnglishMenu file to save the menu entries and another LevelMenu.

3.A) It can be done from any text editor, respecting the format of the example.
It should be saved as .json and added to the project.

3.B) It can be done from the "Language Text Editor" window that is accessed from the Window tab.
It has save, load and create new.

4) The .json must be contained within the same folder. In the example, the folder "LanguagesFiles" is used.
The path to the folder can be changed from the LanguageManager in the "FolderPath" field.

5) Create all the scriptable objects of each language. To do so go to Create-LanguageType.
You must complete the name field and add the names of the files that correspond to that language.

6) Add the languages created in the previous step to the list of the LanguageManager.
Select the initial language of the game from the "Starting Language Type" field.

7) [IMPORTANT] Click the "Recreate Language Entries" button to create / update all the language entries.

*********************************

APPLICATIONS:

A) Text Language Component: add this component to any object that has a Text.
In your "key" field you must specify the key that that text field will display.
The content is automatically updated when the language is changed.

B) BtnLang: add this component to the button that will change the language of your game.
In your "type" field, you must specify the language to which you will change.

C) BtnSelectLanguage: button prefab that when selected is changed from the current language to the one specified in the component.

D) [IMPORTANT] To update the language of a text that changes in runtime, the IUseLanguage interface must be implemented.
This interface adds a subscription function where you must subscribe to the OnLanguageChanged event of the LanguageManager.
The subscription function must be called internally from the Awake ();
It also adds the UpdateLanguage function that serves as the Event Handler. There will happen the text update.
To request a value from the Manager, call the LanguageManager.Instance.GetText (key) function and pass it a Key as a parameter.
This will return the value of that Key in the current language.

*********************************

Any question contact: fedelks94@gmail.com / fede-2212@hotmail.com

Test your JSON files here: https://jsonformatter.curiousconcept.com/



