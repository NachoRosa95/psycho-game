using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LanguageSystem.Utilities;
using UnityEngine;
using UnityEngine.Events;

namespace LanguageSystem
{
    /// <summary>
    /// Main controller for language, must be created on the starting scene.
    /// </summary>
    [Serializable]
    public class LanguageManager : MonoBehaviour
    {
        #region Exposed fields

        private static LanguageManager _instance;

        [HideInInspector] [SerializeField] private string _folderPath = "/Language System/LanguagesFiles/";

        [HideInInspector] [SerializeField] private LanguageType _startingLanguage;

        [HideInInspector] [SerializeField] private List<LanguageType> _languageTypes;

        [SerializeField] private LanguageManagerData _managerData;

        #endregion Exposed fields

        #region Internal fields

        [HideInInspector]
        [SerializeField]
        private LanguagesDictionary _keysDictionary = new LanguagesDictionary();

        #endregion Internal fields

        #region Properties

        public static LanguageManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<LanguageManager>();

                    if (_instance == null)
                    {
                        var obj = new GameObject { name = typeof(LanguageManager).Name };
                        _instance = obj.AddComponent<LanguageManager>();
                    }
                }
                // _instance.StartingLanguage = _instance.ManagerData.StartingLanguage;
                // _instance.FolderPath = _instance.ManagerData.FolderPath;
                // _instance.LanguagesType = _instance.ManagerData.LanguageTypes;

                return _instance;
            }

            private set { _instance = value; }
        }

        public string FolderCompletePath
        {
            get { return Application.dataPath + FolderPath; }
        }

        public string FolderPath
        {
            get { return _folderPath; }
            set { _folderPath = value; }
        }

        public LanguageType Selected { get; private set; }

        public LanguageType StartingLanguage
        {
            get { return _managerData.StartingLanguage; }
            set { _managerData.StartingLanguage = value; }
        }

        public List<LanguageType> LanguageTypes
        {
            get { return _managerData.LanguageTypes; }
            set { _managerData.LanguageTypes = value; }
        }

        public LanguageManagerData ManagerData => _managerData;

        #endregion Properties

        #region Custom Events

        [HideInInspector] public UnityEvent OnLanguageChanged = new UnityEvent();

        #endregion Custom Events

        #region Events methods

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Debug.Log("[Singleton] Destroying duplicated gameobject: " + gameObject);
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);

            if (LanguageTypes.Count > 0)
            {
                Debug.Log("Languages loaded: " + LanguageTypes.Count);
            }
            else
            {
                Debug.LogError("Language collection empty!");
            }

            if (StartingLanguage != null)
            {
                ChangeLanguage(StartingLanguage);
            }
            else
            {
                Debug.LogError("Starting language not selected!");
            }
        }

        private void Start()
        {
            // if (LanguageTypes.Count > 0)
            // {
            //     Debug.Log("Languages loaded: " + LanguageTypes.Count);
            // }
            // else
            // {
            //     Debug.LogError("Language collection empty!");
            // }

            // if (StartingLanguage != null)
            // {
            //     ChangeLanguage(StartingLanguage);
            // }
            // else
            // {
            //     Debug.LogError("Starting language not selected!");
            // }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Changes the selected language for a new one.
        /// </summary>
        /// <param name="newLanguage"></param>
        public void ChangeLanguage(LanguageType newLanguage)
        {
            // Early return if is the same
            if (Selected != null && newLanguage.LanguageName == Selected.LanguageName) return;

            Selected = newLanguage;

            Debug.Log("[LanguageManager] New Language selected: " + Selected.LanguageName);

            OnLanguageChanged.Invoke();
        }

        /// <summary>
        /// Gets text from a key in the current language.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetText(string key, LanguageType lang = null)
        {
            if (lang == null) lang = Selected;
            if (_keysDictionary.ContainsKey(key)
                && _keysDictionary[key].ValuesDictionary.ContainsKey(lang.LanguageName))
            {
                return _keysDictionary[key].ValuesDictionary[lang.LanguageName];
            }

            return "[LanguageError] Key not found on current language. Key: " + key;
        }

        /// <summary>
        /// Sets the text for a key in a given language
        /// </summary>
        /// <returns></returns>
        public bool SetText(string key, string value, LanguageType lang = null, bool createIfNull = false)
        {
            if (_startingLanguage == null) return false;
            if (lang == null) lang = _startingLanguage;
            if (key == null) return false;
            var langName = lang.LanguageName;
            if (!_keysDictionary.ContainsKey(key))
            {
                if (createIfNull)
                {
                    var langValue = new LanguagesValue();
                    _keysDictionary.Add(key, langValue);
                }
                else return false;
            }
            if (!_keysDictionary[key].ValuesDictionary.ContainsKey(langName))
            {
                if (createIfNull)
                {
                    _keysDictionary[key].ValuesDictionary.Add(lang.LanguageName, value);
                }
                else return false;
            }

            _keysDictionary[key].ValuesDictionary[lang.LanguageName] = value;
            Debug.Log("[LanguageManager] Setting key <" + key + "> on language <" + lang.LanguageName + "> to value <" + value + ">");
            return true;
        }

        public bool RemoveKey(string key)
        {
            if (_startingLanguage == null) return false;
            if (key == null) return false;

            if (!_keysDictionary.ContainsKey(key))
            {
                return false;
            }

            _keysDictionary.Remove(key);
            Debug.Log("[LanguageManager] Removing key <" + key + "> on all languages");
            return true;
        }

        // public KeyValuePair<string, string>? GetEntry(string key, LanguageType lang = null)
        // {
        //     if (lang == null) lang = Selected;
        //     if (!_keysDictionary.ContainsKey(key)
        //     || !_keysDictionary[key].ValuesDictionary.ContainsKey(lang.LanguageName))
        //     {
        //         return null;
        //     }
        //     return _keysDictionary[key].ValuesDictionary[lang.LanguageName];
        // }

        /// <summary>
        /// Saves entries to file.
        /// </summary>
        /// <returns></returns>
        public bool SaveToFile()
        {
            // Horrible way to reverse the "Key > LanguageName > Value" structure to a more json-friendly "LanguageName > Key > Value" one

            var languages = new Dictionary<string, List<LanguageItem>>();
            foreach (var pair in _keysDictionary)
            {
                foreach (var language in pair.Value.ValuesDictionary)
                {
                    if (!languages.ContainsKey(language.Key))
                    {
                        var newList = new List<LanguageItem>();
                        languages.Add(language.Key, newList);
                        Debug.Log("[LanguageManager] Adding language to list: " + language.Key);
                    }
                }
            }

            foreach (var language in languages)
            {
                foreach (var pair in _keysDictionary)
                {
                    if (!pair.Value.ValuesDictionary.ContainsKey(language.Key)) continue;
                    var item = new LanguageItem();
                    var value = pair.Value.ValuesDictionary[language.Key];
                    item.key = pair.Key;
                    item.value = value;
                    language.Value.Add(item);
                }
            }

            var i = 0;
            foreach (var language in languages)
            {
                var data = new LanguageData();
                data.items = language.Value;

                var filePath = LanguageManager.Instance.FolderCompletePath + language.Key + ".json";

                if (!string.IsNullOrEmpty(filePath))
                {
                    var dataAsJson = JsonUtility.ToJson(data);
                    Debug.Log("[LanguageManager] Saving language: " + language.Key);
                    File.WriteAllText(filePath, dataAsJson);
                }
                i++;
            }
            return true;
        }

        /// <summary>
        /// Recreates the languages entries.
        /// </summary>
        /// <returns></returns>
        public bool RecreateLanguagesEntries()
        {
            if (LanguageTypes == null) return false;

            _keysDictionary.Clear();

            var noErrors = true;

            foreach (var languageType in LanguageTypes)
            {
                var languageName = languageType.LanguageName;

                foreach (var file in languageType.FilesNames)
                {
                    var filePath = Path.Combine(FolderCompletePath, file);

                    if (File.Exists(filePath))
                    {
                        var dataAsJson = File.ReadAllText(filePath);
                        var loadedData = JsonUtility.FromJson<LanguageData>(dataAsJson);

                        if (loadedData != null && loadedData.items != null)
                        {
                            foreach (var item in loadedData.items)
                            {
                                if (_keysDictionary.ContainsKey(item.key))
                                {
                                    if (_keysDictionary[item.key].ValuesDictionary
                                        .ContainsKey(languageName))
                                    {
                                        noErrors = false;
                                        Debug.LogError("[LanguageError] Entry already exists: " +
                                                       languageName + " / " + item.key + " / File: " + file);
                                    }
                                    else
                                    {
                                        _keysDictionary[item.key].ValuesDictionary
                                            .Add(languageName, item.value);
                                    }
                                }
                                else
                                {
                                    var languagesValue = new LanguagesValue();
                                    languagesValue.ValuesDictionary.Add(languageName, item.value);
                                    _keysDictionary.Add(item.key, languagesValue);
                                }
                            }

                            Debug.Log("[" + file + "] " + loadedData.items.Count + " entries loaded.");
                        }
                    }
                    else
                    {
                        noErrors = false;
                        Debug.LogError("[LanguageError] [" + languageName + "] File not found: " + file);
                    }
                }
            }

            if (!CheckKeysEntries())
            {
                noErrors = false;
            }

            return noErrors;
        }

        /// <summary>
        /// Gets multiple texts in the current language.
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<string> GetMultipleText(List<string> keys)
        {
            var multipleText = new List<string>();

            foreach (var key in keys)
            {
                multipleText.Add(GetText(key));
            }

            return multipleText;
        }

        /// <summary>
        /// Gets a unified text in the current language.
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="withNewLine"></param>
        /// <returns></returns>
        public string GetTextUnified(List<string> keys, bool withNewLine)
        {
            var multipleText = new StringBuilder();

            foreach (var key in keys)
            {
                if (withNewLine)
                {
                    multipleText.Append("\n\n");
                }

                multipleText.Append(GetText(key));
            }

            return multipleText.ToString();
        }

        /// <summary>
        /// All the languages have the key?
        /// </summary>
        /// <param name="key"></param>
        /// <param name="checkIfExists"></param>
        /// <returns></returns>
        public bool ContainsKeyInAllLanguages(string key, bool checkIfExists = true)
        {
            if (checkIfExists && !_keysDictionary.ContainsKey(key))
            {
                Debug.LogError("[LanguageManager] Could not find key '" + key + "' on the dictionary.");
                return false;
            }

            var containsKeyInAllLanguages = true;
            var value = _keysDictionary[key];
            var valuesCount = value.ValuesDictionary.Count;

            if (valuesCount != LanguageTypes.Count)
            {
                containsKeyInAllLanguages = false;

                var errorMsg = new StringBuilder();
                errorMsg.Append("[LanguageError] The key ");
                errorMsg.Append(key);
                errorMsg.Append(" has ");
                errorMsg.Append(valuesCount);
                errorMsg.Append(" entries but needs ");
                errorMsg.Append(LanguageTypes.Count);
                errorMsg.Append(". The key is missing in these languages: ");

                foreach (var language in LanguageTypes)
                {
                    if (!value.ValuesDictionary.ContainsKey(language.LanguageName))
                    {
                        errorMsg.Append("\n" + language.LanguageName);
                    }
                }

                Debug.LogError(errorMsg.ToString());
            }

            return containsKeyInAllLanguages;
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Checks if every key exists in all the languages.
        /// </summary>
        /// <returns></returns>
        private bool CheckKeysEntries()
        {
            var noErrors = true;

            foreach (var item in _keysDictionary)
            {
                noErrors = ContainsKeyInAllLanguages(item.Key, false);
            }

            return noErrors;
        }

        #endregion Non Public Methods
    }
}