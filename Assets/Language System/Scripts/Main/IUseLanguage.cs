namespace LanguageSystem
{
    public interface IUseLanguage
    {
        /// <summary>
        /// Subscribe to the OnLanguageChanged event on LanguageController.
        /// </summary>
        void SubscribeEventLanguage();

        /// <summary>
        /// Updates the text with the current language. Handler of OnLanguageChanged event.
        /// </summary>
        void UpdateLanguage();
    }
}