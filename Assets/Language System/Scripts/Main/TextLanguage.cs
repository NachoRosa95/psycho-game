using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace LanguageSystem
{
    /// <summary>
    /// Component to manage Text with languages
    /// </summary>
    public class TextLanguage : MonoBehaviour, IUseLanguage
    {
        #region Exposed fields

        [SerializeField] private string _key;
        [SerializeField] bool debug;

        #endregion Exposed fields

        #region Internal fields

        private Text _txtObject;
        private TextMeshProUGUI _txtObjectPro;
        private InputField _inputField;
        private TMP_InputField _inputFieldPro;

        private LanguageManager _cachedLanguageManager;

        private LanguageManager CachedLanguageManager
        {
            get
            {
                if (_cachedLanguageManager == null)
                {
                    _cachedLanguageManager = LanguageManager.Instance;
                }

                return _cachedLanguageManager;
            }
        }

        #endregion Internal fields

        #region Properties

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        private void Awake()
        {
            _txtObject = GetComponent<Text>();
            _txtObjectPro = GetComponent<TextMeshProUGUI>();
            _inputField = GetComponent<InputField>();
            _inputFieldPro = GetComponent<TMP_InputField>();
            SubscribeEventLanguage();
        }

        private void OnEnable()
        {
            UpdateLanguage();
        }

        #endregion Events methods

        #region Public Methods

        public void SubscribeEventLanguage()
        {
            CachedLanguageManager.OnLanguageChanged.AddListener(UpdateLanguage);
        }

        public void UpdateLanguage()
        {
            if (debug) Debug.Log("Updating language");
            var text = CachedLanguageManager.GetText(_key);
            if (debug) Debug.Log(text);
            if (_txtObject) _txtObject.text = text;
            if (_txtObjectPro) _txtObjectPro.text = text;
            if (_inputField) _inputField.text = text;
            if (_inputFieldPro) _inputFieldPro.text = text;
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}