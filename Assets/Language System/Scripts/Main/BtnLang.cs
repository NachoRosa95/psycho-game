using UnityEngine;
using UnityEngine.UI;

namespace LanguageSystem
{
    /// <summary>
    /// Button that changes the main language
    /// </summary>
    public class BtnLang : MonoBehaviour
    {
        #region Fields & Properties

        [SerializeField] private LanguageType _type;

        private Text _languageText;

        private Button _btn;

        private LanguageManager _cachedLanguageManager;

        private LanguageManager CachedLanguageManager
        {
            get
            {
                if (_cachedLanguageManager == null)
                {
                    _cachedLanguageManager = LanguageManager.Instance;
                }

                return _cachedLanguageManager;
            }
        }

        #endregion

        #region Methods

        private void Awake()
        {
            _btn = GetComponent<Button>();
            _languageText = GetComponentInChildren<Text>();

            if (_btn != null && _languageText != null && _type != null)
            {
                _btn.onClick.AddListener(ClickButton);
                _languageText.text = _type.LanguageName;
            }
            else
            {
                Debug.LogError("Missing component/variable!" + gameObject);
            }
        }

        /// <summary>
        /// Button Click Effect (changes the language)
        /// </summary>
        private void ClickButton()
        {
            CachedLanguageManager.ChangeLanguage(_type);
        }

        #endregion
    }
}