using System;
using System.Collections.Generic;
using UnityEngine;

namespace LanguageSystem
{
    /// <summary>
    /// Sprites Language class.
    /// </summary>
    [Serializable]
    public class SpritesLanguage
    {
        #region Exposed fields
	
        [SerializeField] private bool _sameSprite;
        [SerializeField] private List<SpriteLanguageItem> _items = new List<SpriteLanguageItem>();
        
        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties
        
        public bool SameSprite
        {
            get { return _sameSprite; }
            set { _sameSprite = value; }
        }

        public List<SpriteLanguageItem> Items
        {
            get { return _items; }
            set { _items = value; }
        }
        
        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Gets the icon in the current language.
        /// </summary>
        /// <returns></returns>
        public Sprite GetLanguageSprite()
        {
            if (_sameSprite)
            {
                return _items[0].MySprite;
            }
            
            var actualLanguage = LanguageManager.Instance.Selected;
            
            foreach (var item in Items)
            {
                if (item.MyLanguage.LanguageName == actualLanguage.LanguageName)
                {
                    return item.MySprite;
                }
            }

            Debug.LogError("[Sprite Error] Cant found a SpriteLanguage in " + actualLanguage.LanguageName);
            return null;
        }
        
        #endregion Public Methods

        #region Non Public Methods
    
        #endregion Non Public Methods
    }
}