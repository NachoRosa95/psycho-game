﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LanguageSystem
{
    [CreateAssetMenu(menuName = "LanguageSystem/ManagerData")]
    public class LanguageManagerData : ScriptableObject
    {
        public string FolderPath;
        public LanguageType StartingLanguage;
        public List<LanguageType> LanguageTypes = new List<LanguageType>();
    }
}
