using System;

namespace LanguageSystem
{
    [Serializable]
    public class LanguageItem
    {
        public string key;
        public string value;
    }
}