﻿using System;
using System.Collections.Generic;
using LanguageSystem.Utilities;
using UnityEngine;

namespace LanguageSystem
{
    [Serializable]
    public class LanguagesValue
    {
        [HideInInspector][SerializeField]
        private DictionaryOfStrings _valuesDictionary = new DictionaryOfStrings();

        public DictionaryOfStrings ValuesDictionary
        {
            get { return _valuesDictionary; }
            set { _valuesDictionary = value; }
        }
    }
}