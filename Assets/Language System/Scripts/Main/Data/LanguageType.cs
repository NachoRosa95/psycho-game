using System;
using System.Collections.Generic;
using UnityEngine;

namespace LanguageSystem
{
    [Serializable]
    [CreateAssetMenu(menuName = "LanguageType", fileName = "Language_")]
    public class LanguageType : ScriptableObject
    {
        #region Exposed fields
	
        [HideInInspector][SerializeField] private string _languageName;

        [HideInInspector][SerializeField] private List<string> _filesNames = new List<string>();
        
        #endregion Exposed fields

        #region Internal fields
        
        #endregion Internal fields

        #region Properties
    
        public string LanguageName
        {
            get { return _languageName; }
            set { _languageName = value; }
        }

        public List<string> FilesNames
        {
            get { return _filesNames; }
            set { _filesNames = value; }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods
        
        #endregion Non Public Methods
    }
}