using System;
using System.Collections.Generic;
using UnityEngine;

namespace LanguageSystem
{
    [Serializable]
    public class LanguageData
    {
        public List<LanguageItem> items = new List<LanguageItem>();
    }
}