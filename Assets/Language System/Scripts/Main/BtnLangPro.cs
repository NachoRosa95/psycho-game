using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LanguageSystem
{
    /// <summary>
    /// Button that changes the main language
    /// </summary>
    public class BtnLangPro : MonoBehaviour
    {
        #region Fields & Properties

        [SerializeField] private LanguageType _type;
        [SerializeField] bool updateText;

        private TextMeshProUGUI _text;

        private Button _btn;

        private LanguageManager _cachedLanguageManager;

        private LanguageManager CachedLanguageManager
        {
            get
            {
                if (_cachedLanguageManager == null)
                {
                    _cachedLanguageManager = LanguageManager.Instance;
                }

                return _cachedLanguageManager;
            }
        }

        #endregion

        #region Methods

        private void Awake()
        {
            _btn = GetComponent<Button>();
            _text = GetComponentInChildren<TextMeshProUGUI>();

            if (_btn != null && _text != null && _type != null)
            {
                _btn.onClick.AddListener(ClickButton);
                if (updateText) _text.text = _type.LanguageName;
            }
            else
            {
                Debug.LogError("Missing component/variable!", this);
            }
        }

        /// <summary>
        /// Button Click Effect (changes the language)
        /// </summary>
        private void ClickButton()
        {
            CachedLanguageManager.ChangeLanguage(_type);
        }

        public void Execute()
        {
            ClickButton();
        }

        #endregion
    }
}