﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LanguageSystem.Utilities
{
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<TKey> keys = new List<TKey>();
     
        [SerializeField]
        private List<TValue> values = new List<TValue>();
     
        /// <inheritdoc />
        public void OnBeforeSerialize()
        {
            // Save the dictionary to lists
            keys.Clear();
            values.Clear();
            
            foreach(var pair in this)
            {
                keys.Add(pair.Key);
                values.Add(pair.Value);
            }
        }

        /// <inheritdoc />
        public void OnAfterDeserialize()
        {
            // Load dictionary from lists
            
            Clear();

            if (keys.Count != values.Count)
            {
                throw new Exception(string.Format("there are {0} keys and {1} values after " +
                                "deserialization. Make sure that both key and value types " +
                                "are serializable.", keys.Count, values.Count));
            }
              
            for (var i = 0; i < keys.Count; i++)
            {
                Add(keys[i], values[i]);
            }
        }
    }
    
    [Serializable] public class DictionaryOfStrings : SerializableDictionary<string, string> {}
    
    [Serializable] public class DictionaryOfInts : SerializableDictionary<int, int> {}
    
    [Serializable] public class DictionaryOfIntAndString : SerializableDictionary<int, string> {}
    
    [Serializable] public class DictionaryOfStringAndInt : SerializableDictionary<string, int> {}
    
    [Serializable] public class LanguagesDictionary : SerializableDictionary<string, LanguagesValue> {}
}