using UnityEngine;

namespace LanguageSystem
{
    /// <summary>
    /// Sprite Language Item.
    /// </summary>
    [System.Serializable]
    public class SpriteLanguageItem
    {
        #region Exposed fields
	
        [SerializeField] private Sprite _sprite;
        [SerializeField] private LanguageType _languageType;
        
        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties
    
        public LanguageType MyLanguage
        {
            get { return _languageType; }
            set { _languageType = value; }
        }

        public Sprite MySprite
        {
            get { return _sprite; }
            set { _sprite = value; }
        }
        
        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sprite"></param>
        /// <param name="languageType"></param>
        public SpriteLanguageItem(Sprite sprite, LanguageType languageType)
        {
            MySprite = sprite;
            MyLanguage = languageType;
        }
        
        #endregion Public Methods

        #region Non Public Methods
    
        #endregion Non Public Methods
    }
}