using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace LanguageSystem
{
    /// <summary>
    /// Generic editor extensions for the Language System.
    /// </summary>
    public static class EditorLanguageExtensions
    {
        private static readonly Texture2D ErrorTexture = Resources.Load<Texture2D>("LanguageSystemBackgrounds/Red");
        private static readonly Color ErrorColorFont = Color.white;
        
        private static readonly Texture2D ValidTexture = Resources.Load<Texture2D>("LanguageSystemBackgrounds/Green");
        private static readonly Color ValidColorFont = Color.black;
        
         /// <summary>
        /// Shows and sets a list.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="targetList"></param>
        /// <param name="haveFoldout"></param>
        /// <param name="foldoutBool"></param>
        /// <typeparam name="T"></typeparam>
        public static bool DrawObjectList<T>(string title, List<T> targetList, bool haveFoldout, bool foldoutBool = false) where T : Object
        {
            if (targetList == null) return false;
            
            GUI.backgroundColor = new Color(0.8f, 0.8f, 0.8f);
            GUILayout.BeginVertical("miniButton");
            GUILayout.BeginHorizontal(GUILayout.Height(25));
            GUILayout.Space(10f);

            var showList = true;
            var titleLabel = title + " (" + targetList.Count + ")";

            if (haveFoldout)
            {
                showList = EditorGUILayout.Foldout(foldoutBool, titleLabel, GetFoldoutStyle());
            }
            else
            {
                GUILayout.Label(titleLabel, "BoldLabel");
            }
            
            // Add dragged types
            var draggedTypes = DropAreaGui<T>();

            if (draggedTypes != null)
            {
                foreach (var draggedCity in draggedTypes)
                {
                    if (targetList.Contains(draggedCity)) continue;
                    
                    targetList.Add(draggedCity);
                    
                    if(haveFoldout) showList = true;
                }
            }

            var clear = GUILayout.Button("Clear", GUILayout.Width(Screen.width /8f));
            var addType = GUILayout.Button("+", GUILayout.Width(Screen.width /15f));
            var removeLastType = GUILayout.Button("-", GUILayout.Width(Screen.width /15f));
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            DeleteDuplicatesListBtn(targetList);
            OrderListByNameBtn(targetList);
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            // Clear list.
            if (clear)
            {
                targetList.Clear();
            }
            
            // Add type.
            if (addType)
            {
                targetList.Add(null);
                
                EditorGUIUtility.ShowObjectPicker<T>(targetList[targetList.Count -1], false, "", GUIUtility.GetControlID(FocusType.Passive) + 100);

                if(haveFoldout) showList = true;
            }
            
            if (Event.current.commandName == "ObjectSelectorUpdated" && targetList.Count > 0)
            {
                targetList[targetList.Count -1] = EditorGUIUtility.GetObjectPickerObject() as T;
            }
    
            // Remove last type.
            if (removeLastType && targetList.Count > 0)
            {
                targetList.RemoveAt(targetList.Count - 1);
                
                if (haveFoldout) showList = true;
            }

            if (showList)
            {
                ShowTypes(targetList);
            }

            GUI.backgroundColor = Color.white;
            GUILayout.Space(10);

            if (haveFoldout && targetList.Count == 0)
            {
                showList = false;
            }

            return showList;
        }

        /// <summary>
        /// Shows the list types.
        /// </summary>
        /// <param name="targetList"></param>
        /// <typeparam name="T"></typeparam>
        private static void ShowTypes<T>(IList<T> targetList) where T : Object
        {
            if (targetList == null || targetList.Count == 0) return;
            
            EditorGUILayout.BeginVertical("textField");
            GUILayout.Space(5f);
            
            for (var j = 0; j < targetList.Count; j++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(10f);
                
                targetList[j] =
                    (T) EditorGUILayout.ObjectField(targetList[j], typeof(T), false);
                
                var removeType = GUILayout.Button("X", GUILayout.Width(Screen.width /15f), GUILayout.MaxHeight(15));
                GUILayout.EndHorizontal();
                GUILayout.Space(5);
    
                // Remove type with X btn.
                if (removeType)
                {
                    targetList.RemoveAt(j);
                    break;
                }
            }
            
            // Error null
            if (targetList.Contains(null))
            {
                GUILayout.Label(typeof(T).Name + " Null!", GetErrorStyle());
            }
            
            EditorGUILayout.EndVertical();
        }
        
        
        /// <summary>
        /// Shows and sets a drop area and returns the dragged objects types.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> DropAreaGui<T> ()
        {            
            var dropArea = GUILayoutUtility.GetRect (80f, 20.0f, GUILayout.ExpandWidth (false));

            var style = new GUIStyle(GUI.skin.box) {richText = true};

            GUI.Box (dropArea, "<color=#808080ff>Drop Area</color>", style);

            if (dropArea.Contains(Event.current.mousePosition))
            {
                if (Event.current.type == EventType.DragUpdated)
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    Event.current.Use ();
                }   
                else if (Event.current.type == EventType.DragPerform)
                {
                    var returnList = new List<T>();
                    
                    foreach (var draggedObject in DragAndDrop.objectReferences)
                    {
                        if (draggedObject.GetType() == typeof(T))
                        {                     
                            var aux = (T) Convert.ChangeType(draggedObject, typeof(T));
                            returnList.Add(aux);
                        }
                    }
                    
                    Event.current.Use ();
                    return returnList;
                }
            }
 
            return null;
        }

        /// <summary>
        /// Button that deletes the duplicated objects (with the same name) on the list.
        /// </summary>
        /// <param name="targetList"></param>
        /// <typeparam name="T"></typeparam>
        public static void DeleteDuplicatesListBtn<T>(List<T> targetList)
        {
            var deleteDuplicates = GUILayout.Button("Delete Duplicates");
            
            if (deleteDuplicates)
            {
                targetList.RemoveAll(x => x == null);
                
                var clearedList = targetList.GroupBy(x => x).Select(x => x.First()).ToList();
                var clearedCount = targetList.Count - clearedList.Count;
                targetList.Clear();
                targetList.AddRange(clearedList);
                Debug.Log("[EditorListExtensions] Cleared duplicates: " + clearedCount);
            }
        }
        
        /// <summary>
        /// Button that order the objects items by their name.
        /// </summary>
        /// <param name="targetList"></param>
        /// <typeparam name="T"></typeparam>
        public static void OrderListByNameBtn<T>(List<T> targetList) where T : Object
        {
            var orderBtn = GUILayout.Button("Order By Name");
            
            if (orderBtn)
            {
                targetList.RemoveAll(x => x == null);
                
                var sortedList = targetList.OrderBy(x => x.name).ToList();
                targetList.Clear();
                targetList.AddRange(sortedList);
            }
        }
        
        /// <summary>
        /// Button that order the language items by their name.
        /// </summary>
        /// <param name="targetList"></param>
        public static void OrderLanguageItemsByName(List<LanguageItem> targetList)
        {
            var orderBtn = GUILayout.Button("Order By Key Name");
            
            if (orderBtn)
            {
                targetList.RemoveAll(x => x == null);
                
                var sortedList = targetList.OrderBy(x => x.key).ToList();
                targetList.Clear();
                targetList.AddRange(sortedList);
            }
        }
        
        /// <summary>
        /// Initializes the icon languages.
        /// </summary>
        public static void InitializeLanguages(SpritesLanguage spritesLanguage)
        {
            if (spritesLanguage == null || spritesLanguage.Items == null) return;
            
            var languagesType = LanguageManager.Instance.LanguageTypes;
            var old = new List<SpriteLanguageItem>(spritesLanguage.Items);
            spritesLanguage.Items.Clear();
                
            foreach (var languageType in languagesType)
            {
                var exists = false;
                    
                for (var i = 0; i < old.Count; i++)
                {
                    if (old[i].MyLanguage != languageType) continue;
                        
                    exists = true;
                    spritesLanguage.Items.Add(old[i]);
                    old.RemoveAt(i);
                    break;
                }

                if (exists) continue;
                    
                var item = new SpriteLanguageItem(null, languageType);
                spritesLanguage.Items.Add(item);
            }
        }

        /// <summary>
        /// Sets and shows the icon languages.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="spritesLanguage"></param>
        /// <param name="selected"></param>
        public static Sprite ShowSpritesLanguage(string title, SpritesLanguage spritesLanguage, Sprite selected)
        {
            if (spritesLanguage == null) return null;
            
            GUILayout.BeginVertical("Box");
            GUILayout.BeginHorizontal();
            GUILayout.Label(title);
            GUILayout.FlexibleSpace();
            spritesLanguage.SameSprite = GUILayout.Toggle(spritesLanguage.SameSprite, " Same Sprites");
            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            
            if (spritesLanguage.SameSprite)
            {
                GUILayout.Space(5f);
                GUILayout.BeginHorizontal();
                GUILayout.Space(5f);
                GUILayout.Label("Sprite for all", GUILayout.Width(Screen.width /5f));
                selected = (Sprite) EditorGUILayout.ObjectField(selected, typeof(Sprite), false);
                GUILayout.EndHorizontal();

                if (selected != null)
                {
                    foreach (var item in spritesLanguage.Items)
                    {
                        item.MySprite = selected;
                    }
                }
            }
            else
            {
                selected = null;
                

                foreach (var item in spritesLanguage.Items)
                {
                    GUILayout.Space(5f);
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5f);
                    GUILayout.Label(item.MyLanguage.LanguageName, GUILayout.Width(Screen.width /5f));
                    item.MySprite = (Sprite) EditorGUILayout.ObjectField(item.MySprite, typeof(Sprite), false);
                    GUILayout.EndHorizontal();
                }
            }
            GUILayout.Space(5);
            GUILayout.EndVertical();
            
            return selected;
        }
        
        /// <summary>
        /// Returns the error GUIStyle.
        /// </summary>
        /// <returns></returns>
        public static GUIStyle GetErrorStyle()
        {
            var errorStyle = new GUIStyle
            {
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Bold,
                clipping = TextClipping.Clip,
                normal = {background = ErrorTexture, textColor = ErrorColorFont}
            };

            return errorStyle;
        }
        
        /// <summary>
        /// Returns the valid GUIStyle.
        /// </summary>
        /// <returns></returns>
        public static GUIStyle GetValidStyle()
        {
            var validStyle = new GUIStyle
            {
                fontSize = 12,
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Bold,
                clipping = TextClipping.Clip,
                normal = {background = ValidTexture, textColor = ValidColorFont}
            };

            return validStyle;
        }
        
        /// <summary>
        /// Draws a subtitle.
        /// </summary>
        /// <param name="title"></param>
        public static void DrawSubtitle(string title)
        {
            GUILayout.BeginHorizontal("Toolbar", GUILayout.Height(15));
            GUILayout.Label(title, GetSubtitleStyle());
            GUILayout.EndHorizontal();
        }
        
        /// <summary>
        /// Gets a subtitle style.
        /// </summary>
        /// <returns></returns>
        public static GUIStyle GetSubtitleStyle()
        {
            var infoStyle = new GUIStyle
            {
                fontSize = 11,
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Bold,
                normal = {textColor = Color.white}
            };

            return infoStyle;
        }
        
        /// <summary>
        /// Gest a foldout style.
        /// </summary>
        /// <returns></returns>
        public static GUIStyle GetFoldoutStyle()
        {
            return new GUIStyle(EditorStyles.foldout) {fontStyle = FontStyle.Bold};
        }
        
        /// <summary>
        /// Draws the validate keys button.
        /// </summary>
        /// <param name="singleKey"></param>
        /// <returns></returns>
        public static bool DrawValidateKeysButton(bool singleKey = false)
        {
            var title = singleKey ? "VALIDATE KEY" : "VALIDATE KEYS";
            
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            var validateButton = GUILayout.Button(title, GUILayout.Width(150));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            return validateButton;
        }
        
        /// <summary>
        /// Validates a text key. Returns true if the key is valid.
        /// </summary>
        /// <param name="key"></param>
        public static bool ValidateKey(string key)
        {
            if (LanguageManager.Instance.ContainsKeyInAllLanguages(key)) return true;
            
            Debug.LogError("[KeyError] Key not found in LanguageType Types loaded on LanguageManager: " + key);
            return false;
        }
    }
}