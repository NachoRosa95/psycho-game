using System.IO;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LanguageSystem
{
    [CustomEditor(typeof(LanguageManager))]
    public class LanguageManagerEditor : Editor
    {
        private LanguageManager _manager;

        private bool _languageErrors;

        private static bool _showLanguages;

        private void OnEnable()
        {
            _manager = (LanguageManager)target;

            _languageErrors = false;
        }

        private void OnDisable()
        {
            if (target != null)
            {
                EditorUtility.SetDirty(target);
            }
        }

        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            GUILayout.Space(10);
            EditorLanguageExtensions.DrawSubtitle("Language Manager");

            DrawFolderPath();

            DrawStartingLanguageType();

            DrawLanguages();

            ChargeLanguages();

            if (GUI.changed && !Application.isPlaying)
            {
                EditorUtility.SetDirty(target);
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
            }

        }

        /// <summary>
        /// Draws the folder path field.
        /// </summary>
        private void DrawFolderPath()
        {
            GUILayout.BeginVertical("Box");
            _manager.FolderPath = EditorGUILayout.TextField("Folder Path", _manager.FolderPath);

            if (_manager.FolderPath == "")
            {
                _manager.FolderPath = "/Language System/LanguagesFiles/";
            }

            // Error directory dont exists
            if (!Directory.Exists(_manager.FolderCompletePath))
            {
                GUILayout.Label("Invalid Folder Path!", EditorLanguageExtensions.GetErrorStyle());
            }

            GUILayout.EndVertical();
            GUILayout.Space(10);
        }

        /// <summary>
        /// Draws the starting language field.
        /// </summary>
        private void DrawStartingLanguageType()
        {
            GUILayout.BeginVertical("Box");
            GUILayout.Label("Starting LanguageType");
            _manager.StartingLanguage =
                (LanguageType)EditorGUILayout.ObjectField(_manager.StartingLanguage,
                    typeof(LanguageType), false);

            // Error null
            if (_manager.StartingLanguage == null)
            {
                GUILayout.Label("Null LanguageType!", EditorLanguageExtensions.GetErrorStyle());
            }

            GUILayout.EndVertical();
            GUILayout.Space(10);
        }

        /// <summary>
        /// Draws the languages types list.
        /// </summary>
        private void DrawLanguages()
        {
            _showLanguages =
                EditorLanguageExtensions.DrawObjectList("Language Types", _manager.LanguageTypes, true, _showLanguages);
        }

        /// <summary>
        /// Creates the dictionary of keys and languages values and checks if there are no errors.
        /// </summary>
        private void ChargeLanguages()
        {
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            var btn = GUILayout.Button("RECREATE LANGUAGE ENTRIES");
            GUILayout.EndVertical();

            if (btn)
            {
                _languageErrors = !_manager.RecreateLanguagesEntries();
            }

            if (_languageErrors)
            {
                EditorGUILayout.HelpBox("Language Types Errors (See log)", MessageType.Error);
            }
            else
            {
                EditorGUILayout.HelpBox("No errors found. All keys in all languages were loaded successfully.", MessageType.None);
            }

            GUILayout.Space(5);
        }

    }
}