using System.IO;
using UnityEditor;
using UnityEngine;

namespace LanguageSystem
{
    [CustomEditor(typeof(LanguageType))]
    public class LanguageTypeEditor : Editor
    {
        private LanguageType _type;

        private void OnEnable()
        {
            _type = (LanguageType) target;
        }
        
        private void OnDisable()
        {
            if (target != null)
            {
                EditorUtility.SetDirty(target);
            }
        }

        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            GUILayout.Space(10);
            EditorLanguageExtensions.DrawSubtitle("Language Type");
            
            DrawLanguageName();
            
            DrawFileNames();
        }

        /// <summary>
        /// Draws the display name of the language.
        /// </summary>
        private void DrawLanguageName()
        {
            GUILayout.BeginVertical("Box");
            _type.LanguageName = EditorGUILayout.TextField("Language Name", _type.LanguageName);
            GUILayout.EndVertical();
            GUILayout.Space(10);
        }

        /// <summary>
        /// Draws the list of file names.
        /// </summary>
        private void DrawFileNames()
        {
            GUILayout.BeginVertical("Box");
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Files");
            var clear = GUILayout.Button("Clear", GUILayout.Width(Screen.width /8f));
            var addFile = GUILayout.Button("+", GUILayout.Width(Screen.width /15f));
            var removeLastFile = GUILayout.Button("-", GUILayout.Width(Screen.width /15f));
            GUILayout.EndHorizontal();
            GUILayout.Space(10);

            // Clear list.
            if (clear)
            {
                _type.FilesNames.Clear();
            }
            
            // Add file.
            if (addFile)
            {
                _type.FilesNames.Add("");
            }
    
            // Remove last file.
            if (removeLastFile && _type.FilesNames.Count > 0)
            {
                _type.FilesNames.RemoveAt(_type.FilesNames.Count - 1);
            }
            
            // Show files.
            for (var j = 0; j < _type.FilesNames.Count; j++)
            {
                GUILayout.BeginHorizontal();
                _type.FilesNames[j] = EditorGUILayout.TextField("File Name", _type.FilesNames[j]);
                var removeType = GUILayout.Button("X", GUILayout.Width(Screen.width /15f), GUILayout.MaxHeight(15));
                GUILayout.EndHorizontal();
    
                // Remove file with X btn.
                if (removeType)
                {
                    _type.FilesNames.RemoveAt(j);
                    break;
                }
                
                // Error directory dont exists
                var filePath = Path.Combine (LanguageManager.Instance.FolderCompletePath, _type.FilesNames[j]);
                
                if (!File.Exists(filePath))
                {
                    GUILayout.Label("Invalid Folder Path!", EditorLanguageExtensions.GetErrorStyle());
                    GUILayout.Space(10);
                }
                
                GUILayout.Space(5);
            }
    
            GUILayout.EndVertical();
            GUILayout.Space(10);
        }
    }
}