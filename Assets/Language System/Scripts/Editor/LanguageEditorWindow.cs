﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace LanguageSystem
{
	/// <summary>
	/// Editor window to create and modify the language data.
	/// </summary>
	public class LanguageEditorWindow : EditorWindow
	{
		[SerializeField] private string _fileName;
		[SerializeField] private LanguageData _languageData;

		private static bool _showValues;

        Vector2 scrollPos = Vector2.zero;

        [MenuItem("Window/Language Text Editor")]
		public static void ShowWindow()
		{
			GetWindow<LanguageEditorWindow>(false, "Language Text Editor", true);
		}
		
		private void OnEnable()
		{
			var data = EditorPrefs.GetString("LanguageEditorWindow", JsonUtility.ToJson(this, false));
			JsonUtility.FromJsonOverwrite(data, this);
		}
 
		protected void OnDisable ()
		{
			var data = JsonUtility.ToJson(this, false);
			EditorPrefs.SetString("LanguageEditorWindow", data);
		}

		private void OnGUI()
		{
			GUILayout.Space(10);
			
			if (_languageData != null)
			{
				var subtitle = _fileName != string.Empty ? _fileName : "New Data";
				EditorLanguageExtensions.DrawSubtitle(subtitle);
				
				DrawItems();
				
				GUILayout.Space(10);
				
				DrawSaveButton();
			}
			
			GUILayout.Space(5);
			
			DrawLoadButton();
			
			GUILayout.Space(5);
			
			DrawCreateButton();
		}

		/// <summary>
		/// Draws the save data button.
		/// </summary>
		private void DrawSaveButton()
		{
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			var saveButton = GUILayout.Button("Save Data", GUILayout.Width(200));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			if (saveButton)
			{
				SaveGameData();
			}
		}
		
		/// <summary>
		/// Draws the load data button.
		/// </summary>
		private void DrawLoadButton()
		{
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			var loadButton = GUILayout.Button("Load Data", GUILayout.Width(200));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			if (loadButton)
			{
				_showValues = true;
				LoadGameData();
			}
		}
		
		/// <summary>
		/// Draws the create data button.
		/// </summary>
		private void DrawCreateButton()
		{
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			var createButton = GUILayout.Button("Create new Data", GUILayout.Width(200));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			if (createButton)
			{
				CreateNewData();
			}
		}

		/// <summary>
		/// Draws the Language Data items.
		/// </summary>
		private void DrawItems()
		{
			if (_languageData.items.Count == 0)
			{
				_showValues = false;
			}
			
			var amountItems = " (" + _languageData.items.Count + ")";
            
            GUILayout.BeginHorizontal("miniButton", GUILayout.Height(25));
            _showValues = EditorGUILayout.Foldout(_showValues, "Language Items" + amountItems, EditorLanguageExtensions.GetFoldoutStyle());
            var clearItems = GUILayout.Button("Clear", GUILayout.Width(Screen.width /8f));
            var addItem = GUILayout.Button("+", GUILayout.Width(Screen.width /15f));
            var removeLastItem = GUILayout.Button("-", GUILayout.Width(Screen.width /15f));
            GUILayout.EndHorizontal();

            if (clearItems)
            {
                _languageData.items.Clear();
            }
            
            if (addItem)
            {
	            var newItem = new LanguageItem
	            {
		            key = "",
		            value = ""
	            };
	            
	            _languageData.items.Add(newItem);
                _showValues = true;
            }

            if (removeLastItem && _languageData.items.Count > 0)
            {
	            _languageData.items.RemoveAt(_languageData.items.Count - 1);
                _showValues = true;
            }
            
            if (_showValues)
            {
                GUILayout.BeginVertical("textField");

                
                scrollPos = GUILayout.BeginScrollView(scrollPos);

                for (var i = 0; i < _languageData.items.Count; i++)
                {
	                GUILayout.BeginVertical("Box");
	                
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5f);
                    GUILayout.Label(i + ". Key: ", GUILayout.Width(Screen.width /12f), GUILayout.MaxHeight(15));
	                _languageData.items[i].key = EditorGUILayout.TextField(_languageData.items[i].key);
	                var removeType = GUILayout.Button("X", GUILayout.Width(Screen.width /15f), GUILayout.MaxHeight(15));
	                GUILayout.EndHorizontal();
	                
	                GUILayout.BeginVertical();
	                _languageData.items[i].value = EditorGUILayout.TextArea(_languageData.items[i].value);
	                GUILayout.EndVertical();
	                
	                GUILayout.EndVertical();
                    
                    GUILayout.Space(5);
    
                    if (removeType)
                    {
                        _languageData.items.RemoveAt(i);
                        break;
                    }

                    // Error if the property is empty field
                    if (_languageData.items[i].key == string.Empty || _languageData.items[i].value == string.Empty)
                    {
                        GUILayout.Label("Your field is empty!", EditorLanguageExtensions.GetErrorStyle());
                    }
	                
	                GUILayout.Space(5);
                }
                
                GUILayout.EndScrollView();

                GUILayout.EndVertical();
            }
			
			EditorLanguageExtensions.OrderLanguageItemsByName(_languageData.items);
		}

		/// <summary>
		/// Load a language data file.
		/// </summary>
		private void LoadGameData()
		{
			var filePath = EditorUtility.OpenFilePanel("Select language file",
				LanguageManager.Instance.FolderCompletePath, "json");
			
			if (!string.IsNullOrEmpty(filePath))
			{
				_fileName = Path.GetFileNameWithoutExtension(filePath);
				
				var dataAsJson = File.ReadAllText(filePath);
				_languageData = JsonUtility.FromJson<LanguageData>(dataAsJson);
			}
		}

		/// <summary>
		/// Saves a language data file.
		/// </summary>
		private void SaveGameData()
		{
			var filePath = EditorUtility.SaveFilePanel("Save language file",
				LanguageManager.Instance.FolderCompletePath, "", "json");

			if (!string.IsNullOrEmpty(filePath))
			{
				_fileName = Path.GetFileNameWithoutExtension(filePath);
				
				var dataAsJson = JsonUtility.ToJson(_languageData);
				File.WriteAllText(filePath, dataAsJson);
			}
		}

		/// <summary>
		/// Creates a new language data.
		/// </summary>
		private void CreateNewData()
		{
			_languageData = new LanguageData();
			_fileName = "";
		}
	}
}

