﻿using UnityEngine;
using System.Collections;
using System;

public class GlobalBehaviour : MonoBehaviour
{
    public event Action OnInit;
    public event Action OnUpdate;
    public event Action OnExit;

    private static GlobalBehaviour instance;

    public static GlobalBehaviour Instance
    {
        get
        {
            if (instance == null)
            {
                var go = GameObject.Find("GlobalBehaviour") ?? new GameObject("GlobalBehaviour");
                instance = go.GetComponent<GlobalBehaviour>() ?? go.AddComponent<GlobalBehaviour>();
                DontDestroyOnLoad(go);
            }
            return instance;
        }
    }

    void Start() { if (OnInit != null) OnInit(); }
    void Update() { if (OnUpdate != null) OnUpdate(); }
    void OnDestroy() { if (OnExit != null) OnExit(); }
}
