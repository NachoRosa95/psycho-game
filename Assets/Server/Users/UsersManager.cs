﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Collections.ObjectModel;

public class User
{
    // private List<InventionData> _viewedInventions = new List<InventionData>();
    // private List<PuzzleData> _completedPuzzles = new List<PuzzleData>();

    public string name;
    public int age;
    public bool male;
    public int country;
    public string hash;
    public bool hasHash { get { return !string.IsNullOrEmpty(hash); } }

    // public ReadOnlyCollection<InventionData> viewedInventions { get; private set; }
    // public ReadOnlyCollection<PuzzleData> completedPuzzles { get; private set; }

    public User()
    {
        // viewedInventions = _viewedInventions.AsReadOnly();
        // completedPuzzles = _completedPuzzles.AsReadOnly();
    }

    public void RequestHash()
    {
        if (hasHash) return;
        UserDTO user = new UserDTO(name, age, male.ToString().ToLower(), country, SystemInfo.deviceUniqueIdentifier);
        Request.DoOnServer("usersPsycho", JsonUtility.ToJson(user), RequestHashCallback);
    }

    private void RequestHashCallback(WWW www)
    {
        if (!string.IsNullOrEmpty(www.error)) return;
        hash = www.text;
        var userData = PlayerPrefs.GetString("user_" + name);
        PlayerPrefs.SetString("user_" + name, userData + hash);
    }

    public override string ToString()
    {
        var curUser = UsersManager.current;
        if (curUser == null) return "null";
        return (curUser.name + "|" + curUser.age + "|" + curUser.male + "|" + curUser.country + "|" + (curUser.hasHash ? curUser.hash : "null"));
    }

    // public void ViewInvention(InventionData inv)
    // {
    //     if (_viewedInventions.Contains(inv) || inv == null) return;
    //     _viewedInventions.Add(inv);
    //     var userViewInvs = PlayerPrefs.GetString("userViewInvs_" + name);
    //     PlayerPrefs.SetString("userViewInvs_" + name, userViewInvs + "," + inv.id);
    // }

    // public void CompletePuzzle(PuzzleData puzz)
    // {
    //     if (_completedPuzzles.Contains(puzz) || puzz == null) return;
    //     _completedPuzzles.Add(puzz);
    //     var userCompPuzzs = PlayerPrefs.GetString("userCompPuzzs_" + name);
    //     PlayerPrefs.SetString("userCompPuzzs_" + name, userCompPuzzs + "," + puzz.id);
    // }
}

public class UsersManager : MonoBehaviour
{
    public static string[] userNames { get { return PlayerPrefs.GetString("userNames").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries); } }

    public static User current { get; private set; }

    static UsersManager()
    {
        if (!Exists("Default")) Create("Default", 0, true, 0);
        SetCurrent("Default");
    }

    public static bool Exists(string name)
    {
        return userNames.Contains(name);
    }

    public static void Create(string name, int age, bool male, int country)
    {
        PlayerPrefs.SetString("user_" + name, name + "," + age + "," + male + "," + country + ",");

        var users = PlayerPrefs.GetString("userNames") + "," + name;
        PlayerPrefs.SetString("userNames", users);
    }

    public static User Get(string name)
    {
        var userData = PlayerPrefs.GetString("user_" + name).Split(',');
        var user = new User
        {
            name = userData[0],
            age = int.Parse(userData[1]),
            male = bool.Parse(userData[2]),
            country = int.Parse(userData[3]),
            hash = userData[4]
        };

        // var userViewInvs = PlayerPrefs.GetString("userViewInvs_" + name).Split(',');
        // foreach (var userViewInv in userViewInvs)
        // {
        //     var inv = InventionData.all.Find(i => i.id.ToString() == userViewInv);
        //     if (inv != null) user.ViewInvention(inv);
        // }

        // var userCompPuzzs = PlayerPrefs.GetString("userCompPuzzs_" + name).Split(',');
        // foreach (var userCompPuzz in userCompPuzzs)
        // {
        //     var puzz = PuzzleData.all.Find(p => p.id.ToString() == userCompPuzz);
        //     if (puzz != null) user.CompletePuzzle(puzz);
        // }

        user.RequestHash();
        return user;
    }

    public static void SetCurrent(string name)
    {
        current = Get(name);
    }

}