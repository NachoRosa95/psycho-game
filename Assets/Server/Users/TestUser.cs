﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TestUser : MonoBehaviour
{
    [SerializeField] string userName = "testUser";
    // [SerializeField] bool debug = true;
    
    void OnEnable()
    {
        var register = ("Testing register: " + TestRegister(userName));
        Debug.LogWarning(register);
        StartCoroutine("DoLogin", userName);
        // var login = ("Testing login: " + TestLogin(userName));
        // if (!debug) return;
        // Debug.LogWarning(login);
    }

    IEnumerator DoLogin(string name)
    {
        foreach (var i in Enumerable.Range(0, 10))
        {
            Debug.LogWarning("Testing login");
            if (!UsersManager.Exists(name)) { Debug.LogWarning("User does not exist"); yield return new WaitForSeconds(0.5f); }
            else { UsersManager.SetCurrent(name); break; }
        }
        if (!UsersManager.Exists(name)) Debug.LogWarning("Login timed out");
    }

    // public string TestLogin(string name)
    // {
    //     if (!UsersManager.Exists(name)) return "User does not exist";
    //     // if (UsersManager.current.name == UsersManager.Get(name).name) return "User is already logged in: " + UsersManager.current.ToString();
    //     UsersManager.SetCurrent(name);
    //     if (UsersManager.current == null) return "How did this happen";
    //     return "User logged in successfully: " + UsersManager.current.ToString();
    // }

    public string TestRegister(string name)
    {
        if (UsersManager.Exists(name)) return "User already exists";
        UsersManager.Create(name, 1, true, 1);
        return "User created successfully";
    }
}
