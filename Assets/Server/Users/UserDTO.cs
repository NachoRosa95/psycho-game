using System;

[Serializable]
public class UserDTO
{
  public string name;
  public int age;
  public string male;
  public int country;
  public string uid;

  public UserDTO(string name, int age, string male, int country, string uid)
  {
    this.name = name;
    this.age = age;
    this.male = male;
    this.country = country;
    this.uid = uid;
  }

}