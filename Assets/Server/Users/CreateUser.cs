﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class StringEvent : UnityEvent<string>
{

}

public class CreateUser : MonoBehaviour
{
    [SerializeField] string username = "";
    [SerializeField] int age = -1;
    [SerializeField] int gender = -1;
    [SerializeField] GameObject errorLabel;
    public UnityEvent OnSuccess = new UnityEvent();
    public StringEvent OnFailure = new StringEvent();


    public void SetName(string value)
    {
        username = value;
    }

    public void SetAge(string value)
    {
        try
        {
            age = int.Parse(value);
        }
        catch (System.SystemException)
        {
            Debug.Log("Provided age string is not a number", this);
            age = -1;
        }
    }

    public void SetGender(int value)
    {
        gender = value;
    }

    public void Execute()
    {
        if (username == "")
        {
            OnFailure?.Invoke("Please insert a username");
        }
        else if (age == -1)
        {
            OnFailure?.Invoke("Please insert a valid age");
        }
        else if (gender <= 0)
        {
            OnFailure?.Invoke("Please select a gender");
        }
        else
        {
            UsersManager.Create(username, age, gender == 0, 032);
            UsersManager.SetCurrent(username);
            if (OnSuccess != null) OnSuccess.Invoke();
        }
    }


    bool IsAllLetters(string value)
    {
        foreach (char c in value)
        {
            if (!char.IsLetter(c))
                return false;
        }
        return true;
    }
}
