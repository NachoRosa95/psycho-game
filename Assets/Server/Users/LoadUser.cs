﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoadUser : MonoBehaviour
{
    public UnityEvent OnSuccess;

    void OnEnable()
    {
        if (UsersManager.userNames.Length <= 1) gameObject.SetActive(false);
    }

    void Execute()
    {
        if (UsersManager.userNames.Length <= 1)
        {
            Debug.LogError("No users to load from", this);
            return;
        }
        UsersManager.SetCurrent(UsersManager.userNames[UsersManager.userNames.Length - 1]);
        if (OnSuccess != null) OnSuccess.Invoke();
    }
}
