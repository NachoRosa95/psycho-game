using System;

[Serializable]
public class MetricasDTO
{
  public string actions;

  public string hash;

  public MetricasDTO(string actions, string hash)
  {
    this.actions = actions;
    this.hash = hash;
  }
}
