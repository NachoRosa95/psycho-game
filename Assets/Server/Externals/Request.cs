
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Request : MonoBehaviour
{
    private static bool exit;
    private static int downloads;
    private static Request instance;
    public static string server = /*"http://mida.ucema.edu.ar/mida/dev/"; "https://uadegames.000webhostapp.com/"; */ "https://scrumgame.azurewebsites.net/api/";
    static Request()
    {
        var go = new GameObject("Request");
        instance = go.GetComponent<Request>() ?? go.AddComponent<Request>();
        DontDestroyOnLoad(instance);
    }

    #pragma warning disable 0618 // `UnityEngine.WWW' is obsolete: `Use UnityWebRequest, a fully featured replacement which is more efficient and has additional features
    public static void DoOnServer(string url, string json, Action<WWW> callback = null) { instance.StartCoroutine(DoCoroutine(server + url, json, callback)); }
    public static void DoOnServer(string url, Action<WWW> callback = null) { instance.StartCoroutine(DoCoroutine(server + url, null, callback)); }
    public static void Do(string url, string json, Action<WWW> callback = null) { instance.StartCoroutine(DoCoroutine(url, json, callback)); }
    public static void Do(string url, Action<WWW> callback = null) { instance.StartCoroutine(DoCoroutine(url, null, callback)); }

    private static IEnumerator DoCoroutine(string url, string json, Action<WWW> callback = null)
    {
        // if(exit) Application.CancelQuit();
        Hashtable headers = new Hashtable();
        headers.Add("Content-Type", "application/json");
        var www = json == null ? new WWW(url, null, headers) : new WWW(url, System.Text.Encoding.UTF8.GetBytes(json), headers);
        downloads++;
        yield return www;
        Debug.Log(www.url + " returned text:\n" + www.text + "\nwith error:\n" + www.error);
        downloads--;
        if (callback != null) callback(www);

        if (exit && downloads == 0) Application.Quit();
    }

    [RuntimeInitializeOnLoadMethod]
    static void RunOnStart()
    {
        // Application can only quit when downloads == 0
        Application.wantsToQuit += () => (downloads == 0);
    }

    public void OnApplicationQuit()
    {
        exit = true;
        // deprecated
        // if(downloads > 0) Application.CancelQuit();
    }
}