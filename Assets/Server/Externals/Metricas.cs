using System;
using UnityEngine;
using System.Collections;

public enum ActionType
{
    UserLogin = 101,
    UserLogout = 102, // *
    UserRegister = 103,

    AttributeChange = 201,

    SceneChange = 301,
}

public static class Metricas
{
    private static float currentTime;

    private static string Actions
    {
        get
        {
            return PlayerPrefs.GetString(UsersManager.current.name + "_actions");
        }
        set
        {
            PlayerPrefs.SetString(UsersManager.current.name + "_actions", value);
        }
    }

    static Metricas()
    {
        GlobalBehaviour.Instance.OnUpdate -= DispatchCountDown;
        GlobalBehaviour.Instance.OnUpdate += DispatchCountDown;
    }

    private static void DispatchCountDown()
    {
        currentTime -= Time.deltaTime;
        if (currentTime > 0) return;
        currentTime = 5;
        Dispatch();
    }

    private static void DoActions(string actions, Action<bool> callback = null)
    {
        if (!UsersManager.current.hasHash) return;
        MetricasDTO metricas = new MetricasDTO(actions, UsersManager.current.hash);
        Request.DoOnServer("metrics", JsonUtility.ToJson(metricas), delegate(WWW www) { if (callback != null && www.error == null) callback(www.text == "ok"); });
    }

    public static void DoAction(ActionType actionId, int id = 0, float value = 0)
    {
        Actions += actionId.ToString() + "|" + id + "|" + value + "|" + DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss") + "_";
    }

    public static void DoAction(ActionType actionId, string idValue = "", float value = 0)
    {
        Actions += actionId.ToString() + "|" + idValue + "|" + value + "|" + DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss") + "_";
    }


    public static void Dispatch()
    {
        if (Actions.Length > 0) DoActions(Actions, delegate(bool result) { if (result) Actions = ""; });
    }
}