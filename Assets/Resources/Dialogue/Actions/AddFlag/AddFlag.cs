﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AddOrRemoveFlag", menuName = "Scriptable Object/Actions/AddOrRemoveFlag", order = 1)]
public class AddFlag : ActionSO
{
    public FlagSO flag;
    public bool add;

    public override bool Execute(GameObject source = null)
    {
        FlagManager.SetFlag(flag.flagKeyName, add);
        return true;
    }
}
